import * as Yup from 'yup'
import { validateMessages as messages } from '../messages'

export const RegisterSchema = Yup.object().shape({
  fullName: Yup.string()
    .required(messages.REQUIRED)
    .min(4, messages.TOO_SHORT),
  email: Yup.string()
    .required(messages.REQUIRED)
    .email(messages.INVALID_EMAIL),
  phone: Yup.string()
    .required(messages.REQUIRED),
  acceptRule: Yup.bool().oneOf([true], messages.REQUIRED),
})
