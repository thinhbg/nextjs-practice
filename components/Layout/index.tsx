import * as React from 'react'
import RecFooter from './RecFooter'
import RecHeader from './RecHeader'

interface Props {
  children: any,
  active?: string
}

const RecLayout = ({ children, active = 'home' }: Props) => {
  console.log()
  return (
    <div className="rec__custom__layout">
      <RecHeader active={active} />
      <div className="pt--80">{children}</div>
      <RecFooter />
    </div>
  )
}

export default RecLayout
