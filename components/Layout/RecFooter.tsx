import Link from 'next/link'
import * as React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import './styles.css'

export interface Props { }

const RecHeader = () => {
  console.log()
  return (
    <>
      <div className="recFooter">
        <Container>
          <Row>
            <Col xs={12} sm={12} md={5}>
              <div>
                <div className="text-center">
                  <img src="/images/logo-transparent-background.svg" alt="rec-logo" />
                </div>
                <p className="recFooter-name recFooter__text">
                  CỘNG ĐỒNG BẤT ĐỘNG SẢN RECBOOK
                </p>
                <div className="recFooter__info">
                  <div className="mr-3 d-inline-block">
                    <img src="/icons/ico-location-white.svg" alt="rec-location" />
                  </div>
                  <a className="recFooter__text" key="location" href="http://maps.google.com/?q=:LK3.09 Hoàng Huy Riverside Hải Phòng">LK3.09 Hoàng Huy Riverside Hải Phòng</a>
                </div>
                <div className="recFooter__info">
                  <div className="mr-3 d-inline-block">
                    <img src="/icons/ico-web.svg" alt="rec-location" />
                  </div>
                  <a className="recFooter__text" key="web" href="https://recbook.com">recbook.com</a>
                </div>
                <div className="recFooter__info">
                  <div className="mr-3 d-inline-block">
                    <img src="/icons/ico-envelop.svg" alt="rec-location" />
                  </div>
                  <a className="recFooter__text" key="email" href="mailto:bds@recbook.com">bds@recbook.com</a>
                </div>
                <div className="recFooter__info">
                  <div className="mr-3 d-inline-block">
                    <img src="/icons/ico-hotline-white.svg" alt="rec-hotline" />
                  </div>
                  <a className="recFooter__text" key="phone" href="tel:0989186179">0989 186 179</a>

                </div>
              </div>
            </Col>
            <Col xs={12} sm={4} md={2} className="mb-3 mb-lg-0">
              <div className="recFooter_about-title-area">
                <p className="recFooter_about-title">
                  CÔNG TY
                </p>
              </div>
              <div className="recFooter_about-area">
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Giới thiệu
                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Tuyển dụng
                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Đội ngũ
                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Liên hệ
                    </a>
                  </Link>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={4} md={3} className="mb-3 mb-lg-0">
              <div className="recFooter_about-title-area">
                <p className="recFooter_about-title">
                  THÔNG TIN
                </p>
              </div>
              <div className="recFooter_about-area">
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Quy chế hoạt động
                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Cơ chế giải quyết khiếu nại
                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Chính sách bảo mật thông tin
                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Liên hệ - Gửi yêu cầu
                    </a>
                  </Link>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={4} md={2} className="mb-3 mb-lg-0">
              <div className="recFooter_about-title-area">
                <p className="recFooter_about-title">
                  DỊCH VỤ
                </p>
              </div>
              <div className="recFooter_about-area">
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Đăng tin miễn phí

                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Thông tin quy hoạch

                    </a>
                  </Link>
                </div>
                <div className="mb-3">
                  <Link href="#" passHref>
                    <a className="recFooter_about-link">
                      Thẩm định giá Bất động sản
                    </a>
                  </Link>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="recFooter-copyright">
        <span className="text-white">
          Copyright © 2020 Recbook
        </span>
      </div>
    </>
  )
}

export default RecHeader
