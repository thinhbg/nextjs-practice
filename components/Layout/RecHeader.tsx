import Link from 'next/link'
import * as React from 'react'
import { useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import Button from '../common/Button'
import { EAvatar } from '../common/EImage'

import './styles.css'
import { actionGetLogIn } from '../../redux/home/actions'

const RecHeader = ({ active = 'home' }: any) => {
  const dispatch = useDispatch()
  const [loggedIn, setLoggedIn] = useState(false)

  const handleLogin = async () => {
    setLoggedIn(true)
    dispatch(actionGetLogIn({
      data: {
        logged: true,
      },
    }))
  }
  return (
    <div className="recHeader">
      <Container>
        <Row className="">
          <Col xs={6} sm={6} md={4} className="px-2 d-flex">
            <h1>
              <a href="/">
                <img src="/images/logo.svg" alt="Rec logo" />
              </a>
            </h1>
            <div className="recHeader-text">
              <p className="d-none d-lg-block">CỘNG ĐỒNG BẤT ĐỘNG SẢN</p>
              <h1>
                <p>RECBOOK</p>
              </h1>
            </div>
          </Col>
          <Col xs={6} sm md className="d-none d-md-flex px-2 justify-content-center align-items-center ">
            <div className="mr-2">
              <img src="/icons/ico-email.svg" className="recHeader__info-image" alt="" />
            </div>
            <div className="d-flex flex-column">
              <span>
                Email
              </span>
              <a className="recHeader__info" key="email" href="mailto:bds@recbook.com">bds@recbook.com</a>
            </div>
          </Col>
          <Col xs={6} sm md className="d-none d-md-flex px-2 ml-2 justify-content-center align-items-center ">
            <div className="mr-2">
              <img src="/icons/ico-hotline.svg" className="recHeader__info-image" alt="" />
            </div>
            <div className="d-flex flex-column">
              <span>
                Hotline
              </span>
              <a className="recHeader__info" key="phone" href="tel:0989186179">0989 186 179</a>
            </div>
          </Col>
          <Col xs={6} sm={6} md={4} className="d-flex px-2 justify-content-end align-items-center">
            {
              !loggedIn
                ? (
                  <>
                    <div className="mr-sm-2">
                      <Button.Outline data-cy="header__register__button" text="Đăng ký" btnColor="outline_button-clear" />
                    </div>
                    <div className="ml-sm-2">
                      <Button.Solid data-cy="header__login__button" text="Đăng nhập" btnColor="solid_button-yellow" onClick={handleLogin} />
                    </div>
                  </>
                )
                : (
                  <>
                    <div
                      className="d-flex align-items-center"
                      style={{
                        background: '#F4F7FC',
                        borderRadius: '30px',
                        padding: '0.25rem 0.5rem',
                      }}
                    >
                      <div style={{ width: '47px', height: '47px' }}>
                        <EAvatar src="/images/Avatar-seller.jpg" />
                      </div>
                      <span
                        className="text-center mx-2"
                        style={{
                          fontWeight: 'bold',
                          fontSize: '1rem',
                          color: '#204A9D',
                        }}
                      >
                        CỘNG ĐỒNG BẤT ĐỘNG SẢN RecBook
                      </span>
                      <img src="/icons/ico-cavet-down-blue.svg" alt="cavet-down" />
                    </div>
                  </>
                )
            }
          </Col>
        </Row>
      </Container>
      <div className="recHeader-subMenu">
        <Container>
          <Row>
            <Col xs={12} className="recHeader__subMenu__row">
              <div className={`h-100 recHeader__subMenu__link ${active === 'home' ? 'recHeader__subMenu__row-active' : ''}`}>
                <Link href="/" passHref>
                  <a className={`recHeader__subMenu__row-person ${active === 'home' ? 'font-weight-bold' : ''}`}>
                    Trang cá nhân
                  </a>
                </Link>
              </div>
              <div className={`h-100 recHeader__subMenu__link ${active === 'houses' ? 'recHeader__subMenu__row-active' : ''}`}>
                <Link href="/houses" passHref>
                  <a className={active === 'houses' ? 'font-weight-bold' : ''}>
                    Nhà phố
                  </a>
                </Link>
              </div>
              <div className="h-100 recHeader__subMenu__link">
                <Link href="./projects" passHref>
                  <a className={active === 'projects' ? 'font-weight-bold' : ''}>
                    Dự án
                  </a>
                </Link>
              </div>
              <div className="h-100 recHeader__subMenu__link">
                <Link href="#" passHref>
                  <a>
                    Tiện ích
                  </a>
                </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </div>

    </div>
  )
}

export default RecHeader
