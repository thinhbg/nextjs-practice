import React, { useMemo } from 'react'
import Link from 'next/link'
import CardProfile from '../../common/Card/CardProfile'
import { CustomImage } from '../../common/EImage'
import './styles.css'

const JSonDataCard = {
  name: 'Nguyễn ngọc anh',
  title: 'Chuyên gia môi giới',
  companyName: 'Công ty CP Tân Hoàng Minh Plus',
  yearExp: 4,
  projects: 35,
  friendlist: 235,
  followers: 1774,
}
const JSonDataCard2 = {
  groupMaster: 'Nguyễn Ngọc Ánh',
  name: 'NameCard',
  email: 'anhnn@recbook.vn',
  hotline: '0123 468 579',
  zalo: '0944 448 844',
  web: 'http://batdongsanleoland.com',
  members: '12777',
  membersImg: [
    {
      src: '/images/ImageBietThu.png',
    }, {
      src: '/images/ImageBietThu.png',
    }, {
      src: '/images/ImageBietThu.png',
    }, {
      src: '/images/ImageBietThu.png',
    },
  ],
  projectsSelling: '128',
}

const JSonDataCard3 = {
  title: 'Danh sách dự án của nhóm',
  data: [
    {
      imgSrc: '/images/ImageBietThu.png',
      projectName: 'BDS PLus PlusBDS PLus PlusBDS PLus PlusBDS PLus Plus',
    },
    {
      imgSrc: '/images/ImageBietThu.png',
      projectName: 'BDS PLus Plus',
    },
    {
      imgSrc: '/images/ImageBietThu.png',
      projectName: 'BDS PLus Plus',
    },
    {
      imgSrc: '/images/ImageBietThu.png',
      projectName: 'BDS PLus Plus',
    },
  ],
}

const JSonDataCard4 = {
  title: 'Danh sách dự án của nhóm',
  data: [
    {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'buy',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'buy',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'sell',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'sell',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'buy',
    },
  ],
}

const JsonIncoming = [
  {
    imgSrc: '/images/incoming-img1.png',
  }, {
    imgSrc: '/images/incoming-img2.png',
  }, {
    imgSrc: '/images/incoming-img3.png',
  },
]
const GroupDetailLeftSideBar = () => {
  const JSonDataCard2After = useMemo(() => (
    {
      ...JSonDataCard2,
      updateGroup: true,
    }
  ), [JSonDataCard2])

  return (
    <div>
      <div
        className="grDetailLeft-image"
      >
        <CustomImage src="/images/recbook-wallpaper.jpg" verticalAlignProp="middle" />
      </div>
      <CardProfile
        className="mb-4"
        type={2}
        imageSrc="/images/card-news.png"
        dataSource={JSonDataCard2After}
      />
      <CardProfile
        className="mb-4"
        type={5}
        imageSrc="/images/card-news.png"
        dataSource={JSonDataCard3}
      />
      {' '}
      <CardProfile
        className="mb-4"
        type={4}
        imageSrc="/images/card-news.png"
        dataSource={JSonDataCard4}
      />
      <div className="eSeparator-horizontal my-3" />
      <div className="d-flex justify-content-between flex-wrap px-1">
        <Link href="#">
          <a>
            <span style={{
              fontSize: '0.875rem',
              color: '#B0B0B0',
            }}
            >
              Recbook
            </span>
          </a>
        </Link>
        <Link href="#">
          <a>
            <span style={{
              fontSize: '0.875rem',
              color: '#B0B0B0',
            }}
            >
              Trợ giúp
            </span>
          </a>
        </Link>
        <Link href="#">
          <a>
            <span style={{
              fontSize: '0.875rem',
              color: '#B0B0B0',
            }}
            >
              Liên hệ
            </span>
          </a>
        </Link>
        <Link href="#">
          <a>
            <span style={{
              fontSize: '0.875rem',
              color: '#B0B0B0',
            }}
            >
              Điều khoản
            </span>
          </a>
        </Link>
      </div>
    </div>
  )
}
export default GroupDetailLeftSideBar
