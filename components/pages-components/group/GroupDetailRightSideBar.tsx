import React from 'react'
import { EAvatar } from '../../common/EImage'
import './styles.css'

const JsonHot = [
  {
    imgSrc: '/images/group-project-01.png',
  }, {
    imgSrc: '/images/group-project-01.png',
  }, {
    imgSrc: '/images/hot-img1.png',
  },
]

const suggestMembers = [
  {
    name: 'HiSoft HiSoft HiSoft HiSoft HiSoft',
    imgSrc: '/images/Avatar-seller.jpg',
    exp: 8,
  }, {
    name: 'HiSoft HiSoft HiSoft',
    imgSrc: '/images/Avatar-seller.jpg',
    exp: 7,
  }, {
    name: 'HiSoft HiSoft HiSoft',
    imgSrc: '/images/Avatar-seller.jpg',
    exp: 6,
  }, {
    name: 'HiSoft HiSoft HiSoft',
    imgSrc: '/images/Avatar-seller.jpg',
    exp: 5,
  },
]

const GroupDetailRightSideBar = () => (
  <div>
    <div className="grDetail--right--excellent mb-4">
      <div
        className="d-flex flex-column align-items-center justify-content-center position-relative py-4"
        style={{
          background: '#37474F',
          color: 'white',
        }}
      >
        <div
          style={{
            width: '64px',
            height: '64px',
            position: 'absolute',
            bottom: '-2rem',
          }}
        >
          <EAvatar
            src="/images/Avatar-seller.jpg"
            className="object-fit-cover"
            borderCustom="2px solid var(--color-green-300)"
          />
        </div>
        <p
          className="text-uppercase font-weight-bold"
          style={{ fontSize: '14px' }}
        >
          THÀNH VIÊN SUẤT SẮC CỦA THÁNG
        </p>
        <p
          className="font-weight-bold"
          style={{
            fontSize: '14px',
            textDecoration: 'underline',
          }}
        >
          HiSoft
        </p>
      </div>
      <div
        className="d-flex  align-items-center justify-content-around py-4"
        style={{
          background: 'white',
          borderRadius: '0px 0px 10px 10px',
          color: '#37474F',
        }}
      >
        <div className="d-flex flex-column align-items-center w-50">
          <span
            style={{
              fontWeight: 'bold',
              fontSize: '3rem',
              color: '#37474F',
            }}
          >
            123
          </span>
          <span
            style={{
              fontWeight: 'bold',
              fontSize: '1.125rem',
            }}
          >
            Chốt Sale
          </span>
        </div>
        <div className="eSeparator-vertical" style={{ height: '5rem' }} />
        <div className="d-flex flex-column align-items-center w-50">
          <span
            style={{
              fontWeight: 'bold',
              fontSize: '3rem',
              color: '#37474F',
            }}
          >
            1515
          </span>
          <span
            style={{
              fontWeight: 'bold',
              fontSize: '1.125rem',
            }}
          >
            Bài đăng
          </span>
        </div>
      </div>
    </div>

    <div className="grDetail--grProject">
      <p
        className="font-weight-bold text-center text-uppercase mb-3"
        style={{
          fontSize: '0.875rem',
          color: '#204A9D',
        }}
      >
        Dự án mới của nhóm
      </p>
      {
       JsonHot?.slice(0, 2).map((item, index) => (
         <div
           key={index}
           className="overflow-hidden mb-3"
           style={{
             border: ' 3px solid #FFFFFF',
             boxShadow: '0px 0px 10px 5px rgba(0, 0, 0, 0.1)',
             borderRadius: '10px',
           }}
         >
           <img src={item.imgSrc} alt={item.imgSrc} className="object-fit-cover" />

         </div>
       ))
       }
      <div className="d-flex justify-content-center align-items-center mb-4">
        <span
          className="mr-3 text-center"
          style={{
            fontSize: '1.125rem',
            color: '#0F4D98',
          }}
        >
          Xem tất cả
        </span>
        <img src="/icons/ico-dropdown.svg" alt="dropdown" />
      </div>
      <div className="eSeparator-horizontal mb-4" />
      <div className="">
        <p
          className="font-weight-bold text-center mb-3"
          style={{
            fontSize: '0.875rem',
            color: '#204A9D',
          }}
        >
          Đề xuất thêm thành viên
        </p>
        <div className="grDetail--suggestMembers">
          <div className="p-4">
            {
                    suggestMembers?.map((item:any, index:number) => (
                      <div className="grDetail__suggestMembers--item" key={index}>
                        <div className="flex-shrink-0" style={{ width: '4rem' }}>
                          <EAvatar src={item?.imgSrc} />
                        </div>
                        <div className="flex-grow-1">
                          <p
                            className="mb-0"
                            style={{
                              fontWeight: 'bold',
                              color: '#204A9D',
                            }}
                          >
                            {item?.name}
                          </p>
                          <p
                            className="mb-0"
                            style={{
                              color: 'rgba(32, 74, 157, 0.5)',
                              fontSize: '0.875rem',
                            }}
                          >
                            {item?.exp}
                            {' '}
                            năm kinh nghiệm
                          </p>
                        </div>
                        <div className="flex-shrink-0">
                          <img src="/icons/ico-add-friend-blue.svg" alt="add-friend-blue" className="object-fit-cover" />
                        </div>
                      </div>
                    ))
                }
          </div>

          <div className="eSeparator-horizontal" />
          <div
            className="d-flex align-items-center justify-content-center p-3 color-toryBlue "
            onClick={() => {
              console.log('CLick')
            }}
          >
            Tìm thêm
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default GroupDetailRightSideBar
