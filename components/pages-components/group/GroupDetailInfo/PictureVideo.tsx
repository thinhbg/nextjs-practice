import React, { useMemo, useState } from 'react'
import './GroupDetailInfo.css'
import { Col, Row } from 'react-bootstrap'
import ImageGallery from 'react-image-gallery'
import EModal from '../../../common/EModal'

const JSonImages = [
  {
    image: {
      src: '/images/houseSlideBackground.png',

    },
  }, {
    image: {
      src: '/images/hot-img1.png',

    },
  }, {
    image: {
      src: '/images/ImageBietThu.png',

    },
  }, {
    image: {
      src: '/images/ImageBietThu.png',

    },
  }, {
    image: {
      src: '/images/houseSlideBackground.png',

    },
  }, {
    image: {
      src: '/images/houseSlideBackground.png',

    },
  }, {
    image: {
      src: '/images/houseSlideBackground.png',

    },
  },
]

const PictureVideo = () => {
  const [showModal, setShowModal] = useState(false)

  const imagesArray = useMemo(() => JSonImages.map((item:any) => ({ original: item?.image?.src, thumbnail: item?.image?.src })), [JSonImages])

  return (
    <div className="grDetail__tab--info grDetail__tab--info-picVid">
      <p className="picVid__title">Hình ảnh/Video</p>
      <Row>
        {
            JSonImages?.map((item: any, index: number) => (
              <Col key={index} xs={6} lg={4} className="p-2 picVid__col">
                <img className="object-fit-cover" src={item?.image?.src} alt="item?.image?.src" />
              </Col>
            ))
          }
      </Row>
      <div className="picVid__seeAll">
        <span
          style={{ cursor: 'pointer' }}
          onClick={() => {
            setShowModal(true)
          }}
        >
          Xem tất cả
        </span>
      </div>

      <EModal
        title="Ảnh"
        showModal={showModal}
        onHide={() => {
          setShowModal(false)
        }}
        widthHeightProp="maxW-90vw"
      >
        <ImageGallery
          items={imagesArray || []}
          lazyLoad
          showPlayButton={false}

        />
      </EModal>
    </div>
  )
}
export default PictureVideo
