import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import TextTruncate from 'react-text-truncate'
import Items from '../../../common/Items'
import './GroupDetailInfo.css'
import PictureVideo from './PictureVideo'
import Members from './Members'

const detail = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus nunc in auctor viverra dolor integer convallis vestibulum. In lorem tristique bibendum tellus. Adipiscing mattis tincidunt praesent pellentesque ut consectetur et. In mauris aliquam ut convallis ullamcorper amet, pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus nunc in auctor viverra dolor integer convallis vestibulum. In lorem tristique bibendum tellus. Adipiscing mattis tincidunt praesent pellentesque ut consectetur et. In mauris aliquam ut convallis ullamcorper amet, pellentesque.'
const rules = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus nunc in auctor viverra dolor integer convallis vestibulum. In lorem tristique bibendum tellus. Adipiscing mattis tincidunt praesent pellentesque ut consectetur et. In mauris aliquam ut convallis ullamcorper amet, pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus nunc in auctor viverra dolor integer convallis vestibulum. In lorem tristique bibendum tellus. Adipiscing mattis tincidunt praesent pellentesque ut consectetur et. In mauris aliquam ut convallis ullamcorper amet, pellentesque.'
const dataFile = [
  {
    icon: '/icons/ico-media-file.svg',
    content: 'Elementum at ut venenatis quam ',
    type: 'MP3',
    name: 'Devon Lane',
    avatar: '/images/Avatar-seller.jpg',
    time: '16:09 - 29/10/2020',
  },
  {
    icon: '/icons/ico-doc-file.svg',
    content: 'Elementum at ut venenatis quam ',
    type: 'MP3',
    name: 'Devon Lane',
    avatar: '/images/Avatar-seller.jpg',
    time: '16:09 - 29/10/2020',
  },
  {
    icon: '/icons/ico-media-file.svg',
    content: 'Elementum at ut venenatis quam ',
    type: 'MP3',
    name: 'Devon Lane',
    avatar: '/images/Avatar-seller.jpg',
    time: '16:09 - 29/10/2020',
  },
  {
    icon: '/icons/ico-pdf-file.svg',
    content: 'Elementum at ut venenatis quam ',
    type: 'MP3',
    name: 'Devon Lane',
    avatar: '/images/Avatar-seller.jpg',
    time: '16:09 - 29/10/2020',
  },
  {
    icon: '/icons/ico-media-file.svg',
    content: 'Elementum at ut venenatis quam ',
    type: 'MP3',
    name: 'Devon Lane',
    avatar: '/images/Avatar-seller.jpg',
    time: '16:09 - 29/10/2020',
  },
  {
    icon: '/icons/ico-doc-file.svg',
    content: 'Elementum at ut venenatis quam ',
    type: 'MP3',
    name: 'Devon Lane',
    avatar: '/images/Avatar-seller.jpg',
    time: '16:09 - 29/10/2020',
  },
]
interface Props {
  classNameProp?: string
  groupName: string

}

const GroupDetailInfo = ({ classNameProp, groupName }: Props) => {
  const [tab, setTab] = useState(1)
  const [showDetail, setShowDetail] = useState(false)
  const [showRules, setShowRules] = useState(false)

  return (
    <div className={`${classNameProp ? classNameProp : ''}`}>
      <p className="grDetail--nameGroup">
        {groupName}
      </p>
      <div className="grDetail--tabBlock">
        <span
          className={`grDetail-text ${tab === 1 ? 'text_active' : ''}`}
          onClick={() => { setTab(1) }}
        >
          Giới thiệu
        </span>
        <span
          className={`grDetail-text ${tab === 2 ? 'text_active' : ''}`}
          onClick={() => { setTab(2) }}
        >
          Thảo luận
        </span>
        <span
          className={`grDetail-text ${tab === 3 ? 'text_active' : ''}`}
          onClick={() => { setTab(3) }}
        >
          Thành viên
        </span>
        <span
          className={`grDetail-text ${tab === 4 ? 'text_active' : ''}`}
          onClick={() => { setTab(4) }}
        >
          Hình ảnh/Video
        </span>
        <span
          className={`grDetail-text ${tab === 5 ? 'text_active' : ''}`}
          onClick={() => { setTab(5) }}
        >
          File
        </span>
      </div>
      {
        tab === 1 && (
        <div className="grDetail__tab--info">
          <div className="header__tab">
            <Row className="w-100">
              <Col xs={2} sm={2}>
                <img src="/images/avatar-land.png" alt="#" className="avatar-tab" />
              </Col>
              <Col className="detail__header-tab">
                <div className="wrap__name-group">
                  <span className="name__group">ZLAND HẢI PHÒNG • </span>
                  <img src="/icons/ico-lock-blue.svg" alt="#" className="mx-2" />
                  <span>Nhóm Riêng tư</span>
                </div>
                <div className="wrap__edit">
                  <div className="settings__group" style={{ cursor: 'pointer' }}>
                    <img src="/icons/ico-pencil.svg" alt="#" className="mx-2" />
                    <span>Thiết lập nhóm</span>
                  </div>
                  <div className="settings__picture" style={{ cursor: 'pointer' }}>
                    <img src="/icons/ico-picture.svg" alt="#" className="mx-2" />
                    <span>Chỉnh sửa ảnh</span>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
          <div className="detail__tab">
            <Row className="row__admin">
              <Col xs={2} sm={2}>
                <span>Quản trị viên</span>
              </Col>
              <Col className="col__avatar-admin">
                <img src="/images/Avatar-seller.jpg" alt="#" className="avatar__admin" />
                <img src="/images/Avatar-seller.jpg" alt="#" className="avatar__admin" />
                <img src="/images/Avatar-seller.jpg" alt="#" className="avatar__admin" />
              </Col>
            </Row>
            <Row className="row__introduce">
              <Col xs={2} sm={2}>
                <span>Giới thiệu</span>
              </Col>
              <Col className="content__rules">
                {!showDetail ? (
                  <TextTruncate
                    line={4}
                    element="span"
                    truncateText="…"
                    text={detail}
                    textTruncateChild={(
                      <a className="color_yellow" onClick={() => setShowDetail(!showDetail)}>Xem thêm</a>
                )}
                  />
                )
                  : (
                    <>
                      <span>{detail}</span>
                      <a className="color_yellow" onClick={() => setShowDetail(!showDetail)}>
                        {'  '}
                        Thu gọn
                      </a>
                    </>
                  )}
              </Col>
            </Row>
            <Row>
              <Col xs={2} sm={2}>
                <span>Nội quy</span>
              </Col>
              <Col className="content__rules">
                {!showRules ? (
                  <TextTruncate
                    line={4}
                    element="span"
                    truncateText="…"
                    text={rules}
                    textTruncateChild={(
                      <a className="color_yellow" onClick={() => setShowRules(!showRules)}>Xem thêm</a>
                )}
                  />
                )
                  : (
                    <>
                      <span>{rules}</span>
                      <a className="color_yellow" onClick={() => setShowRules(!showRules)}>
                        {'  '}
                        Thu gọn
                      </a>
                    </>
                  )}
              </Col>
            </Row>
          </div>
        </div>
        )
      }
      {
        tab === 5 && (
          <div className="grDetail__tab--file">
            <div className="span__file">
              <span>File</span>
            </div>
            <Row className="head__file">
              <Col lg={7} xs={6}>
                <span>Tên tệp</span>
              </Col>
              <Col lg={1} xs={1}>
                <span>Loại</span>
              </Col>
              <Col lg={4} xs={5} className="text-center">
                <span>Tải lên</span>
              </Col>
            </Row>
            <div className="line__space" />
            {dataFile.map((el: any) => (
              <Row className="content__file">
                <Col lg={7} xs={6}>
                  <img src={el?.icon} alt="#" />
                  <span className="ml-4">{el?.content}</span>
                </Col>
                <Col lg={1} xs={1}>
                  <span>{el?.type}</span>
                </Col>
                <Col lg={4} xs={5} className="pl-4">
                  <Items.UserUpload data={{
                    avatar: el?.avatar,
                    name: el?.name,
                    time: el?.time,
                  }}
                  />
                </Col>
              </Row>
            ))}
            <div className="show__all">
              <a href="" className="link__all">Xem tất cả</a>
            </div>
          </div>
        )
}
      {
          tab === 3 && (
          <Members />
          )
      }
      {
        tab === 4 && (
        <PictureVideo />
        )
      }
    </div>
  )
}

export default GroupDetailInfo
