import React, { useState } from 'react'
import './GroupDetailListProduct.css'
import { ESlider } from '../../../common/ESlider'
import Card from '../../../common/Card'
import Input from '../../../common/Input'
import { EAvatar } from '../../../common/EImage'

const JsonDataSpecial = [
  {
    location: 'Liễu giai, ba đình, Liễu giai, ',
    img: '/images/incoming-img1.png',
    title: 'Bán căn hộ D Palce-full Nội thất 86m2, Yên hoà',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông nam',
    price: 4300000000,
  }, {
    location: 'Liễu giai, ba đình',
    img: '/images/incoming-img1.png',
    title: 'Bán căn hộ D Palce-full Nội thất 86m2, Yên hoà',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông nam',
    price: 4300000000,
  }, {
    location: 'Liễu giai, ba đình',
    img: '/images/incoming-img1.png',
    title: 'Bán căn hộ D Palce-full Nội thất 86m2, Yên hoà',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông nam',
    price: 4300000000,
  }, {
    location: 'Liễu giai, ba đình',
    img: '/images/incoming-img1.png',
    title: 'Bán căn hộ D Palce-full Nội thất 86m2, Yên hoà',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông nam',
    price: 4300000000,
  },
]
const JsonStatus = [
  {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'HI Soft VN',
    img: [{ src: '/images/ImageBietThu.png' }, { src: '/images/houseSlideBackground.png' }, { src: '/images/Avatar-seller.jpg' }, { src: '/images/ImageBietThu.png' }, { src: '/images/ImageBietThu.png' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    room: '3PN',
    bath: '2VS',
    direction: 'Đông Nam',
    describe: 'Turpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magnaTurpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magna',
    price: '4,300,000,000',
    sharePrice: '34,000,000',
    tags: ['giá tốt', 'trung tâm', 'đang bán'],
    likes: 123,
    comments: 234,
    seen: 345,
  }, {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'HI Soft VN 02',
    img: [{ src: '/images/ImageBietThu.png' }, { src: '/images/houseSlideBackground.png' }, { src: '/images/Avatar-seller.jpg' }, { src: '/images/ImageBietThu.png' }, { src: '/images/ImageBietThu.png' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    room: '3PN',
    bath: '2VS',
    direction: 'Đông Nam',
    describe: 'Turpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magna',
    price: '4,300,000,000',
    sharePrice: '34,000,000',
    tags: ['giá tốt', 'trung tâm', 'đang bán'],
    likes: 123,
    comments: 234,
    seen: 345,
  }, {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'HI Soft VN',
    img: [{ src: '/images/ImageBietThu.png' }, { src: '/images/houseSlideBackground.png' }, { src: '/images/Avatar-seller.jpg' }, { src: '/images/ImageBietThu.png' }, { src: '/images/ImageBietThu.png' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    room: '3PN',
    bath: '2VS',
    direction: 'Đông Nam',
    describe: 'Turpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magna',
    price: '4,300,000,000',
    sharePrice: '34,000,000',
    tags: ['giá tốt', 'trung tâm', 'đang bán'],
    likes: 123,
    comments: 234,
    seen: 345,
  },
]

interface Props {
  classNameProp?: string
}

const GroupDetailListProduct = ({ classNameProp }:Props) => (
  <div className={`grDetail--listProduct ${classNameProp ? classNameProp : ''}`}>

    <div className="my-3">
      <div className="d-flex align-items-center">
        <p
          className="font-weight-bold flex-sm-shrink-0 flex-shrink-1 mb-0"
          style={{
            fontSize: '1.5rem',
            color: '#204A9D',
          }}
        >
          Sản phẩm quan trọng của nhóm
        </p>
        <div className="eSeparator-horizontal d-none d-sm-block" />
      </div>
      <ESlider
        className="my-4"
        settingCustom={{
          arrows: true,
          variableWidth: false,
          infinite: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: false,
          swipeToSlide: true,
          swipe: true,
          centerMode: false,
          responsive: [
            {
              breakpoint: 1440,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 991.98,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
        }}
        slickLeftClass="no-left"
        slickRightClass="no-right"
        arrowSize={{ width: '3rem', height: '3rem' }}
      >
        {
          JsonDataSpecial?.map((item:any, index:number) => (
            <div className="px-3" key={index}>
              <Card.House data={item} btnText="Chi tiết" btnColor="outline_button-clear-yellow" />
            </div>
          ))
        }
      </ESlider>
    </div>
    <div className="grDetail--discussion">
      <div className="d-flex flex-column flex-sm-row align-items-center mb-5">
        <p
          className="font-weight-bold flex-shrink-0 mb-0"
          style={{
            fontSize: '1.5rem',
            color: '#204A9D',
          }}
        >
          Thảo luận chung
        </p>
        <div className="eSeparator-horizontal mx-3 d-none d-sm-block" />
        <div className="flex-shrink-0 " style={{ width: '12rem' }}>
          <Input.Select
            optionsList={[]}
            placeholder="Mới nhất"
            className=""
          />
        </div>
      </div>

      <div className="mb-5 bg-white">
        <Input.InputText
          className="recInput-text-status"
          prefix={<EAvatar src="/images/avatar-land.png" width="50px" verticalAlign="middle" />}
          placeholder="Chào Hisoft, bạn có tin tức gì mới không?"
          suffix={(
            <>
              <div className="eSeparator-horizontal d-block d-sm-none" />
              <div className="d-flex align-items-center justify-content-around py-3 py-sm-0">
                <div className="mx-2" style={{ cursor: 'pointer' }}><img src="/icons/ico-stream.svg" alt="stream" /></div>
                <div className="mx-2" style={{ cursor: 'pointer' }}><img src="/icons/ico-camera-blue.svg" alt="camera" /></div>
                <div className="mx-2" style={{ cursor: 'pointer' }}><img src="/icons/ico-attach.svg" alt="attach" /></div>
              </div>
            </>
          )}
        />
      </div>

      {
        JsonStatus?.slice(0, 1)?.map((item, index) => (
          <Card.CardStatus
            key={index}
            classNameCard="mb-4"
            linkMore="#"
            showTag
            showMenu
            data={item}
          />
        ))
      }
      {
        JsonStatus?.slice(0, 1)?.map((item, index) => (
          <Card.CardStream
            key={index}
            classNameCard="mt-5 mb-4"
            linkMore="#"
            showTag
            showMenu
            data={item}
          />
        ))
      }

      {
        JsonStatus?.slice(1)?.map((item, index) => (
          <Card.CardStatus
            key={index}
            classNameCard="mb-4"
            linkMore="#"
            showTag
            showMenu
            data={item}
          />
        ))
      }
    </div>
  </div>
)

export default GroupDetailListProduct
