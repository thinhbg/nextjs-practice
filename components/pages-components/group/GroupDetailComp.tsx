import React from 'react'
import { Col, Row } from 'react-bootstrap'

import GroupDetailLeftSideBar from './GroupDetailLeftSideBar'
import GroupDetailRightSideBar from './GroupDetailRightSideBar'
import SectionFriendOnline from '../../common/SectionFriendOnline'
import GroupDetailInfo from './GroupDetailInfo'
import GroupDetailListProduct from './GroupDetailListProduct'
import './styles.css'

const GroupDetailComp = () => (
  <Row className="w-100 mx-auto px-4" style={{ marginTop: '-5rem' }}>
    <Col xs className="d-none d-xl-block px-0 px-lg-2" style={{ maxWidth: '20rem' }}>
      <GroupDetailLeftSideBar />
    </Col>
    <Col xs className="px-0 px-lg-3">
      <GroupDetailInfo groupName="RecBook Master" />
      <SectionFriendOnline title="Thành viên trực tuyến" />
      <GroupDetailListProduct />
    </Col>
    <Col xs className="d-none d-lg-block px-0 px-lg-2" style={{ maxWidth: '21rem' }}>
      <GroupDetailRightSideBar />
    </Col>
  </Row>
)
export default GroupDetailComp
