import SectionSlide from './ProjectSlide'
import SectionSearch from './ProjectSearch'
import SectionTrend from './ProjectTrend'
import SectionLocalTrend from './ProjectLocalTrend'
import SectionIncentives from './ProjectIncentives'
import SectionHot from './ProjectHot'
import SectionPartners from './ProjectPartners'
import SectionProjectNews from './ProjectProjectNews'

export {
  SectionSlide,
  SectionSearch,
  SectionTrend,
  SectionLocalTrend,
  SectionIncentives,
  SectionHot,
  SectionPartners,
  SectionProjectNews,
}
