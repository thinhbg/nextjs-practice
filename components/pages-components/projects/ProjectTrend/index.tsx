import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import Button from '../../../common/Button'
import Card from '../../../common/Card'
import './styles.css'

const ProjectTrend = () => {
  const [option, setOption] = useState(1)
  return (
    <div className="project__trend">
      {/* TITLE */}
      <div className="d-flex justify-content-center align-items-center container line__text">
        <span className="custom__line eSeparator-horizontal" />
        <p className="mb-0 custom__title">
          CÁC BẤT DỰ ÁN NỔI BẬT
        </p>
        <span className="custom__line eSeparator-horizontal" />

      </div>
      <div className="option__filter">
        <Button.Outline
          text="Mới nhất"
          btnColor="outline_button-blue"
          className={`${option === 1 ? '' : 'btn__inactive'} btn__option`}
          onClick={() => setOption(1)}
        />
        <Button.Outline
          text="Đang bán chạy"
          btnColor="outline_button-blue"
          className={`${option === 2 ? '' : 'btn__inactive'} btn__option`}
          onClick={() => setOption(2)}
        />
      </div>
      <div className="container">

        <Row className="mb-4" noGutters>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
          <Col xl="3" md="6" sm="12" className="mb-4">
            <Card.Project classNameProject="card__project" />
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default ProjectTrend
