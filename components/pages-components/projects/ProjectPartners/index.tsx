import React from 'react'
import { ESlider } from '../../../common/ESlider'
import './styles.css'

const ProjectPartners = () => (
  <div className="project__partners">

    {/* ------------------------------------------------------------------------------------ XEM THÊM CÁC NHÀ MÔI GIỚI */}
    <p className="text-center color-matisse my-5">TỪ CÁC CHỦ ĐẦU TƯ</p>
    <ESlider
      type={2}
      settingCustom={{
        responsive: [
          {
            breakpoint: 1900,
            settings: {
              slidesToShow: 6,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 1140,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 790,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
        ],
      }}
    >
      <div className="px-3">
        <img src="/images/partners1.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners2.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners3.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners4.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners1.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners2.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners3.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners4.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners1.png" alt="images" className="partners_img" />
      </div>
      <div className="px-3">
        <img src="/images/partners2.png" alt="images" className="partners_img" />
      </div>

    </ESlider>

  </div>
)

export default ProjectPartners
