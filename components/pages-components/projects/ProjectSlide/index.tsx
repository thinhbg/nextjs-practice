import React, { useRef, useState } from 'react'
import Slider from 'react-slick'
import Button from '../../../common/Button'
import Card from '../../../common/Card'
import './styles.css'

const ProjectSlide = () => {
  const [currentSlide, setCurrentSlide] = useState(0)

  const JsonData = [
    {
      logo: '/images/logo-project.png',
      img: '/images/slide-project.png',
      location: 'Đại Mỗ, Tây Mỗ, Từ Liêm, Hà Nội',
      direction: 'Nằm trên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch phía Tây thủ đô, Vinhomes Smart City sở hữu vị trí đắc địa, cách Trung tâm Hội nghị Quốc gia chỉ 7 phút di chuyển',
      acreage: 20,
      detail: [
        { value: 'Khu chèo thuyền Kayak ' },
        { value: 'Bệnh viện Đa khoa Quốc tế Vinmec' },
        { value: 'Trường học Quốc tế Vinschool' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Tòa văn phòng...' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
      ],
    },
    {
      logo: '/images/logo-project.png',
      img: '/images/slide-project.png',
      location: 'Đại Mỗ, Tây Mỗ, Từ Liêm, Hà Nội',
      direction: 'Nằm trên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch phía Tây thủ đô, Vinhomes Smart City sở hữu vị trí đắc địa, cách Trung tâm Hội nghị Quốc gia chỉ 7 phút di chuyển',
      acreage: 110,
      detail: [
        { value: 'Khu chèo thuyền Kayak ' },
        { value: 'Bệnh viện Đa khoa Quốc tế Vinmec' },
        { value: 'Trường học Quốc tế Vinschool' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Tòa văn phòng...' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
      ],
    },
    {
      logo: '/images/logo-project.png',
      img: '/images/slide-project.png',
      location: 'Đại Mỗ, Tây Mỗ, Từ Liêm, Hà Nội',
      direction: 'Nằm trên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch phía Tây thủ đô, Vinhomes Smart City sở hữu vị trí đắc địa, cách Trung tâm Hội nghị Quốc gia chỉ 7 phút di chuyển',
      acreage: 480,
      detail: [
        { value: 'Khu chèo thuyền Kayak ' },
        { value: 'Bệnh viện Đa khoa Quốc tế Vinmec' },
        { value: 'Trường học Quốc tế Vinschool' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Tòa văn phòng...' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
      ],
    },
    {
      logo: '/images/logo-project.png',
      img: '/images/slide-project.png',
      location: 'Đại Mỗ, Tây Mỗ, Từ Liêm, Hà Nội',
      direction: 'Nằm trên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch phía Tây thủ đô, Vinhomes Smart City sở hữu vị trí đắc địa, cách Trung tâm Hội nghị Quốc gia chỉ 7 phút di chuyển',
      acreage: 980,
      detail: [
        { value: 'Khu chèo thuyền Kayak ' },
        { value: 'Bệnh viện Đa khoa Quốc tế Vinmec' },
        { value: 'Trường học Quốc tế Vinschool' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Tòa văn phòng...' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
      ],
    },
  ]
  const slick = useRef<any>(null)
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    afterChange: (index: number) => setCurrentSlide(index),
  }
  return (
    <div className="slide_project">
      <div className="content_slide">
        <div
          className="project__arrow-ico left__ico"
          onClick={() => slick.current && slick.current.slickPrev()}
        >
          <img src="/icons/ico-left.svg" alt="left-ico" />
        </div>
        <div
          className="project__arrow-ico right__ico"
          onClick={() => slick.current && slick.current.slickNext()}
        >
          <img src="/icons/ico-right.svg" alt="left-ico" />
        </div>
        <Card.Project
          type={2}
          showImg={false}
          classNameProject="project__card-slide"
          showBorderTop
          classNameContent="project__card-content"
          data={JsonData[currentSlide]}
        />
        <Button.Solid text="KHÁM PHÁ" btnColor="solid_button-yellow" className="w-100 projects__info--button mt-4" />
      </div>
      <Slider ref={slick} {...settings}>
        {JsonData.map((item: any, index: number) => (
          <div className="slide__panel" key={index}>
            <img
              src={item?.img}
              className="img-fluid object-fit-cover"
              alt={item?.img || 'Image'}
            />
          </div>
        ))}
      </Slider>
    </div>
  )
}

export default ProjectSlide
