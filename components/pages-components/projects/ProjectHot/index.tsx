import React from 'react'
import { Col, Row } from 'react-bootstrap'
import Button from '../../../common/Button'
import Card from '../../../common/Card'
import { ESlider } from '../../../common/ESlider'
import './styles.css'

const JsonData = {
  avatarSeller: '/images/Avatar-seller.jpg',
  img: '/images/ImageBietThu.png',
  company: 'Công ty CP Tân Hoàng Minh Plus',
  title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
  address: 'Liễu Giai, Ba Đình, Hà Nội',
  acreage: '120m2',
  bedrooms: '3PN',
  bathrooms: '2VS',
  direction: 'Đông Nam',
  describe: 'Turpis mi amet ornare nulla sapien magna velit. Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit.',
  price: '4,300,000,000',
  nameSeller: 'Bùi Gia Thịnh',
}

const ProjectHot = () => (
  <div className="project__hot">
    {/* TITLE */}
    <div className="d-flex justify-content-center align-items-center container mb-5">
      <span className="custom__line eSeparator-horizontal" />
      <p className="mb-0 custom__title">
        CÁC DỰ ÁN ĐANG HOT
      </p>
      <span className="custom__line eSeparator-horizontal" />

    </div>
    <div className="wrap__hot">
      <Row noGutters className="list__hot">
        <Col md={3}>
          <Card.Project type={2} showCompass={false} />
        </Col>
        <Col md={9} className="m-auto">
          <ESlider
            settingCustom={{
              centerMode: true,
              responsive: [
                {
                  breakpoint: 2000,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                  },
                },
                {
                  breakpoint: 1300,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                  },
                },
              ],
            }}
          >
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
          </ESlider>
        </Col>
      </Row>
      <Row noGutters className="list__hot">
        <Col md={3}>
          <Card.Project type={2} showCompass={false} />
        </Col>
        <Col md={9} className="m-auto">
          <ESlider
            settingCustom={{
              centerMode: true,
              responsive: [
                {
                  breakpoint: 2000,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                  },
                },
                {
                  breakpoint: 1300,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                  },
                },
              ],
            }}
          >
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
            <div className="px-3">
              <Card.House data={JsonData} />
            </div>
          </ESlider>
        </Col>
      </Row>

      <div className="d-flex justify-content-center m-5">
        <Button.Outline text="XEM TẤT CẢ DỰ ÁN" />
      </div>

    </div>
  </div>
)

export default ProjectHot
