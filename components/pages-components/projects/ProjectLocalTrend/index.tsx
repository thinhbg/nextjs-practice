import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import Card from '../../../common/Card'
import { ESlider } from '../../../common/ESlider'
import Input from '../../../common/Input'
import './styles.css'

const JsonData = {
  avatarSeller: '/images/Avatar-seller.jpg',
  img: '/images/ImageBietThu.png',
  company: 'Công ty CP Tân Hoàng Minh Plus',
  title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
  address: 'Liễu Giai, Ba Đình, Hà Nội',
  acreage: '120m2',
  bedrooms: '3PN',
  bathrooms: '2VS',
  direction: 'Đông Nam',
  describe: 'Turpis mi amet ornare nulla sapien magna velit. Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit.',
  price: '4,300,000,000',
  nameSeller: 'Bùi Gia Thịnh',
}
const ProjectLocalTrend = () => {
  const [filter, setFilter] = useState('1')
  const settingCustom = {
    responsive: [
      {
        breakpoint: 1441,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1140,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 790,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  }
  return (
    <div className="project__local__trend position-relative my-5">
      {/* TITLE */}
      <div className="d-flex justify-content-center align-items-center container">
        <span className="custom__line eSeparator-horizontal" />
        <p className="mb-0 custom__title">
          CÁC BẤT ĐỘNG SẢN NỔI BẬT TẠI ĐỊA PHƯƠNG
        </p>
        <span className="custom__line eSeparator-horizontal" />

      </div>

      {/* LIST BĐS */}
      <div>
        <div className="d-flex justify-content-center flex-row">
          <div className="btn__search--filter">
            <Input.Select
              placeholder="Hà Nội"
              optionsList={[
                { label: 'Hà Nội', value: 'Hà Nội' },
                { label: 'Sài Gòn', value: 'Sài Gòn' },
              ]}
            />
          </div>

        </div>

        <div className="d-flex justify-content-center aligh-items-center mb-5">
          <Row xs={2} md={3} xl={6}>
            <Col>
              <div
                className={`filter__item mx-3 ${filter === '1' ? 'active' : ''}`}
                onClick={() => setFilter('1')}
              >
                <img className="filter__item--image" src="images/Rectangle 339.png" alt="" />
                <p className="text-center sub__text color-matisse">Studio</p>
              </div>
            </Col>
            <Col>
              <div
                className={`filter__item mx-3 ${filter === '2' ? 'active' : ''}`}
                onClick={() => setFilter('2')}
              >
                <img className="filter__item--image" src="images/Rectangle 339.png" alt="" />
                <p className="text-center sub__text color-matisse">1 Phòng ngủ</p>
              </div>
            </Col>
            <Col>
              <div
                className={`filter__item mx-3 ${filter === '3' ? 'active' : ''}`}
                onClick={() => setFilter('3')}
              >
                <img className="filter__item--image" src="images/Rectangle 339.png" alt="" />
                <p className="text-center sub__text color-matisse">2 Phòng ngủ</p>
              </div>
            </Col>
            <Col>
              <div
                className={`filter__item mx-3 ${filter === '4' ? 'active' : ''}`}
                onClick={() => setFilter('4')}
              >
                <img className="filter__item--image" src="images/Rectangle 339.png" alt="" />
                <p className="text-center sub__text color-matisse">3 Phòng ngủ</p>
              </div>
            </Col>
            <Col>
              <div
                className={`filter__item mx-3 ${filter === '5' ? 'active' : ''}`}
                onClick={() => setFilter('5')}
              >
                <img className="filter__item--image" src="images/Rectangle 339.png" alt="" />
                <p className="text-center sub__text color-matisse">4 Phòng ngủ</p>
              </div>
            </Col>
            <Col>
              <div
                className={`filter__item mx-3 ${filter === '6' ? 'active' : ''}`}
                onClick={() => setFilter('6')}
              >
                <img className="filter__item--image" src="images/Rectangle 339.png" alt="" />
                <p className="text-center sub__text color-matisse">Penthouse</p>
              </div>
            </Col>
          </Row>
        </div>

        <ESlider
          settingCustom={settingCustom}
          className="mb-5"
        >
          <div className="px-3">
            <Card.House data={JsonData} />
          </div>
          <div className="px-3">
            <Card.House data={JsonData} />
          </div>
          <div className="px-3">
            <Card.House data={JsonData} />
          </div>
          <div className="px-3">
            <Card.House data={JsonData} />
          </div>
          <div className="px-3">
            <Card.House data={JsonData} />
          </div>
          <div className="px-3">
            <Card.House data={JsonData} />
          </div>
          <div className="px-3">
            <Card.House data={JsonData} />
          </div>

        </ESlider>

        <div className="d-flex justify-content-center mb-5">
          <div className="more__all">
            <span>XEM TẤT CẢ</span>
          </div>
        </div>

      </div>
    </div>
  )
}

export default ProjectLocalTrend
