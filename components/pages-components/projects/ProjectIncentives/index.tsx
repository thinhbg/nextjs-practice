import React from 'react'
import Button from '../../../common/Button'
import { ESlider } from '../../../common/ESlider'
import './styles.css'

const ProjectIncentives = () => (
  <div className="project__incentives">
    {/* TITLE */}
    <div className="d-flex justify-content-center align-items-center container mb-5">
      <span className="custom__line eSeparator-horizontal" />
      <p className="mb-0 custom__title">
        ƯU ĐÃI CHỦ ĐẦU TƯ
      </p>
      <span className="custom__line eSeparator-horizontal" />

    </div>

    {/* LIST BĐS */}

    <ESlider
      settingCustom={{
        centerMode: true,
        arrows: false,
        responsive: [
          {
            breakpoint: 2000,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 1300,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
        ],
      }}
    >
      <div className="px-3">
        <img className="promote__image__holder" src="images/project-slide1.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="images/project-slide2.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="images/project-slide3.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="images/project-slide1.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="images/project-slide1.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="images/project-slide1.png" alt="" />
      </div>

    </ESlider>

    {/* BANNER */}
    <div className="project__banner--container">
      <img className="object-fit-cover w-100 h-100" src="images/bg-incentives.png" alt="banner" />
      <img className="projects__image d-none d-xl-block" src="images/image-phone.png" alt="banner" />
      <div className="banner__text">
        <p className="banner__text--top">TIẾP CẬN VỚI 10,000 NHÀ MÔI GIỚI TẠI</p>
        <p className="banner__text--middle">
          MẠNG XÃ HỘI BẤT ĐỘNG SẢN LỚN NHẤT VIỆT NAM
        </p>
        <Button.Solid btnColor="solid_button-yellow" text="ĐĂNG KÝ NGAY" />
      </div>

    </div>

  </div>
)

export default ProjectIncentives
