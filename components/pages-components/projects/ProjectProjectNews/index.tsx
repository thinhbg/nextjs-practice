import React from 'react'
import { Col, Row } from 'react-bootstrap'
import Card from '../../../common/Card'
import './styles.css'

const ProjectProjectNews = () => (
  <div className="project_news">
    {/* TITLE */}
    <div className="d-flex justify-content-center align-items-center container my-5">
      <span className="custom__line eSeparator-horizontal" />
      <p className="mb-0 custom__title">
        TIN DỰ ÁN ĐÁNG CHÚ Ý
      </p>
      <span className="custom__line eSeparator-horizontal" />
    </div>

    <div className="container">
      <Row>
        <Col lg="4" md="6" className="mb-3">
          <div className="text-center houses__news">
            <Card.News />
          </div>
        </Col>
        <Col lg="4" md="6" className="mb-3">
          <div className="text-center houses__news">
            <Card.News />
          </div>
        </Col>
        <Col lg="4" md="6" className="mb-3">
          <div className="text-center houses__news">
            <Card.News />
          </div>
        </Col>
      </Row>
    </div>
    <div className="bg__color-news" />
  </div>
)

export default ProjectProjectNews
