import SectionSlide from './HousesSlide'
import SectionSearch from './HousesSearch'
import SectionsLocalTrend from './HouseLocalTrend'
import SectionBankNews from './HouseBankNews'

export {
  SectionSlide,
  SectionSearch,
  SectionsLocalTrend,
  SectionBankNews,
}
