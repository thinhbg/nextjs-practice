import React, { useState } from 'react'
import InputRange from 'react-input-range'
import { Col, Collapse, Row } from 'react-bootstrap'
import Input from '../../../common/Input'
import Button from '../../../common/Button'
import Card from '../../../common/Card'
import 'react-input-range/lib/css/index.css'
import './styles.css'
import { numberWithCommas } from '../../../../lib/common'

const SearchSection = () => {
  const [showMore, setShowMore] = useState(false)
  const [price, setPrice] = useState({ min: 0, max: 1800000000 })
  const [area, setArea] = useState({ min: 0, max: 250 })
  const [filType, setFilType] = useState('Đang bán')
  const [filPrice, setFilPrice] = useState('Mới nhất')

  return (
    <div className="houses__section--search position-relative my-5">
      <div className="mb-5">
        <div className="d-flex justify-content-center align-items-center py-3">
          <img src="/icons/ico-search.svg" className="mr-3" alt="ico-search" />
          <p className="house__section--title mb-0">
            TÌM KIẾM BẤT ĐỘNG SẢN
          </p>
        </div>

        {/* SEARCH BOX */}
        <div className="d-flex justify-content-center">
          <div className="houses__search--container">
            <Row className="mb-4">
              <Col md={6} lg={{ span: undefined }} className="mb-3">
                <Input.Select
                  optionsList={[]}
                  icon="/icons/ico-city.svg"
                  placeholder="Nhà phố"
                />
              </Col>
              <Col md={6} lg={{ span: undefined }} className="mb-3">
                <Input.Select
                  optionsList={[]}
                  icon="/icons/ico-square.svg"
                  placeholder="Tình/Thành"
                />
              </Col>
              <Col md={6} lg={{ span: undefined }} className="mb-3">
                <Input.Select
                  optionsList={[]}
                  icon="/icons/ico-recOut.svg"
                  placeholder="Quận/Huyện"
                />
              </Col>
              <Col md={6} lg={{ span: undefined }} className="mb-3">
                <Input.Select
                  optionsList={[]}
                  icon="/icons/ico-recIn.svg"
                  placeholder="Phường/Xã"
                />
              </Col>
              <Col sm={12} lg={2} className="d-none d-lg-block">
                <Button.Solid
                  btnColor="solid_button-yellow"
                  text="Tìm kiếm"
                  className="px-0 w-100"
                />
              </Col>
            </Row>

            <Collapse in={showMore}>
              <div
                id="houses__search--collapse"
                className="my-3"
              >
                <Row>
                  <Col>
                    <Row className="">

                      <Col lg={{ span: undefined }} md="6">
                        <div className="d-flex justify-content-center align-items-center mb-3">
                          <img className="mr-3" src="/icons/ico-USD.svg" alt="ico-USD" />
                          <p className="mb-0">Mức giá (vnđ)</p>
                        </div>
                        <InputRange
                          step={100000000}
                          maxValue={1800000000}
                          minValue={0}
                          value={price}
                          formatLabel={value => ''}
                          onChange={(value: any) => setPrice(value)}
                        />
                        <Row className="mt-3">
                          <Col>
                            <Input.InputText
                              className="search__input from mb-3"
                              value={numberWithCommas(price.min).toString()}
                              readOnly
                            />
                          </Col>
                          <Col>
                            <Input.InputText
                              className="search__input to mb-3"
                              value={numberWithCommas(price.max).toString()}
                              readOnly
                            />
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={{ span: undefined }} md="6">
                        <div className="d-flex justify-content-center align-items-center mb-3">
                          <img className="mr-3" src="/icons/ico-area.svg" alt="ico-area" />
                          <p className="mb-0">Diện tích (m2)</p>
                        </div>
                        <InputRange
                          step={25}
                          maxValue={250}
                          minValue={0}
                          value={area}
                          formatLabel={value => ''}
                          onChange={(value: any) => setArea(value)}
                        />
                        <Row className="mt-3">
                          <Col>
                            <Input.InputText
                              className="search__input from mb-3"
                              value={`${area.min.toString()}m2`}
                              onChangeFunc={(value) => {}}
                              readOnly
                            />
                          </Col>
                          <Col>
                            <Input.InputText
                              className="search__input to mb-3"
                              value={`${area.max.toString()}m2`}
                              onChangeFunc={(value) => {}}
                              readOnly
                            />
                          </Col>
                        </Row>
                      </Col>
                      <Col lg={3} xs={12}>
                        <Row>
                          <Col lg="12" md="6" className="mb-4">
                            <Input.Select
                              optionsList={[]}
                              icon="/icons/ico-recIn.svg"
                              placeholder="Số phòng"
                            />
                          </Col>
                          <Col lg="12" md="6" className="mb-4">
                            <Input.Select
                              optionsList={[]}
                              icon="/icons/ico-recIn.svg"
                              placeholder="Hướng nhà"
                            />
                          </Col>
                        </Row>
                      </Col>
                    </Row>

                  </Col>
                  <Col lg={2} xs={12} className="mb-4">
                    <Row className="h-100">
                      <Col sm={6} lg={2} className={showMore ? ' d-lg-none' : 'd-none'}>
                        <Button.Solid
                          btnColor="solid_button-yellow"
                          text="Tìm kiếm"
                          className="px-0 w-100 mb-3 mb-sm-0"
                        />
                      </Col>
                      <Col sm={6} lg={12}>
                        <div className="google_map d-flex justify-content-center align-items-center flex-column h-100">
                          <img src="/icons/ico-googlemap.svg" alt="google-map" className="d-none d-lg-block" />
                          <p className="map__text my-3">Tìm kiếm bằng bản đồ</p>
                        </div>
                      </Col>
                    </Row>

                  </Col>
                </Row>

              </div>
            </Collapse>

            <div
              className="d-flex justify-content-center show__more my-3"
              onClick={() => setShowMore(!showMore)}
              aria-controls="houses__search--collapse"
              aria-expanded={showMore}
            >
              <p className="mb-0 mr-3">
                {showMore ? 'Thu gọn' : 'Tìm kiếm nâng cao'}
              </p>
              <img src={`/icons/${showMore ? 'ico-up-gold.svg' : 'ico-dropdown-gold.svg'}`} alt="drop" />
            </div>
          </div>

        </div>
      </div>

      {/* RESULTS BOX */}
      <div>
        <div className="container px-5 d-flex align-items-center justify-content-between mb-5">
          <div className="d-flex align-items-center">
            <p className="mr-5 mb-0 color-matisse">Loại:</p>
            <Button.Outline
              btnColor={filType === 'Đang bán' ? 'outline_button-yellow' : 'outline_button-blue'}
              onClick={() => setFilType('Đang bán')}
              className="mr-3 btn__search--filter px-4"
              text="Đang bán"
            />
            <Button.Outline
              className="mr-3 btn__search--filter px-4"
              btnColor={filType === 'Tìm mua' ? 'outline_button-yellow' : 'outline_button-blue'}
              onClick={() => setFilType('Tìm mua')}
              text="Tìm mua"
            />
            <Button.Outline
              className="btn__search--filter px-4"
              btnColor={filType === 'Cho thuê' ? 'outline_button-yellow' : 'outline_button-blue'}
              onClick={() => setFilType('Cho thuê')}
              text="Cho thuê"
            />
          </div>
          <div className="d-flex align-items-center ">
            <p className="mb-0 color-matisse">Lọc theo:</p>
            <Button.Outline
              btnColor={filPrice === 'Mới nhất' ? 'outline_button-yellow' : 'outline_button-blue'}
              onClick={() => setFilPrice('Mới nhất')}
              className="ml-5 btn__search--filter px-4"
              text="Mới nhất"
            />
            <Button.Outline
              btnColor={filPrice === 'Giá tốt nhất' ? 'outline_button-yellow' : 'outline_button-blue'}
              onClick={() => setFilPrice('Giá tốt nhất')}
              className="ml-3 btn__search--filter px-4"
              text="Giá tốt nhất"
            />
          </div>
        </div>

        <div className="container">
          <Row className="mb-4">
            <Col xl="4" md="6" sm="12" className="mb-4">
              <Card.House />
            </Col>
            <Col xl="4" md="6" sm="12" className="mb-4">
              <Card.House />
            </Col>
            <Col xl="4" md="6" sm="12" className="mb-4">
              <Card.House />
            </Col>
            <Col xl="4" md="6" sm="12" className="mb-4">
              <Card.House />
            </Col>
            <Col xl="4" md="6" sm="12" className="mb-4">
              <Card.House />
            </Col>
            <Col xl="4" md="6" sm="12" className="mb-4">
              <Card.House />
            </Col>
          </Row>
        </div>

        <div className="container ">
          <p className="text-right color-creamCan pointer" style={{ cursor: 'pointer' }}>
            Xem tất cả &gt;
          </p>
        </div>
      </div>
    </div>
  )
}

export default SearchSection
