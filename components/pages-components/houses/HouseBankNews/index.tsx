import React from 'react'
import { Col, Row } from 'react-bootstrap'
import Card from '../../../common/Card'
import Input from '../../../common/Input'
import Button from '../../../common/Button'
import { ESlider } from '../../../common/ESlider'
import 'react-input-range/lib/css/index.css'
import './styles.css'

const BankNews = () => (
  <div className="bank__news position-relative mt-5 pb-5">
    {/* TITLE */}
    <div className="d-flex justify-content-center align-items-center container mb-5">
      <span className="custom__line eSeparator-horizontal" />
      <p className="mb-0 custom__title">
        ƯU ĐÃI TỪ CÁC NGÂN HÀNG
      </p>
      <span className="custom__line eSeparator-horizontal" />

    </div>

    {/* LIST BĐS */}

    <ESlider
      settingCustom={{
        centerMode: true,
        variableWidth: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 2000,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 1700,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      }}
    >
      <div className="px-3">
        <img className="promote__image__holder" src="/images/image 33.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="/images/image 33.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="/images/image 33.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="/images/image 33.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="/images/image 33.png" alt="" />
      </div>
      <div className="px-3">
        <img className="promote__image__holder" src="/images/image 33.png" alt="" />
      </div>

    </ESlider>

    {/* TITLE */}
    <div className="d-flex justify-content-center align-items-center container my-5">
      <span className="custom__line eSeparator-horizontal" />
      <p className="mb-0 custom__title">
        TIN BĐS ĐÁNG CHÚ Ý
      </p>
      <span className="custom__line eSeparator-horizontal" />
    </div>

    <div className="container">
      <Row>
        <Col lg="4" md="6" className="mb-3">
          <div className="text-center houses__news">
            <Card.News />
          </div>
        </Col>
        <Col lg="4" md="6" className="mb-3">
          <div className="text-center houses__news">
            <Card.News />
          </div>
        </Col>
        <Col lg="4" md="6" className="mb-3">
          <div className="text-center houses__news">
            <Card.News />
          </div>
        </Col>
      </Row>

    </div>

    <p className="text-center color-matisse my-5">Đăng ký nhận tin để không bỏ lỡ các tin tức Bất động sản hot nhất!</p>
    <div className="container my-5 register__for__info">
      <Row className="mb-4">
        <Col>
          <Input.InputText className="houses__button" placeholder="Họ tên" />
        </Col>
        <Col>
          <Input.InputText className="houses__button" placeholder="Email" />
        </Col>
        <Col>
          <Input.InputText className="houses__button" placeholder="Số điện thoại" />
        </Col>
        <Col>
          <Input.Select
            optionsList={[]}
            placeholder="Nhu cầu tìm mua"
            className="house__search--select houses__button"
          />
        </Col>
        <Col sm={2}>
          <Button.Solid
            btnColor="solid_button-blue"
            text="ĐĂNG KÝ"
            className="px-0 w-100"
          />
        </Col>
      </Row>
    </div>

  </div>
)

export default BankNews
