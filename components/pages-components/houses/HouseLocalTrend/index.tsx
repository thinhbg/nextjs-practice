import React, { useState } from 'react'
import Input from '../../../common/Input'
import Card from '../../../common/Card'
import { ESlider } from '../../../common/ESlider'
import 'react-input-range/lib/css/index.css'
import './styles.css'
import Button from '../../../common/Button'

const LocalTrendingSection = () => {
  const [filter, setFilter] = useState('1')
  const settingCustom = {
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1140,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 790,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  }

  return (
    <div className="local__trend position-relative my-5">
      {/* TITLE */}
      <div className="d-flex justify-content-center align-items-center container">
        <span className="custom__line eSeparator-horizontal" />
        <p className="mb-0 custom__title">
          CÁC BẤT ĐỘNG SẢN NỔI BẬT TẠI ĐỊA PHƯƠNG
        </p>
        <span className="custom__line eSeparator-horizontal" />

      </div>

      {/* LIST BĐS */}
      <div>
        <div className="d-flex justify-content-center flex-row">
          <div className="btn__search--filter">
            <Input.Select
              placeholder="Hà Nội"
              optionsList={[
                { label: 'Hà Nội', value: 'Hà Nội' },
                { label: 'Sài Gòn', value: 'Sài Gòn' },
              ]}
            />
          </div>

        </div>

        <div className="d-flex justify-content-center aligh-items-center mb-5">
          <div
            className={`filter__item mx-3 ${filter === '1' ? 'active' : ''}`}
            onClick={() => setFilter('1')}
          >
            <img className="filter__item--image" src="/images/Rectangle 339.png" alt="" />
            <p className="text-center sub__text color-matisse">Nhà mặt phố</p>
          </div>
          <div
            className={`filter__item mx-3 ${filter === '2' ? 'active' : ''}`}
            onClick={() => setFilter('2')}
          >
            <img className="filter__item--image" src="/images/Rectangle 339.png" alt="" />
            <p className="text-center sub__text color-matisse">Nhà trong ngõ</p>
          </div>
          <div
            className={`filter__item mx-3 ${filter === '3' ? 'active' : ''}`}
            onClick={() => setFilter('3')}
          >
            <img className="filter__item--image" src="/images/Rectangle 339.png" alt="" />
            <p className="text-center sub__text color-matisse">Biệt thự</p>
          </div>
          <div
            className={`filter__item mx-3 ${filter === '4' ? 'active' : ''}`}
            onClick={() => setFilter('4')}
          >
            <img className="filter__item--image" src="/images/Rectangle 339.png" alt="" />
            <p className="text-center sub__text color-matisse">Tòa nhà văn phòng</p>
          </div>
        </div>

        <ESlider
          settingCustom={settingCustom}
          className="mb-5"
        >
          <div className="px-3">
            <Card.House />
          </div>
          <div className="px-3">
            <Card.House />
          </div>
          <div className="px-3">
            <Card.House />
          </div>
          <div className="px-3">
            <Card.House />
          </div>
          <div className="px-3">
            <Card.House />
          </div>
          <div className="px-3">
            <Card.House />
          </div>
          <div className="px-3">
            <Card.House />
          </div>

        </ESlider>

        <div className="d-flex justify-content-center mb-5">
          <Button.Outline text="XEM TẤT CẢ BĐS" />
        </div>

        {/* BANNER */}
        <div className="house__banner--container mb-5">
          <img className="object-fit-cover w-100 h-100" src="/images/BannerHouses.png" alt="banner" />
          <img className="houses__image d-none d-xl-block" src="/images/phoi-canh-kien-truc.png" alt="banner" />

          <div className="banner__text">
            <p className="banner__text--top">TRANG KẾT NỐI MÔI GIỚI</p>
            <p className="banner__text--middle">BẤT ĐỘNG SẢN NHÀ ĐẤT</p>
            <p className="banner__text--bottom">LỚN NHẤT VIỆT NAM</p>
            <Button.Solid btnColor="solid_button-yellow" text="ĐĂNG KÝ NGAY" />
          </div>

        </div>

        {/* ------------------------------------------------------------------------------------ TIN TỨC TỪ CÁC NHÀ MÔI GIỚI UY TÍN */}
        <div className="d-flex justify-content-center align-items-center container mb-5">
          <span className="custom__line eSeparator-horizontal" />
          <p className="mb-0 custom__title">
            TIN TỪ CÁC NHÀ MÔI GIỚI UY TÍN
          </p>
          <span className="custom__line eSeparator-horizontal" />
        </div>
        <ESlider
          settingCustom={{
            responsive: [
              {
                breakpoint: 1900,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: 1140,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: 790,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
            ],
          }}
        >
          <div className="px-3">
            <Card.House type={2} />
          </div>
          <div className="px-3">
            <Card.House type={2} />
          </div>
          <div className="px-3">
            <Card.House type={2} />
          </div>
          <div className="px-3">
            <Card.House type={2} />
          </div>
          <div className="px-3">
            <Card.House type={2} />
          </div>
          <div className="px-3">
            <Card.House type={2} />
          </div>

        </ESlider>

        {/* ------------------------------------------------------------------------------------ XEM THÊM CÁC NHÀ MÔI GIỚI */}
        <p className="text-center color-matisse my-5">Xem thêm các Nhà Môi Giới khác</p>
        <ESlider
          settingCustom={{
            responsive: [
              {
                breakpoint: 1900,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: 1140,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: 790,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
            ],
          }}
        >
          <div className="px-3">
            <Card.Seller />
          </div>
          <div className="px-3">
            <Card.Seller />
          </div>
          <div className="px-3">
            <Card.Seller />
          </div>
          <div className="px-3">
            <Card.Seller />
          </div>
          <div className="px-3">
            <Card.Seller />
          </div>
          <div className="px-3">
            <Card.Seller />
          </div>

        </ESlider>

      </div>
    </div>
  )
}

export default LocalTrendingSection
