import React, { useRef, useState } from 'react'
import './styles.css'
import Slider from 'react-slick'
import Button from '../../../common/Button'

const SliderSection = () => {
  const [currentSlide, setCurrentSlide] = useState(0)

  const JsonData = [
    {
      image: '/images/houseSlideBackground.png',
      title: 'Biệt thử ngõ Xuân, Cầu Giấy, Hà Nội 1',
      address: '123 Trung Hòa, Cầu Giấy, HN',
      area: 'Diện tích sử dụng: 360m2',
      bed: 2,
      toilet: 2,
      direction: 'Đông Nam',
    },
    {
      image: '/images/houseSlideBackground.png',
      title: 'Biệt thử ngõ Hạ, Cầu Giấy, Hà Nội 2',
      address: '123 Đỗ Quang, Cầu Giấy, HN',
      area: 'Diện tích sử dụng: 360m2',
      bed: 3,
      toilet: 3,
      direction: 'Đông Nam',
    },
    {
      image: '/images/houseSlideBackground.png',
      title: 'Biệt thử ngõ Thu, Cầu Giấy, Hà Nội 3',
      address: '118 Nguyễn Ngọc Nại, Khương Mai, Hà Nội',
      area: 'Diện tích sử dụng: 360m2',
      bed: 4,
      toilet: 4,
      direction: 'Đông Nam',
    },
    {
      image: '/images/houseSlideBackground.png',
      title: 'Biệt thử ngõ Đông, Hà Nội 4',
      address: '123 Chùa bộc, Đống Đa, Hà Nội',
      area: 'Diện tích sử dụng: 360m2',
      bed: 5,
      toilet: 5,
      direction: 'Đông Nam',
    },
  ]
  const slick = useRef<any>(null)
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    afterChange: (index: number) => setCurrentSlide(index),
  }

  return (
    <div className="houses__section--slide position-relative">
      <div className="houses__slide--info">
        <div
          className="house__arrow-ico left__ico"
          onClick={() => slick.current && slick.current.slickPrev()}
        >
          <img src="/icons/ico-left.svg" alt="left-ico" />
        </div>
        <div
          className="house__arrow-ico right__ico"
          onClick={() => slick.current && slick.current.slickNext()}
        >
          <img src="/icons/ico-right.svg" alt="left-ico" />
        </div>
        <div className="mb-3 houses__info">
          <div className="houses__info--title">
            <p className="w-100 text-center mb-0 font-weight-bold">
              {JsonData[currentSlide].title}
            </p>
          </div>
          <div className="houses__info--address d-flex">
            <img className="mr-3" src="/icons/ico-location.svg" alt="address-ico" />
            <span>
              {JsonData[currentSlide].address}
            </span>
          </div>
          <div className="houses__info--area d-flex">
            <img className="mr-3" src="/icons/ico-area.svg" alt="area-ico" />
            <span>
              {JsonData[currentSlide].area}
            </span>
          </div>
          <div className="houses__info--functions d-flex justify-content-between">
            <div className="funtions--info d-flex align-items-center">
              <img src="/icons/ico-bed.svg" alt="area-ico" />
              <p className="my-3 ml-2">
                {JsonData[currentSlide].bed}
                PN
              </p>
            </div>
            <div className="funtions--info d-flex align-items-center">
              <img src="/icons/ico-bath.svg" alt="area-ico" />
              <p className="my-3 ml-2">
                {JsonData[currentSlide].toilet}
                VS
              </p>
            </div>
            <div className="funtions--info d-flex align-items-center">
              <img src="/icons/ico-compass.svg" alt="area-ico" />
              <p className="my-3 ml-2">{JsonData[currentSlide].direction}</p>
            </div>
          </div>
        </div>
        <Button.Solid text="Xem giá" btnColor="solid_button-yellow" className="w-100 houses__info--button" />
      </div>
      <Slider ref={slick} {...settings}>
        {JsonData.map((item: any, index: number) => (
          <div className="slide__panel" key={index}>
            <img
              src={item?.image}
              className="img-fluid object-fit-cover w-100"
              alt={item?.image || 'Image'}
            />
          </div>
        ))}
      </Slider>

      <div className="houses__section--ads">
        <div className="houses__ads">
          <div className="ads__background">
            <img
              src="/images/background-ads1.png"
              alt="background"
              className="object-fit-cover w-100 h-100"
            />
          </div>
          <h3 className="ads__title">
            ĐĂNG TIN BÁN
            <br />
            NHÀ ĐẤT MIỄN PHÍ
          </h3>
          <Button.Outline
            text="Đăng ngay"
            btnColor="outline_button-yellow"
            className="house__outline-btn"
          />
        </div>
        <div className="houses__ads">
          <div className="ads__background">
            <img
              src="/images/background-ads1.png"
              alt="background"
              className="object-fit-cover w-100 h-100"
            />
          </div>
          <h3 className="ads__title">
            KẾT NỐI TÌM MUA VỚI NHÀ
            <br />
            MÔI GIỚI CHUYÊN NGHIỆP
          </h3>
          <Button.Outline
            text="KẾT NỐI"
            btnColor="outline_button-yellow"
            className="house__outline-btn"
          />
        </div>
        <div className="houses__ads">
          <div className="ads__background">
            <img
              src="/images/background-ads1.png"
              alt="background"
              className="object-fit-cover w-100 h-100"
            />
          </div>
          <h3 className="ads__title">
            CHO THUÊ BẤT ĐỘNG SẢN
            <br />
            NHANH CHÓNG
          </h3>
          <Button.Outline
            text="ĐĂNG TIN"
            btnColor="outline_button-yellow"
            className="house__outline-btn"
          />
        </div>
      </div>
    </div>
  )
}

export default SliderSection
