import React from 'react'
import { Col, Row } from 'react-bootstrap'
import SectionFriendOnline from '../../../common/SectionFriendOnline/SectionFriendOnline'
import SectionLeftSideBar from './SectionLeftSideBar'
import SectionSearch from '../../../common/SectionSearch'
import SectionStory from './SectionStory'
import SectionStatus from './SectionStatus'
import './styles.css'
import SectionRightSideBar from './SectionRightSideBar'

const LoggedIn = () => (
  <div className="px-4 mx-auto">
    <SectionSearch className="my-4 " />
    <Row className="w-100 mx-auto">
      <Col xs className="d-none d-xl-block px-0 px-lg-2" style={{ maxWidth: '20rem' }}>
        <SectionLeftSideBar />
      </Col>
      <Col xs className="px-0 px-lg-3">
        <SectionStory className="mb-5" />
        <SectionFriendOnline title="Bạn bè trực tuyến" />
        <SectionStatus />

      </Col>
      <Col xs className="d-none d-lg-block px-0 px-lg-2" style={{ maxWidth: '21rem' }}>
        <SectionRightSideBar />
      </Col>
    </Row>
  </div>
)
export default LoggedIn
