import React from 'react'
import Link from 'next/link'
import CardProfile from '../../../common/Card/CardProfile/CardProfile'
import { CustomImage } from '../../../common/EImage'

const JSonDataCard = {
  name: 'Nguyễn ngọc anh',
  title: 'Chuyên gia môi giới',
  companyName: 'Công ty CP Tân Hoàng Minh Plus',
  yearExp: 4,
  projects: 35,
  friendlist: 235,
  followers: 1774,
}
const JSonDataCard2 = {
  name: 'NameCard',
  email: 'anhnn@recbook.vn',
  hotline: '0123 468 579',
  zalo: '0944 448 844',
  web: 'http://batdongsanleoland.com',
}

const JSonDataCard3 = {
  title: 'Nhóm đang tham gia',
  data: [
    {
      imgSrc: '/images/ImageBietThu.png',
      nameGroup: 'BDS PLus PlusBDS PLus PlusBDS PLus PlusBDS PLus Plus',
      members: 1234,
      membersImg: [
        {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        },
      ],
    },
    {
      imgSrc: '/images/ImageBietThu.png',
      nameGroup: 'BDS PLus Plus',
      members: 1234,
      membersImg: [
        {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        },
      ],
    },
    {
      imgSrc: '/images/ImageBietThu.png',
      nameGroup: 'BDS PLus Plus',
      members: 1234,
      membersImg: [
        {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        },
      ],
    },
    {
      imgSrc: '/images/ImageBietThu.png',
      nameGroup: 'BDS PLus Plus',
      members: 1234,
      membersImg: [
        {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        }, {
          src: '/images/ImageBietThu.png',
        },
      ],
    },
  ],
}

const JSonDataCard4 = {
  title: 'Nhóm đang tham gia',
  data: [
    {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'buy',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'buy',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'sell',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'sell',
    }, {
      img: '/images/ImageBietThu.png',
      name: 'Tobiwan odPixel',
      status: 'buy',
    },
  ],
}

const JsonIncoming = [
  {
    imgSrc: '/images/incoming-img1.png',
  }, {
    imgSrc: '/images/incoming-img2.png',
  }, {
    imgSrc: '/images/incoming-img3.png',
  },
]
const SectionLeftSideBar = () => (
  <>
    <CardProfile
      className="mb-4"
      type={1}
      imageSrc="/images/card-news.png"
      dataSource={JSonDataCard}
    />
    <CardProfile
      className="mb-4"
      type={2}
      imageSrc="/images/card-news.png"
      dataSource={JSonDataCard2}
    />
    <CardProfile
      className="mb-4"
      type={3}
      imageSrc="/images/card-news.png"
      dataSource={JSonDataCard3}
    />
    <CardProfile
      className="mb-4"
      type={4}
      imageSrc="/images/card-news.png"
      dataSource={JSonDataCard4}
    />
    <p style={{
      color: '#204A9D',
      fontWeight: 'bold',
      fontSize: '0.875rem',
      textAlign: 'center',
      textTransform: 'uppercase',
    }}
    >
      Các dự án mới ra mắt
    </p>
    <div className="overflow-hidden mb-3">
      {
        JsonIncoming?.map((item, index) => (
          <CustomImage
            key={index}
            src={item.imgSrc}
            className="mb-3"
          />
        ))
      }
    </div>
    <div className="eSeparator-horizontal my-3" />
    <div className="d-flex justify-content-between flex-wrap px-1">
      <Link href="#">
        <a>
          <span style={{
            fontSize: '0.875rem',
            color: '#B0B0B0',
          }}
          >
            Recbook
          </span>
        </a>
      </Link>
      <Link href="#">
        <a>
          <span style={{
            fontSize: '0.875rem',
            color: '#B0B0B0',
          }}
          >
            Trợ giúp
          </span>
        </a>
      </Link>
      <Link href="#">
        <a>
          <span style={{
            fontSize: '0.875rem',
            color: '#B0B0B0',
          }}
          >
            Liên hệ
          </span>
        </a>
      </Link>
      <Link href="#">
        <a>
          <span style={{
            fontSize: '0.875rem',
            color: '#B0B0B0',
          }}
          >
            Điều khoản
          </span>
        </a>
      </Link>
    </div>

  </>
)

export default SectionLeftSideBar
