import React from 'react'
import Card from '../../../../common/Card'
import './SectionStatus.css'
import { ESlider } from '../../../../common/ESlider'
import CardSpecial from '../../../../common/Card/CardSpecial'

interface Props {}
const JsonStatus = [
  {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'HI Soft VN',
    img: [{ src: '/images/ImageBietThu.png' }, { src: '/images/houseSlideBackground.png' }, { src: '/images/Avatar-seller.jpg' }, { src: '/images/ImageBietThu.png' }, { src: '/images/ImageBietThu.png' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    room: '3PN',
    bath: '2VS',
    direction: 'Đông Nam',
    describe: 'Turpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magna',
    price: '4,300,000,000',
    sharePrice: '34,000,000',
    tags: ['giá tốt', 'trung tâm', 'đang bán'],
    likes: 123,
    comments: 234,
    seen: 345,
  }, {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'HI Soft VN 02',
    img: [{ src: '/images/ImageBietThu.png' }, { src: '/images/houseSlideBackground.png' }, { src: '/images/Avatar-seller.jpg' }, { src: '/images/ImageBietThu.png' }, { src: '/images/ImageBietThu.png' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    room: '3PN',
    bath: '2VS',
    direction: 'Đông Nam',
    describe: 'Turpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magna',
    price: '4,300,000,000',
    sharePrice: '34,000,000',
    tags: ['giá tốt', 'trung tâm', 'đang bán'],
    likes: 123,
    comments: 234,
    seen: 345,
  }, {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'HI Soft VN',
    img: [{ src: '/images/ImageBietThu.png' }, { src: '/images/houseSlideBackground.png' }, { src: '/images/Avatar-seller.jpg' }, { src: '/images/ImageBietThu.png' }, { src: '/images/ImageBietThu.png' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    room: '3PN',
    bath: '2VS',
    direction: 'Đông Nam',
    describe: 'Turpis mi amet ornar Tristique semper tellus consectetur Tristique semper tellus consectetur Tristique semper tellus consectetur enim eget sede tai nulla sapien magna velit Turpis mi amet ornare nulla sapien magna',
    price: '4,300,000,000',
    sharePrice: '34,000,000',
    tags: ['giá tốt', 'trung tâm', 'đang bán'],
    likes: 123,
    comments: 234,
    seen: 345,
  },
]

const JsonDataSpecial = [
  {
    location: 'Liễu giai, ba đình',
    img: '/images/incoming-img1.png',
    title: 'Bán căn hộ D Palce-full Nội thất 86m2, Yên hoà',
    acreage: '120m2',
    bedrooms: 3,
    bathrooms: 2,
    direction: 'Đông nam',
  }, {
    location: 'Liễu giai, ba đình',
    img: '/images/incoming-img1.png',
    title: 'Bán căn hộ D Palce-full Nội thất 86m2, Yên hoà',
    acreage: '120m2',
    bedrooms: 3,
    bathrooms: 2,
    direction: 'Đông nam',
  }, {
    location: 'Liễu giai, ba đình',
    img: '/images/incoming-img1.png',
    title: 'Bán căn hộ D Palce-full Nội thất 86m2, Yên hoà',
    acreage: '120m2',
    bedrooms: 3,
    bathrooms: 2,
    direction: 'Đông nam',
  },
]
const SectionStatus = ({}:Props) => {
  console.log()
  return (
    <>
      <div>
        {
        JsonStatus?.slice(0, 1)?.map((item, index) => (
          <Card.CardStatus
            key={index}
            classNameCard="mb-3"
            linkMore="#"
            showTag
            showMenu
            data={item}
          />
        ))
      }
      </div>
      <div className="eSeparator-horizontal" style={{ margin: '2.5rem auto' }} />
      <div className="my-3">
        <p
          className="font-weight-bold"
          style={{
            fontSize: '1.5rem',
            color: '#204A9D',
          }}
        >
          Bất động sản đặc biệt
        </p>
        <ESlider
          settingCustom={{
            arrows: true,
            variableWidth: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            swipeToSlide: true,
            swipe: true,
            centerMode: false,
            responsive: [
              {
                breakpoint: 991.98,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: 767.98,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: 575.98,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
            ],
          }}
          slickLeftClass="no-left"
          slickRightClass="no-right"
          arrowSize={{ width: '3rem', height: '3rem' }}
        >
          {
            JsonDataSpecial?.map((item:any, index) => (
              <CardSpecial dataSource={item} key={index} />
            ))
          }
        </ESlider>
      </div>
      <div className="eSeparator-horizontal" style={{ margin: '2.5rem auto' }} />
      <div>
        {
          JsonStatus?.slice(1)?.map((item, index) => (
            <Card.CardStatus
              key={index}
              classNameCard="mb-3"
              linkMore="#"
              showTag
              showMenu
              data={item}
            />
          ))
        }
      </div>
    </>
  )
}

export default SectionStatus
