import React from 'react'
import './styles.css'
import StoryCard from '../../../../common/Card/StoryCard'
import { ESlider } from '../../../../common/ESlider'

const JsonData = [
  {
    borderProp: '2px solid pink',
    cardData: {
      img: '/images/ImageBietThu.png',
      name: 'Hi Soft Soft Hi Soft Soft ',
      title: 'Tìm mua',
      titleBackground: 'var(--color-green-300)',

    },
  }, {
    borderProp: '2px solid pink',
    cardData: {
      img: '/images/ImageBietThu.png',
      name: 'Hi Soft Soft',
      title: 'Tìm mua',
      titleBackground: 'var(--color-green-300)',

    },
  }, {
    borderProp: '2px solid pink',
    cardData: {
      img: '/images/ImageBietThu.png',
      name: 'Hi Soft Soft',
      title: 'Tìm mua',
      titleBackground: 'var(--color-green-300)',

    },
  }, {
    borderProp: '2px solid pink',
    cardData: {
      img: '/images/ImageBietThu.png',
      name: 'Hi Soft Soft',
      title: 'Tìm mua',
      titleBackground: 'var(--color-green-300)',

    },
  }, {
    borderProp: '2px solid var(--color-green-300)',
    cardData: {
      img: '/images/ImageBietThu.png',
      name: 'Hi Soft Soft',
      title: 'Tìm mua',
      titleBackground: 'var(--color-green-300)',

    },
  }, {
    borderProp: '2px solid var(--color-green-300)',
    cardData: {
      img: '/images/ImageBietThu.png',
      name: 'Hi Soft Soft',
      title: 'Tìm mua',
      titleBackground: 'var(--color-green-300)',

    },
  },
]

interface Props {
  className?: string
}

const SectionStory = ({ className }:Props) => (
  <div className={`${className ? className : ''}`} style={{ height: 300 }}>
    <ESlider
      settingCustom={{
        arrows: false,
        variableWidth: false,
        infinite: false,
        slidesToShow: 5,
        centerMode: false,
        autoplay: false,
        swipeToSlide: true,
        swipe: true,
        responsive: [
          {
            breakpoint: 1919.98,
            settings: {
              slidesToShow: 4,
            },
          }, {
            breakpoint: 1439.98,
            settings: {
              slidesToShow: 3,
            },
          }, {
            breakpoint: 1365.98,
            settings: {
              slidesToShow: 2,
            },
          },
        ],
      }}
    >
      {
        JsonData?.map((item, index) => (
          <StoryCard
            key={index}
            borderProp={item.borderProp}
            cardData={item.cardData}
            classNameProp="mx-2"
            width="205px"
            height="300px"
          />
        ))
      }
    </ESlider>

  </div>
)

export default SectionStory
