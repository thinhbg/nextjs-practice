import React from 'react'
import { CustomImage } from '../../../common/EImage'
import Card from '../../../common/Card'

const JsonHot = [
  {
    imgSrc: '/images/hot-img1.png',
  }, {
    imgSrc: '/images/hot-img2.png',
  }, {
    imgSrc: '/images/hot-img1.png',
  },
]
const SectionRightSideBar = () => (
  <>
    <div className="d-flex align-items-center mb-3">
      <p
        className="mb-0"
        style={{
          color: '#204A9D',
          fontWeight: 'bold',
          fontSize: '0.875rem',
          width: '7rem',
        }}
      >
        Quảng cáo
      </p>
      <div className="eSeparator-horizontal" />
    </div>
    <div className="overflow-hidden mb-3">
      {
        JsonHot?.slice(0, 2).map((item, index) => (
          <Card.House classNameCard="mb-3" btnColor="outline_button-clear-yellow" btnText="Chi tiết" />
        ))
      }
    </div>
    <div className="d-flex align-items-center mb-3">
      <p
        className="mb-0"
        style={{
          color: '#204A9D',
          fontWeight: 'bold',
          fontSize: '0.875rem',
          width: '9rem',
        }}
      >
        Dự án nổi bật
      </p>
      <div className="eSeparator-horizontal" />
    </div>
    <div className="overflow-hidden mb-3">
      {
        JsonHot?.slice(0, 2).map((item, index) => (
          <CustomImage
            key={index}
            src={item.imgSrc}
            className="mb-3"
          />
        ))
      }
    </div>
  </>
)

export default SectionRightSideBar
