import React, { useState } from 'react'
import Button from '../../../common/Button'
import FormAuthentication from './FormAuthentication'
import HomeSlide from './HomeSlide'
import Pricing from './Pricing'
import RealEstateTrending from './RealEstateTrending'
import Register from './Register'
import Search from './Search'
import './styles.css'

const Login = () => {
  const [searchResult, setSearchResult] = useState(false)
  return (
    <div className="home-not-logged-in">
      <HomeSlide />
      <Search visibleProp={searchResult} />
      <FormAuthentication />
      <Register />
      <Pricing />
      <RealEstateTrending />
      <div className="home__register__button-login">
        <Button.Solid
          data-cy="home__register_button"
          text="Đăng ký ngay"
          btnColor="solid_button-yellow"
          className="text-uppercase font-weight-bold font-22 py-md-3"
        />
        <p className="home_register_text">
          ĐỂ CÙNG THAM GIA MẠNG XÃ HỘI BẤT ĐỘNG SẢN HẤP DẪN NHẤT VIỆT NAM
        </p>
      </div>
    </div>
  )
}
export default Login
