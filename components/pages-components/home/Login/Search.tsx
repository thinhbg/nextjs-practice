import Link from 'next/link'
import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import Rating from 'react-rating'
import Button from '../../../common/Button'
import Card from '../../../common/Card'
import { CustomImage, EAvatar } from '../../../common/EImage'
import Input from '../../../common/Input'

interface SearchProps {
  visibleProp: boolean
}

const JsonResultSearch = [
  {
    name: 'This is name 1',
    company: 'Company name 1',
    exp: 5,
    projects: 128,
  },
  {
    name: 'This is name 1',
    company: 'Company name 1',
    exp: 5,
    projects: 128,
  },
  {
    name: 'This is name 1',
    company: 'Company name 1',
    exp: 5,
    projects: 128,
  },
  {
    name: 'This is name 1',
    company: 'Company name 1',
    exp: 5,
    projects: 128,
  },
]
const JsonData = [
  {
    value: 'name 1',
    label: 'name 1',
  },
  {
    value: 'name 2',
    label: 'name 2',
  },
  {
    value: 'name 3',
    label: 'name 3',
  },
  {
    value: 'name 4',
    label: 'name 4',
  },
]

const JsonSearchQuickView = {
  avatar: '/images/ImageBietThu.png',
  name: 'HiSoft Com',
  isPro: true,
  exp: 5,
  projects: 128,
  rating: 4,
  isFriend: false,
  featurePost: {
    img: '/images/ImageBietThu.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    address: 'Liễu giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
    price: '4,300,000,000',
  },
}

const Search = ({ visibleProp }:SearchProps) => {
  console.log('Search -> visibleProp', visibleProp)

  const [visible, setVisible] = useState(false)
  const [quickview, setQuickview] = useState(false)
  console.log('Search -> visible', visible)
  // useEffect(() => {
  //   if (visibleProp === false) {
  //     setVisible(false)
  //   } else setVisible(true)
  // }, [visibleProp])

  const viewQuickResult = (key:any) => {
    setVisible(false)
    setQuickview(true)
  }

  return (
    <div className="custom-container-1114 mb-4">
      <div className="home__search">
        <Row>
          <Col xs={12} lg={3} className="d-flex align-items-center home__search__input">
            <Input.InputText
              data-cy="home__select-text"
              onChangeFunc={() => {
                setVisible(true)
              }}
              prefix={<img src="/icons/ico-search.svg" alt="search" />}
              placeholder="Tìm kiếm môi giới"
              className="border-0"
            />
          </Col>
          <Col xs={12} md={4} lg className="d-flex align-items-center px-2 mb-2 mb-md-0">
            <Input.Select
              data-cy="home__select-sale"
              optionsList={JsonData}
              placeholder="Cần bán"
              icon="/icons/ico-tag.svg"
              classNamePrefix="no-border font-1rem"
            />
          </Col>
          <Col xs={12} md={4} lg className="d-flex align-items-center px-2 mb-2 mb-md-0">
            <Input.Select
              data-cy="home__select-personal"
              optionsList={JsonData}
              placeholder="Cá nhân"
              icon="/icons/ico-person.svg"
              classNamePrefix="no-border font-1rem"
            />
          </Col>
          <Col xs={12} md={4} lg className="d-flex align-items-center px-2 mb-2 mb-md-0">
            <Input.Select
              data-cy="home__select-location"
              optionsList={JsonData}
              placeholder="Hà nội"
              icon="/icons/ico-location.svg"
              classNamePrefix="no-border font-1rem"
            />
          </Col>
          <Col xs={12} lg className="d-flex align-items-center justify-content-center px-2">
            <Button.Solid
              text="Tìm kiếm"
              btnColor="solid_button-yellow"
              data-cy="home__search-button"
              onClick={() => {
                console.log('Click')
              }}
            />
          </Col>
        </Row>
      </div>
      {
        visible && JsonResultSearch && (
          <div className="search__result-block">
            {
              JsonResultSearch.map((item, index) => (
                <div className="search-result" key={index}>
                  <div className="search-result-data">
                    <div className="search-result-image">
                      <CustomImage src="/images/RealEstate-1.png" />
                    </div>
                    <span>
                      {item.name}
                    </span>
                    <div className="search-dot-separator" />
                    <span>
                      {item.company}
                    </span>
                    <div className="search-dot-separator" />
                    <span>
                      {item.exp}
                      {' '}
                      năm kinh nghiệm
                    </span>
                    <div className="search-dot-separator" />
                    <span>
                      {item.projects}
                      {' '}
                      dự án đã bán
                    </span>
                  </div>
                  <div
                    className="search-result-button"
                    onClick={() => {
                      console.log('Clicked xem nhanh')
                      viewQuickResult(1)
                    }}
                  >
                    <img src="/icons/ico-eye.svg" alt="eye" />
                    <span>
                      Xem nhanh
                    </span>
                  </div>
                </div>
              ))
            }
          </div>
        )
      }
      {
        quickview && (
          <div className="quickView">
            <img
              className="quickView__moreInfo "
              src="/icons/ico-more-info.svg"
              alt="more info"
              style={{ cursor: 'pointer' }}
              onClick={() => {
                console.log('CLick more')
              }}
            />
            <Row className="quickView-header">
              <Col xs={12} md={12} lg={5} className="d-flex">
                <div className="quickView-header-image">
                  <EAvatar src={JsonSearchQuickView?.avatar} />
                </div>
                <div className="quickView-header-info">
                  <div className="d-flex">
                    <span className="header-info-title">
                      {JsonSearchQuickView.name}
                    </span>
                    {
                      JsonSearchQuickView.isPro && (
                        <div className="header-info-isPro">
                          <span>
                            PRO
                          </span>
                        </div>
                      )
                    }
                  </div>
                  <span className="header-info-subTitle">
                    Công ty CP Tân Hoàng Minh Plus
                  </span>
                </div>
              </Col>
              <Col xs={12} md className="d-flex flex-column">
                <span className="header__info-noUnderline">
                  {JsonSearchQuickView.exp}
                </span>
                <span className="header-info-subTitle">
                  Năm kinh nghiệm
                </span>
              </Col>
              <Col xs={12} md className="d-flex flex-column">
                <span className="header__info-noUnderline">
                  {JsonSearchQuickView.projects}
                </span>
                <span className="header-info-subTitle">
                  Dự án đã bán
                </span>
              </Col>
              <Col xs={12} md={1} />
            </Row>

            <div className="eSeparator-horizontal" />

            <div className="quickView-review">
              <div className="review-rating">
                <span style={{ marginRight: '1rem' }}>
                  Đánh giá
                </span>
                <Rating
                  emptySymbol={<img src="/icons/ico-rating-empty.svg" alt="not-rated" />}
                  fullSymbol={<img src="/icons/ico-rating-rated.svg" alt="rated" />}
                  initialRating={JsonSearchQuickView.rating}
                  readonly={JsonSearchQuickView.rating ? true : false}
                  className="pb-1"
                />
                <span style={{ fontWeight: 'bold', marginLeft: '1rem' }}>
                  {JsonSearchQuickView.rating}
                  /5
                </span>
              </div>

              <div className="eSeparator-vertical d-none d-lg-block" style={{ marginLeft: '2rem', marginRight: '2rem' }} />

              <div className="review__contact">
                <Button.Outline
                  text="Kết bạn"
                  btnColor="outline_button-blue"
                  prefix={
                    JsonSearchQuickView?.isFriend ? (
                      <img src="/icons/ico-add-friend-blue.svg" alt="icon blue" className="mr-4" />
                    ) : (
                      <img src="/icons/ico-add-friend-blue.svg" alt="icon blue" className="mr-4" />
                    )

                }
                  className="px-4 m-2"
                />
                <Button.Outline
                  text="Theo dõi"
                  btnColor="outline_button-blue"
                  prefix={<img src="/icons/ico-eye-blue.svg" alt="icon eye" className="mr-4" />}
                  className="px-4 m-2"
                />
                <Button.Outline
                  text="Nhắn tin"
                  btnColor="outline_button-blue"
                  prefix={<img src="/icons/ico-inbox.svg" alt="icon eye" className="mr-4" />}
                  className="px-4 m-2"
                />
                <Button.Outline
                  text="Gọi điện"
                  btnColor="outline_button-blue"
                  prefix={<img src="/icons/ico-hotline-blue.svg" alt="icon eye" className="mr-4" />}
                  className="px-4 m-2"
                />
              </div>
            </div>

            <div className="eSeparator-horizontal" />

            <div className="quickView-info">
              <p className="quickView-info-title">
                Bài đăng nổi bật
              </p>
              <Card.House classNameCard="rec_card-horizontal" />
            </div>
            <Link href="#" passHref>
              <a className="quickView__button-link">
                <div className="quickView__button">
                  <span data-cy="quickView__button">
                    Xem tất cả
                  </span>
                  <img src="/icons/ico-bigger.svg" alt="ico bigger" className="quickView__button-image" />
                </div>
              </a>
            </Link>
          </div>
        )
      }

    </div>
  )
}

export default Search
