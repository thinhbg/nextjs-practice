import React from 'react'
import Card from '../../../common/Card'
import { ESlider } from '../../../common/ESlider'

const JsonData = [
  {
    avatarSeller: '/images/RealEstate-1.png',
    nameSeller: 'HiSoft',
    img: '/images/ImageBietThu.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    address: 'Liễu giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/RealEstate-1.png',
    nameSeller: 'HiSoft',
    img: '/images/ImageBietThu.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    address: 'Liễu giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/RealEstate-1.png',
    nameSeller: 'HiSoft',
    img: '/images/ImageBietThu.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    address: 'Liễu giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/RealEstate-1.png',
    nameSeller: 'HiSoft',
    img: '/images/ImageBietThu.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    address: 'Liễu giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/RealEstate-1.png',
    nameSeller: 'HiSoft',
    img: '/images/ImageBietThu.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    address: 'Liễu giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet conse Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
    price: '4,300,000,000',
  },

]
const RealEstateTrending = () => {
  console.log()

  return (
    <div className="overflow-hidden realEstate__block">
      <p className="realEstate__title">
        CÁC BẤT ĐỘNG SẢN NỔI BẬT
      </p>
      <ESlider
        className="realEstate__slide"
        settingCustom={{
          infinite: true,
          speed: 500,
          autoplay: true,
          autoplaySpeed: 10000,
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: true,
          responsive: [
            {
              breakpoint: 1440,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              },
            }, {
              breakpoint: 991.98,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 767.98,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 575.98,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
        }}
      >
        {
          JsonData.map((item, index) => (
            <div className="py-3 px-2 realEstate__slide--item background-white" key={index}>
              <Card.House data={item} />
            </div>
          ))
        }
      </ESlider>
    </div>
  )
}

export default RealEstateTrending
