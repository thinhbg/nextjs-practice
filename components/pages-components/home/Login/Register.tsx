import React from 'react'
import { Col, Row } from 'react-bootstrap'
import Button from '../../../common/Button'

const Register = () => {
  console.log()

  return (
    <div className="home_register_block overflow-hidden mt-md-5 mt-2 mb-4">
      <div className="custom-container-1114">
        <p className="register__title-1 text-center text-lg-left">
          MẠNG XÃ HỘI BẤT ĐỘNG SẢN CHUYÊN NGHIỆP
        </p>
        <p className="register__title-2 text-center text-lg-left">
          LỚN NHẤT VIỆT NAM
        </p>
      </div>
      <div className="position-relative">
        <img src="/images/home_registerNow.png" alt="register now" className="register__wallpaper" />
        <div className="custom-container-1114">
          <Row>
            <Col xs={12} lg={6}>
              <div className="py-3 py-md-5">
                <div className="register_info-area mb-3 mb-md-4 ">
                  <img src="/icons/ico-ung-dung-da-nen-tang.svg" alt="ung dung da nen tang" />
                  <span className="register_info-text"> Ứng dụng đa nền tảng - Kết nối mọi nơi</span>
                </div>
                <div className="register_info-area mb-3 mb-md-4">
                  <img src="/icons/ico-tao-dung-ho-so.svg" alt="Tao dung ho so" />
                  <span className="register_info-text"> Tạo dựng hồ sơ NMG chuyên nghiệp - uy tín</span>
                </div>
                <div className="register_info-area mb-3 mb-md-4">
                  <img src="/icons/ico-ket-noi-hang-vang.svg" alt="Ket noi hang vang NMG" />
                  <span className="register_info-text"> Kết nối hàng vạng NMG và các dự án BĐS</span>
                </div>
                <div className="register_info-area mb-3 mb-md-4">
                  <img src="/icons/ico-nen-tang-quan-ly.svg" alt="Nen tang quan ly tai chinh" />
                  <span className="register_info-text"> Nền tảng quản lý tài chính cá nhân NMS</span>
                </div>
                <div className="text-center">
                  <Button.Solid
                    data-cy="Register__register-button"
                    text="Đăng ký ngay"
                    btnColor="solid_button-yellow"
                    className="text-uppercase font-weight-bold mt-md-4"
                  />
                </div>
              </div>
            </Col>
            <Col xs={12} lg={{ span: 6 }} className="position-relative">
              <div className="text-center">
                <img src="/images/phones.png" className="register__info-image" alt="register banner" />
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}

export default Register
