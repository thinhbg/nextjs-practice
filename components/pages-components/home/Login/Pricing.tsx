import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'

const JsonData = [
  {
    image: '/images/pricing-1.png',
    title: '1 triệu',
    subTitle: 'THÀNH VIÊN CHỈ SAU 06 THÁNG RA MẮT',
  },
  {
    image: '/images/pricing-2.png',
    title: '25,000+',
    subTitle: 'SẢN PHẨM BẤT ĐỘNG SẢN ĐƯỢC ĐĂNG TẢI',
  },
  {
    image: '/images/pricing-3.png',
    title: '200+',
    subTitle: 'GIAO DỊCH THÀNH CÔNG MỖI NGÀY',
  },
  {
    image: '/images/pricing-4.png',
    title: '300 Tỷ',
    subTitle: 'TIỀN HOA HỒNG GIAO DỊCH ĐÃ THÀNH CÔNG',
  },
]
const Pricing = () => {
  console.log()

  return (
    <Container className="pricing-block">
      <Row>
        {
        JsonData.map((item, index) => (
          <Col xs={12} sm={6} lg={3} key={index}>
            <div className="pricing__card">
              <img src={item.image} aria-hidden alt="item image" className="pricing__card-image" />
              <p className="pricing__card-title">
                {item?.title}
              </p>
              <p className="pricing__card-subTitle">
                {item?.subTitle}
              </p>
            </div>
          </Col>
        ))
      }
      </Row>
    </Container>
  )
}

export default Pricing
