import React, { useRef } from 'react'
import Slider from 'react-slick'
import './styles.css'

function HomeSlide() {
  const JsonData = [
    {
      image: '/images/ImageBietThu.png',
      title: 'RecBook BdS 1',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  1',
    },
    {
      image: '/images/ImageBietThu.png',
      title: 'RecBook BdS 2',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  2',
    },
    {
      image: '/images/Rectangle45.png',
      title: 'RecBook BdS 3',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  3',
    },
    {
      image: '/images/Rectangle45.png',
      title: 'RecBook BdS 4',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  4',
    },
  ]
  const slick = useRef<any>(null)
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  }
  return (
    <div className="w-100 carousel__area">
      <Slider ref={slick} {...settings}>
        {
          JsonData.map((item, index) => (
            <div className="w-100 h-100 position-relative" key={index}>
              <div className="homeSlide-container">
                <p className="homeSlide__title">{item.title}</p>
                <p className="homeSlide__subTitle">{item.subTitle}</p>
              </div>
              <div className="overlay-gradient" />

              <img
                src={item?.image}
                className="carousel__area-image"
                alt={item?.image || 'Image'}
              />
            </div>
          ))
        }
      </Slider>
    </div>
  )
}

export default HomeSlide
