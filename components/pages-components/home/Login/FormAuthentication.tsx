import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import Slider from 'react-slick'
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik'
import EInput from '../../../common/Input'
import Button from '../../../common/Button'
import { RegisterSchema } from '../../../validate/validator-schema/FormAuthen'

const FormAuthentication = () => {
  const JsonData = [
    {
      image: '/images/tuong-tac-voi-ban-be.svg',
      title: 'RecBook BdS 1',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  1',
    },
    {
      image: '/images/tuong-tac-voi-ban-be.svg',
      title: 'RecBook BdS 2',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  2',
    },
    {
      image: '/images/tuong-tac-voi-ban-be.svg',
      title: 'RecBook BdS 3',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  3',
    },
    {
      image: '/images/tuong-tac-voi-ban-be.svg',
      title: 'RecBook BdS 4',
      subTitle: 'ook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook Sub Recbook  Sub Recbook Sub Recbook  4',
    },
  ]
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  }
  return (
    <Container>
      <p className="formAuth__title">
        ĐĂNG KÝ THAM GIA MẠNG XÃ HỘI NHÀ MÔI GIỚI
        <br />
        TIẾP CẬN HÀNG TRIỆU KHÁCH HÀNG
      </p>
      <Row>
        <Col xs={12} md={6}>
          <div className="form__carousel__area">
            <Slider {...settings}>
              {
                JsonData.map((item, index) => (
                  <React.Fragment key={index}>
                    <div className="w-100 h-100 position-relative">
                      <img
                        src={item?.image}
                        className="object-cover"
                        style={{
                          objectFit: 'cover', width: '100%', height: '100%', zIndex: -1,
                        }}
                        alt={item?.image || 'Image'}
                      />
                    </div>
                    <div>
                      <p className="form__slide-title">
                        {item.title}
                      </p>
                      <p className="form__slide-subTitle">
                        {item.subTitle}
                      </p>
                    </div>
                  </React.Fragment>
                ))
              }
            </Slider>
          </div>
        </Col>
        <Col xs={12} md={6} className="formAuth-form">
          <Formik
            initialValues={{
              fullName: '', email: '', phone: '', company: '', acceptRule: false, exp: '', gender: '',
            }}
            validationSchema={RegisterSchema}
            onSubmit={(values, { setSubmitting }) => {
              setSubmitting(false)
            }}
          >
            {({
              errors, handleSubmit, handleChange, values, setFieldValue,
            }) => (
              <Form className="d-flex flex-column" onSubmit={handleSubmit}>
                <p className="form__field-label">
                  Họ và tên
                </p>
                <Field
                  type="text"
                  value={values.fullName}
                  onChangeFunc={(inputValue:string) => { handleChange('fullName')(inputValue) }}
                  component={EInput.InputText}
                  name="fullName"
                />
                <ErrorMessage name="fullName" component="div" className="error_message" />

                <p className="form__field-label">
                  Số điện thoại
                </p>
                <Field
                  value={values.phone}
                  type="phone"
                  onChangeFunc={(inputValue:string) => { handleChange('phone')(inputValue) }}
                  component={EInput.InputText}
                  name="phone"
                />
                <ErrorMessage name="phone" component="div" className="error_message" />

                <p className="form__field-label">
                  Email
                </p>
                <Field
                  value={values.email}
                  type="email"
                  onChangeFunc={(inputValue:string) => { handleChange('email')(inputValue) }}
                  component={EInput.InputText}
                  name="email"
                />
                <ErrorMessage name="email" component="div" className="error_message" />

                <p className="form__field-label">
                  Đơn vị công tác
                </p>
                <Field
                  value={values.company}
                  type="text"
                  onChangeFunc={(inputValue:string) => { handleChange('company')(inputValue) }}
                  component={EInput.InputText}
                  name="company"
                />
                <ErrorMessage name="company" component="div" className="error_message" />

                <Row>
                  <Col xs={6}>
                    <p className="form__field-label">
                      Giới tính
                    </p>
                    <Field
                      value={values.gender}
                      type="text"
                      onChangeFunc={(inputValue:string) => { handleChange('gender')(inputValue) }}
                      component={EInput.InputText}
                      name="gender"
                    />
                    <ErrorMessage name="gender" component="div" className="error_message" />
                  </Col>
                  <Col xs={6}>
                    <p className="form__field-label">
                      Năm kinh nghiệm
                    </p>
                    <Field
                      value={values.exp}
                      type="text"
                      onChangeFunc={(inputValue:string) => { handleChange('exp')(inputValue) }}
                      component={EInput.InputText}
                      name="exp"
                    />
                    <ErrorMessage name="exp" component="div" className="error_message" />
                  </Col>
                </Row>

                <p className="form__field-label" />
                <EInput.Checkbox
                  label="Tôi đã đọc và đồng ý với Quy định & Điều lệ"
                  name="acceptRule"
                  checked={values.acceptRule}
                  onChangeFunc={(e:any) => {
                    setFieldValue('acceptRule', e.target.checked)
                  }}
                />
                <ErrorMessage name="acceptRule" component="div" className="error_message" />

                <Button.Solid
                  type="submit"
                  disabled={false}
                  text="Đăng ký ngay"
                  className="text-uppercase mt-5"
                  btnColor="solid_button-yellow"
                  borderRadius="10px"
                />
              </Form>
            )}
          </Formik>
        </Col>
      </Row>
    </Container>
  )
}

export default FormAuthentication
