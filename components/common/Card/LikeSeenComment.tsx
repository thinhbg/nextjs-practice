import React, { useMemo, useState } from 'react'
import InputSearch from '../SectionSearch/InputSearch'
import './styles.css'
import { EAvatar } from '../EImage'
import Link from 'next/link'

interface Props {
    likes: number
    comments: number
    seen: number
}

const JsonComment = [
  {
    name: 'HiSoft 1',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n'
            + 'chủ 120m2, full nội thất vào ở ngaychủ 120m2, full nội thất vào ở ngay chủ 120m2, full nội thất vào ở ngay chủ 120m2, full nội thất vào ở ngay chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 2',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 3',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 4',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 5',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 6',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 7',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 8',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 9',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  }, {
    name: 'HiSoft 10',
    image: {
      src: '/images/RealEstate-1.png',
    },
    comment: 'Căn M3-06 Vinhomes Metropolis Liêu Giai, chính\n' + 'chủ 120m2, full nội thất vào ở ngay',
  },
]
const LikeSeenComment = ({
  comments, likes, seen,
}: Props) => {
  const [numberComments, setNumberComments] = useState(2)
  console.log('numberComments', numberComments)
  console.log('JsonComment', JsonComment?.length)

  const numberCommentsAfter = useMemo(() => JsonComment.slice(0, numberComments), [numberComments])
  return (
    <>
      <div className="likeComment-block">
        <div className="like-comment-seen flex-column flex-sm-row ">
          <div className="d-flex">
            <div
              style={{
                width: '27px', height: '27px', marginRight: '1.25rem', cursor: 'pointer',
              }}
              onClick={() => {
                console.log('Click Like - bookmark')
              }}
            >
              <img src="/icons/ico-bookmark-blue.svg" alt="bookmark" />
            </div>
            <div className="ml-4 d-flex d-sm-none align-items-center">
              <div className="d-flex align-items-center mr-2">
                <img
                  src="/icons/ico-like-blue.svg"
                  alt="like"
                  style={{ width: '27px', height: '27px', cursor: 'pointer' }}
                  onClick={() => {
                    console.log('Clicked')
                  }}
                />
                <span
                  className="ml-2"
                  style={{
                    fontWeight: 600,
                    fontSize: '1.125rem',
                    color: 'var(--color-toryBlue)',
                  }}
                >
                  {likes}
                </span>
              </div>
              <div className="d-flex align-items-center mr-2">
                <img
                  src="/icons/ico-comment-blue.svg"
                  alt="comment"
                  style={{ width: '27px', height: '27px', cursor: 'pointer' }}
                  onClick={() => {
                    console.log('Clicked')
                  }}
                />
                <span
                  className="ml-2"
                  style={{
                    fontWeight: 600,
                    fontSize: '1.125rem',
                    color: 'var(--color-toryBlue)',
                  }}
                >
                  {comments}
                </span>
              </div>
              <div className="d-flex align-items-center mr-2">
                <img
                  src="/icons/ico-eye-blue.svg"
                  alt="eye"
                  style={{ width: '27px', height: '27px', cursor: 'pointer' }}
                  onClick={() => {
                    console.log('Clicked')
                  }}
                />
                <span
                  className="ml-2"
                  style={{
                    fontWeight: 600,
                    fontSize: '1.125rem',
                    color: 'var(--color-toryBlue)',
                  }}
                >
                  {seen}
                </span>
              </div>

            </div>

          </div>
          <div className="flex-grow-1 my-2 my-sm-0 w-100">
            <InputSearch
              placeholder="Viết bình luận"
              className="m-0"
            />
          </div>
          <div className="ml-4 d-sm-flex d-none align-items-center">
            <div className="d-flex align-items-center mr-2">
              <img
                src="/icons/ico-like-blue.svg"
                alt="like"
                style={{ width: '27px', height: '27px', cursor: 'pointer' }}
                onClick={() => {
                  console.log('Clicked')
                }}
              />
              <span
                className="ml-2"
                style={{
                  fontWeight: 600,
                  fontSize: '1.125rem',
                  color: 'var(--color-toryBlue)',
                }}
              >
                {likes}
              </span>
            </div>
            <div className="d-flex align-items-center mr-2">
              <img
                src="/icons/ico-comment-blue.svg"
                alt="comment"
                style={{ width: '27px', height: '27px', cursor: 'pointer' }}
                onClick={() => {
                  console.log('Clicked')
                }}
              />
              <span
                className="ml-2"
                style={{
                  fontWeight: 600,
                  fontSize: '1.125rem',
                  color: 'var(--color-toryBlue)',
                }}
              >
                {comments}
              </span>
            </div>
            <div className="d-flex align-items-center mr-2">
              <img
                src="/icons/ico-eye-blue.svg"
                alt="eye"
                style={{ width: '27px', height: '27px', cursor: 'pointer' }}
                onClick={() => {
                  console.log('Clicked')
                }}
              />
              <span
                className="ml-2"
                style={{
                  fontWeight: 600,
                  fontSize: '1.125rem',
                  color: 'var(--color-toryBlue)',
                }}
              >
                {seen}
              </span>
            </div>

          </div>
        </div>
        <div>
          {
                        numberCommentsAfter?.map(item => (
                          <>
                            <div className="d-flex mb-3">
                              <div className="mr-4">
                                <EAvatar src={item?.image?.src} width="50px" height="50px" />
                              </div>
                              <div className="likeSeenComment__peopleComment">
                                <span>
                                  <Link href="">
                                    <a className="peopleComment--name">
                                      <span>
                                        {item?.name}
                                      </span>
                                    </a>
                                  </Link>
                                </span>
                                <span className="peopleComment--comment">
                                  {item?.comment}
                                </span>
                              </div>
                            </div>
                          </>
                        ))
                    }
        </div>
      </div>
      {
        numberComments < JsonComment?.length && (
        <p
          className="likeSeenComment__viewMore-comment"
          onClick={() => {
            setNumberComments(pre => pre + 5)
          }}
        >
          Xem thêm bình luận
        </p>
        )
    }
    </>
  )
}
export default LikeSeenComment
