import React from 'react'
import TextTruncate from 'react-text-truncate'
import { EAvatar } from '../EImage'

interface Props {
  classNameProp?: string
  borderProp?: string
  width?: string
  height?: string
  cardData:{
    img: string
    name: string
    title: string
    titleBackground: string
    titleColor?: string
  }
}

const StoryCard = ({
  classNameProp,
  borderProp,
  cardData,
  width,
  height,
}:Props) => {
  console.log('classNameProp')

  const styleTitle = () => {
    const customStyle = {
      background: '',
      color: 'white',
    }
    if (cardData?.titleBackground) customStyle.background = cardData?.titleBackground
    if (cardData?.titleColor) customStyle.color = cardData?.titleColor
    return customStyle
  }
  const cardStyle = () => {
    const customStyle = {
      width: 'auto',
      height: 'auto',
      border: '2px solid pink',
    }
    if (width) customStyle.width = width
    if (height) customStyle.height = height
    if (borderProp) customStyle.border = borderProp
    return customStyle
  }

  return (
    <div
      className={`storyCard-block ${classNameProp ? classNameProp : ''}`}
      style={cardStyle()}
    >
      <div className="storyCard__head">
        <div style={{ width: '42px' }}>
          <EAvatar src="/icons/ico-avatar-demo.svg" />
        </div>
        <div className="flex-grow-1">
          <TextTruncate
            line={1}
            element="div"
            truncateText="…"
            text={cardData.name}
            containerClassName="storyCard__head-name w-100"
          />
        </div>
      </div>
      <img src="/images/RealEstate-1.png" alt="RealEstate-1" className="storyCard__image" />
      <span className="storyCard__content-title" style={styleTitle()}>
        { cardData.title }
      </span>
    </div>
  )
}

export default StoryCard
