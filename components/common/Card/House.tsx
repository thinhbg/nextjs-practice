import Link from 'next/link'
import React from 'react'
import TextTruncate from 'react-text-truncate'
import Button from '../Button'
import './styles.css'
import { numberWithCommas } from '../../../lib/common'

interface Props {
  classNameCard?: string;
  onBuy?: () => void;
  onClickDetail?: (event: any) => void;
  onClickSeller?: (event: any) => void;
  linkMore?: any;
  type?: number;
  showTag?: Boolean;
  showMenu?: Boolean;
  btnColor?: 'outline_button-yellow' | 'outline_button-clear' | 'outline_button-blue' | 'outline_button-clear-yellow'
  btnText?: string
  data?:{
    avatarSeller?: string;
    img?: string;
    nameSeller?: string;
    company?: string;
    title?: string;
    address?: string;
    acreage?: string;
    bedrooms: string;
    bathrooms: string;
    direction?: string;
    describe?: string;
    price?: string;
  }
}

const House = (props: Props) => {
  const {
    classNameCard = '',
    onBuy,
    onClickDetail,
    onClickSeller,
    linkMore = '#',
    type = 1,
    showTag = true,
    showMenu = true,
    btnColor = 'outline_button-yellow',
    btnText = 'Thương lượng ngay',
    data = {
      avatarSeller: '/images/Avatar-seller.jpg',
      img: '/images/ImageBietThu.png',
      company: 'Công ty CP Tân Hoàng Minh Plus',
      title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
      address: 'Liễu Giai, Ba Đình, Hà Nội',
      acreage: '120m2',
      bedrooms: '3PN',
      bathrooms: '2VS',
      direction: 'Đông Nam',
      describe: '',
      price: '4,300,000,000',
      nameSeller: 'Hi Soft Soft',
    },
  } = props

  return (
    <div className={`rec_card ${classNameCard}`}>
      {type === 1 && data?.nameSeller && data?.avatarSeller ? (
        <div className="rec_card_img">
          <img src={data?.img} alt="images" className="card_img" />
          <div className="rec_card_seller" onClick={onClickSeller}>
            <div className="seller_avatar">
              <img
                src={data?.avatarSeller}
                alt="images"
                className="Avatar_seller"
              />
            </div>
            <span className="ml-2">{data?.nameSeller}</span>
          </div>
        </div>
      ) : (
        <div className="rec_card_img">
          {data?.nameSeller && data?.avatarSeller
          && (
          <div className="rec_header_card">
            <div className="header_seller">
              <div className="header_avatar">
                <img
                  src={data?.avatarSeller}
                  alt="images"
                  className="h_avatar_seller"
                  onClick={onClickSeller}
                />

                <div className="header_info ml-2">
                  <div className="header_name">
                    <span className="text_name" onClick={onClickSeller}>{data?.nameSeller}</span>
                    {showTag ? (
                      <div className="header_tag ml-4">
                        <span className="ml-3 mr-3">PRO</span>
                      </div>
                    ) : null}
                  </div>
                  <div className="header_company">
                    <span>{data?.company}</span>
                  </div>
                </div>
              </div>
            </div>
            {showMenu ? (
              <div className="header_menu">
                <img src="/icons/ico-three-dot.svg" alt="ico-dot" />
              </div>
            ) : null}
          </div>
          )}
          <img src={data?.img} alt="images" className="card_img " />
        </div>
      )}
      <div>
        <div className="rec_card_detail">
          <div className="card_title" onClick={onClickDetail}>
            <span>{data?.title}</span>
          </div>
          <div className="card_address">
            <img src="/icons/ico-locate.svg" alt="ico-locate" />
            <span className="ml-1">{data?.address}</span>
          </div>
          <div className="card_describe mt-2 mb-2">
            <div className="d-flex justify-content-between flex-wrap">
              <div className="d-flex">
                <img src="/icons/ico-acreage.svg" alt="ico-acreage" />
                <span className="ml-1" style={{ fontSize: '0.875rem' }}>{data?.acreage}</span>
              </div>
              <div className="d-flex">
                <img src="/icons/ico-room.svg" alt="ico-room" />
                <span className="ml-1" style={{ fontSize: '0.875rem' }}>{data?.bedrooms}</span>
              </div>
              <div className="d-flex">
                <img src="/icons/ico-bath.svg" alt="ico-bath" />
                <span className="ml-1" style={{ fontSize: '0.875rem' }}>{data?.bathrooms}</span>
              </div>
              <div className="d-flex">
                <img src="/icons/ico-compass.svg" alt="ico-compass" />
                <span className="ml-1" style={{ fontSize: '0.875rem' }}>{data?.direction}</span>
              </div>
            </div>
          </div>
          {
            data?.describe && (
            <div className="card_sub_title">
              <TextTruncate
                line={3}
                element="span"
                truncateText="…"
                text={data?.describe}
                textTruncateChild={(
                  <Link href={linkMore}>
                    <a className="color_yellow">Xem thêm</a>
                  </Link>
                      )}
              />
            </div>
            )
          }
        </div>
        <div className="rec_card_price">
          <div className="label_price">
            <span>Giá</span>
            <span className="number_price ml-2">
              {numberWithCommas(data?.price)}
              đ
            </span>
          </div>
          <Button.Outline
            text={btnText}
            btnColor={btnColor}
            className="card_btn"
            onClick={onBuy}
          />
        </div>
      </div>
    </div>
  )
}

export default House
