import Link from 'next/link'
import React, { useMemo, useState } from 'react'
import TextTruncate from 'react-text-truncate'
import { Col, Row } from 'react-bootstrap'

import ImageGallery from 'react-image-gallery'
import Button from '../Button'
import InputSearch from '../SectionSearch/InputSearch'
import EModal from '../EModal'

import './styles.css'
import LikeSeenComment from './LikeSeenComment'
import TagContact from './TagContact'

interface Props {
  classNameCard?: string;
  onClickDetail?: (event: any) => void;
  onClickSeller?: (event: any) => void;
  linkMore?: any;
  showTag?: Boolean;
  showMenu?: Boolean;
  data?: any
}
// {
//     avatarSeller?: string;
//     img?: any[];
//     nameSeller?: string;
//     company?: string;
//     title?: string;
//     address?: string;
//     acreage?: string;
//     room?: string;
//     bath?: string;
//     direction?: string;
//     describe?: string;
//     price?: string;
//     sharePrice?: string;
//     tags: any[]
//     likes?:number
//     comments?:number
//     seen?:number
//   }
const CardStatus = ({
  classNameCard = '',
  onClickDetail,
  onClickSeller,
  linkMore = '#',
  showTag = true,
  showMenu = true,
  data = {
    avatarSeller: '/images/logo.svg',
    img: [{ src: '/images/logo.svg' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Recbook',
    address: 'LK3.09 Hoàng Huy Riverside Hải Phòng',
    acreage: '0',
    room: '0PN',
    bath: '0VS',
    describe: '',
    direction: '',
    price: '0',
    sharePrice: '0',
    nameSeller: 'Recbook',
    tags: [''],
    likes: 1,
    comments: 1,
    seen: 1,
  },
}: Props) => {
  const [showModal, setShowModal] = useState(false)

  const imagesArray = useMemo(() => data?.img?.map((item:any) => ({ original: item.src, thumbnail: item.src })), [data?.img])

  return (
    <div className={`rec_card ${classNameCard}`}>
      { data?.nameSeller && data?.avatarSeller && (
        <div className="rec_card_img">
          <div className="rec_header_card">
            <div className="header_seller">
              <div className="header_avatar">
                {data?.nameSeller && data?.avatarSeller
                  && (
                    <img
                      src={data?.avatarSeller}
                      alt="images"
                      className="h_avatar_seller"
                      onClick={onClickSeller}
                    />
                  )}
                <div className="header_info ml-2">
                  <div className="header_name">
                    <span className="text_name" onClick={onClickSeller}>{data?.nameSeller}</span>
                    {showTag && (
                      <div className="header_tag ml-4">
                        <span className="ml-3 mr-3">PRO</span>
                      </div>
                    )}
                  </div>
                  <div className="header_company">
                    <span>{data?.company}</span>
                  </div>
                </div>
              </div>
            </div>
            {
              showMenu && (
              <div className="header_menu">
                <img src="/icons/ico-three-dot.svg" alt="ico-dot" />
              </div>
              )
            }
          </div>
        </div>
      )}
      <Row className="w-100 mx-auto">
        <Col xs={12} sm={6} className="pr-0 overflow-hidden">
          <div>
            {
              data?.img?.slice(0, 1).map((item:any, index:number) => (
                <img src={item?.src} alt="images" className="cardStatus_img" onClick={() => (setShowModal(true))} key={index} />
              ))
            }
          </div>
          <div className="d-flex mt-2">
            <div className="w-50 mr-1">
              {
                data?.img?.slice(1, 2).map((item:any, index:number) => (
                  <img src={item?.src} alt="images" className="cardStatus_subImg" onClick={() => (setShowModal(true))} key={index} />
                ))
              }
            </div>
            <div className="w-50 ml-1">
              {
                data?.img?.length <= 3
                  ? data?.img?.slice(2, 3).map((item:any, index:number) => (
                    <img src={item?.src} alt="images" className="cardStatus_subImg" onClick={() => (setShowModal(true))} key={index} />
                  ))
                  : (
                    <div className="cardStatus_subImg remain__image" onClick={() => (setShowModal(true))}>
                      {data?.img?.slice(2, 3).map((item:any, index:number) => (
                        <img src={item?.src} alt="images" className="cardStatus_subImg" key={index} />
                      ))}
                      <div className="overlay-gradient" style={{ display: 'grid', placeItems: 'center' }}>
                        {
                        data?.img?.length - 2
                      }
                      </div>
                    </div>
                  )
              }
            </div>
          </div>
        </Col>
        <Col xs={12} sm={6} className="px-0">
          <div className="rec_card_detail">
            <div className="cardStatus_title">
              <span>{data?.title}</span>
            </div>
            <div className="card_address">
              <img src="/icons/ico-locate.svg" alt="ico-locate" />
              <span className="ml-1">{data?.address}</span>
            </div>
            <div className="cardStatus_describe mt-2 mb-2">
              <div className="d-flex flex-wrap">
                <div className="d-flex w-50 align-items-center">
                  <img src="/icons/ico-acreage.svg" alt="ico-acreage" />
                  <span className="ml-1 flex-grow-1">{data?.acreage}</span>
                </div>
                <div className="d-flex w-50 align-items-center">
                  <img src="/icons/ico-room.svg" alt="ico-room" />
                  <span className="ml-1 flex-grow-1">{data?.room}</span>
                </div>
              </div>
              <div className="d-flex flex-wrap">
                <div className="d-flex w-50 align-items-center">
                  <img src="/icons/ico-bath.svg" alt="ico-bath" />
                  <span className="ml-1 flex-grow-1">{data?.bath}</span>
                </div>
                <div className="d-flex w-50 align-items-center">
                  <img src="/icons/ico-compass.svg" alt="ico-compass" />
                  <span className="ml-1 flex-grow-1">{data?.direction}</span>
                </div>
              </div>
            </div>
            <div className="cardStatus-price d-flex flex-wrap justify-content-between">
              <div className="cardStatus__price">
                <span className="cardStatus__price-text1">Giá: </span>
                <span className="cardStatus__price-text2">
                  {data.price ? data.price : 0}
                  đ
                </span>
              </div>
              <div className="eSeparator-vertical lessThan1440" />
              <div className="cardStatus__price">
                <span className="cardStatus__price-text1">Đơn giá/m2: </span>
                <span className="cardStatus__price-text2">
                  {data.sharePrice ? data.sharePrice : 0}
                  đ
                </span>
              </div>
            </div>
            <div className="eSeparator-horizontal my-3" />
            <div className="card_sub_title">
              <TextTruncate
                line={7}
                element="span"
                truncateText="…"
                text={data?.describe}
                textTruncateChild={(
                  <Link href="/groups/123">
                    <a className="color_yellow">Xem thêm</a>
                  </Link>
                )}
              />
            </div>
          </div>
        </Col>
      </Row>

      <TagContact tags={data?.tags} />

      <div className="eSeparator-horizontal" />

      <LikeSeenComment comments={data?.comments} likes={data?.likes} seen={data?.seen} />

      <EModal
        title="Ảnh"
        showModal={showModal}
        onHide={() => { setShowModal(false) }}
        widthHeightProp="maxW-90vw height90vh"
      >
        <ImageGallery items={imagesArray || []} />
      </EModal>
    </div>
  )
}

export default CardStatus
