import React from 'react'
import './styles.css'

interface Props {
  classNameCard?: string;
  onClickDetail?: (event: any) => void;
  onClickSeller?: (event: any) => void;
  data?:{
    avatarSeller?: string;
    img?: string;
    nameSeller?: string;
    company?: string;
    title?: string;
    address?: string;
    acreage?: string;
    bedrooms?: string;
    bathrooms?: string;
    direction?: string;
    price?: string;
  }
}

const HouseNotification = (props: Props) => {
  const {
    classNameCard = '',
    onClickDetail,
    onClickSeller,
    data = {
      avatarSeller: '/images/Avatar-seller.jpg',
      nameSeller: 'Esther Howard',
      img: '/images/ImageBietThu.png',
      company: 'Công ty CP Tân Hoàng Minh Plus',
      title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
      address: 'Liễu Giai, Ba Đình, Hà Nội',
      acreage: '120m2',
      bedrooms: '3PN',
      bathrooms: '2VS',
      direction: 'Đông Nam',
      price: '4,300,000,000',
    },
  } = props

  return (
    <div className="rec_house_notify">
      <div className="rec_card_seller house_notify" onClick={onClickSeller}>
        <div className="seller_avatar">
          <img
            src={data?.avatarSeller}
            alt="images"
            className="Avatar_seller"
          />
        </div>
        <span className="ml-2">{data?.nameSeller}</span>
      </div>
      <div className={`rec_card ${classNameCard}`}>
        <div className="rec_card_img">
          <img src={data?.img} alt="images" className="card_img" />
        </div>
        <div className="rec_card_detail">
          <div className="card_title" onClick={onClickDetail}>
            <span>{data?.title}</span>
          </div>
          <div className="card_address">
            <img src="/icons/ico-locate.svg" alt="ico-locate" />
            <span className="ml-1">{data?.address}</span>
          </div>
          <div className="card_describe mt-2 mb-2">
            <div className="d-flex justify-content-between flex-wrap">
              <div className="d-flex">
                <img src="/icons/ico-acreage.svg" alt="ico-acreage" />
                <span className="ml-1">{data?.acreage}</span>
              </div>
              <div className="d-flex">
                <img src="/icons/ico-room.svg" alt="ico-room" />
                <span className="ml-1">{data?.bedrooms}</span>
              </div>
              <div className="d-flex">
                <img src="/icons/ico-bath.svg" alt="ico-bath" />
                <span className="ml-1">{data?.bathrooms}</span>
              </div>
              <div className="d-flex">
                <img src="/icons/ico-compass.svg" alt="ico-compass" />
                <span className="ml-1">{data?.direction}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="rec_card_price">
          <div className="label_price">
            <span className="number_price ml-2">{`${data?.price}đ`}</span>
          </div>
          <span onClick={onClickDetail} className="hover_point">Chi tiết</span>
        </div>
      </div>
    </div>
  )
}

export default HouseNotification
