import House from './House'
import Seller from './Seller'
import StoryCard from './StoryCard'
import News from './News'
import CardProfile from './CardProfile/CardProfile'
import CardStatus from './CardStatus'
import Project from './Project'
import HouseNotification from './HouseNotification'
import CardSpecial from './CardSpecial'
import CardStream from './CardStream'
import LikeSeenComment from './LikeSeenComment'
import TagContact from './TagContact'

const Card = {
  House,
  Seller,
  StoryCard,
  News,
  CardProfile,
  CardStatus,
  Project,
  TagContact,
  LikeSeenComment,
  CardStream,
  CardSpecial,
  HouseNotification,
}
export default Card
