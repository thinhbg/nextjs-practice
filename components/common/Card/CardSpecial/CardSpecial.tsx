import React from 'react'
import './CardSpecial.css'
import { CustomImage } from '../../EImage'

interface Props {
  className?: string
  dataSource?: any
}

const CardSpecial = ({
  className = '',
  dataSource = null,
}:Props) => (
  <div className={`cardSpecial ${className}`}>
    <div className="d-flex align-items-center">
      <img src="/icons/ico-location.svg" alt="location" />
      <span style={{
        fontSize: '1.125rem',
        color: '#204A9D',
        marginLeft: '0.5rem',
      }}
      >
        {dataSource?.location}
      </span>
    </div>
    <div className="w-100 overflow-hidden my-3">
      <img className="cardSpecial__image" src={dataSource?.img} alt={dataSource?.img} />
    </div>
    <p style={{
      fontWeight: 'bold',
      fontSize: '1.25rem',
      color: '#204A9D',
    }}
    >
      {dataSource?.title}
    </p>
    <div className="card_describe mt-2 mb-2">
      <div className="d-flex justify-content-between flex-wrap">
        <div className="d-flex">
          <img src="/icons/ico-acreage.svg" alt="ico-acreage" />
          <span className="ml-1">{dataSource?.acreage}</span>
        </div>
        <div className="d-flex">
          <img src="/icons/ico-room.svg" alt="ico-room" />
          <span className="ml-1">{dataSource?.bedrooms}</span>
        </div>
        <div className="d-flex">
          <img src="/icons/ico-bath.svg" alt="ico-bath" />
          <span className="ml-1">{dataSource?.bathrooms}</span>
        </div>
        <div className="d-flex">
          <img src="/icons/ico-compass.svg" alt="ico-compass" />
          <span className="ml-1">{dataSource?.direction}</span>
        </div>
      </div>
    </div>

  </div>
)
export default CardSpecial
