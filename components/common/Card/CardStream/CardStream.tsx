import React, { useMemo, useState } from 'react'

import ImageGallery from 'react-image-gallery'

import './CardStream.css'
import '../styles.css'
import { Col, Row } from 'react-bootstrap'
import Link from 'next/link'
import TextTruncate from 'react-text-truncate'
import EModal from '../../EModal'
import LikeSeenComment from '../LikeSeenComment'
import TagContact from '../TagContact'

interface Props {
    classNameCard?: string;
    onClickDetail?: (event: any) => void;
    onClickSeller?: (event: any) => void;
    linkMore?: any;
    showTag?: Boolean;
    showMenu?: Boolean;
    data?: any
}

const CardStream = ({
  classNameCard = '',
  onClickSeller,
  linkMore = '#',
  showTag = true,
  showMenu = true,
  data = {
    avatarSeller: '/images/logo.svg',
    img: [{ src: '/images/logo.svg' }],
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Recbook Recbook Recbook Recbook  Recbook Recbook Recbook ',
    address: 'LK3.09 Hoàng Huy Riverside Hải Phòng',
    acreage: '0',
    room: '0PN',
    bath: '0VS',
    describe: '',
    direction: '',
    price: '0',
    sharePrice: '0',
    nameSeller: 'Recbook',
    tags: [''],
    likes: 1,
    comments: 1,
    seen: 1,
  },
}: Props) => {
  const [showModal, setShowModal] = useState(false)

  const imagesArray = useMemo(() => data?.img?.map((item:any) => ({ original: item.src, thumbnail: item.src })), [data?.img])

  return (
    <div className={`cardStream ${classNameCard}`}>
      <div
        className="text-center mx-auto"
        style={{
          background: '#FF5C5C',
          borderRadius: '20px',
          width: '12rem',
          color: 'white',
          marginBottom: '-1rem',
          height: '40px',
          transform: 'translateY(-50%)',
          display: 'grid',
          placeItems: 'center',
        }}
      >
        <span>
          Live Stream
        </span>
      </div>
      { data?.nameSeller && data?.avatarSeller && (
        <div className="rec_card_img">
          <div className="rec_header_card">
            <div className="header_seller">
              <div className="header_avatar">
                {data?.nameSeller && data?.avatarSeller
                    && (
                    <img
                      src={data?.avatarSeller}
                      alt="images"
                      className="h_avatar_seller"
                      onClick={onClickSeller}
                    />
                    )}
                <div className="header_info ml-2">
                  <div className="header_name">
                    <span className="text_name" onClick={onClickSeller}>{data?.nameSeller}</span>
                    {showTag && (
                    <div className="header_tag ml-4">
                      <span className="ml-3 mr-3">PRO</span>
                    </div>
                    )}
                  </div>
                  <div className="header_company">
                    <span>{data?.company}</span>
                  </div>
                </div>
              </div>
            </div>
            {
                showMenu && (
                <div className="header_menu">
                  <img src="/icons/ico-three-dot.svg" alt="ico-dot" />
                </div>
                )
            }
          </div>
        </div>
      )}

      <div className="w-100 overflow-hidden cardStream__imageVideo">
        <img
          className="object-fit-cover"
          src="/images/recbook-wallpaper.jpg"
          alt="recbook-wallpaper"
        />
      </div>

      <div
        className="w-100 overflow-hidden "
        style={{
          padding: '2rem 2rem 0',
        }}
      >
        <p
          className="font-weight-bold"
          style={{
            fontSize: '1.25rem',
            color: '#204A9D',

          }}
        >
          {data?.title}
        </p>

        <div className="card_address">
          <img src="/icons/ico-locate.svg" alt="ico-locate" />
          <span className="ml-1">{data?.address}</span>
        </div>
        <div />
        <Row className="my-3">
          <Col
            xs={12}
            xl={6}
            className="xxl-col-100"
            style={{
              padding: '1rem',
              background: '#F4F7FC',
              borderRadius: '10px',
            }}
          >
            <div className="d-flex flex-wrap justify-content-around">
              <div className="d-flex align-items-center">
                <img src="/icons/ico-acreage.svg" alt="ico-acreage" />
                <span className="ml-1 flex-grow-1">{data?.acreage}</span>
              </div>
              <div className="d-flex align-items-center">
                <img src="/icons/ico-room.svg" alt="ico-room" />
                <span className="ml-1 flex-grow-1">{data?.room}</span>
              </div>
              <div className="d-flex align-items-center">
                <img src="/icons/ico-bath.svg" alt="ico-bath" />
                <span className="ml-1 flex-grow-1">{data?.bath}</span>
              </div>
              <div className="d-flex align-items-center">
                <img src="/icons/ico-compass.svg" alt="ico-compass" />
                <span className="ml-1 flex-grow-1">{data?.direction}</span>
              </div>
            </div>
          </Col>
          <Col
            xs={12}
            xl={6}
            className="xxl-col-100
            align-items-center d-flex flex-wrap justify-content-around"
          >
            <div className="cardStatus__price">
              <span className="cardStatus__price-text1">Giá: </span>
              <span className="cardStatus__price-text2">
                {data.price ? data.price : 0}
                đ
              </span>
            </div>
            <div className="eSeparator-vertical h-50" />
            <div className="cardStatus__price">
              <span className="cardStatus__price-text1">Đơn giá/m2: </span>
              <span className="cardStatus__price-text2">
                {data.sharePrice ? data.sharePrice : 0}
                đ
              </span>
            </div>
          </Col>
        </Row>
        <div>
          <p
            className="font-weight-bold"
            style={{ color: '#204A9D' }}
          >
            Chi tiết
          </p>
          <div
            className=""
            style={{
              color: '#0F4D98',
            }}
          >
            <TextTruncate
              line={7}
              element="div"
              truncateText="…"
              text={data?.describe}
              textTruncateChild={(
                <Link href="/groups/123">
                  <a className="color_yellow">Xem thêm</a>
                </Link>
                 )}
            />
          </div>
        </div>
      </div>

      <TagContact tags={data?.tags} />

      <div className="eSeparator-horizontal" />

      <LikeSeenComment comments={data?.comments} likes={data?.likes} seen={data?.seen} />

      <EModal title="Ảnh" widthHeightProp="maxW-90vw height90vh" showModal={showModal} onHide={() => { setShowModal(false) }}>
        <ImageGallery items={imagesArray || []} />
      </EModal>
    </div>
  )
}

export default CardStream
