import React from 'react'
import Button from '../Button'
import './styles.css'

interface Props {
    tags: any
}
const TagContact = ({ tags }: Props) => (
  <div className="cardStatus-tags d-flex align-items-center">
    <span className="mr-3">Tag</span>
    <div className="flex-grow-1">
      {
        tags?.map((item:any, index:number) => {
          console.log('item', item)
          switch (item) {
            case 'đang bán':
              return (
                <div className="d-inline-block cardStatus__tags-item cardStatus__tags-item-sell" key={index}>
                  {item}
                </div>
              )
            case 'cần mua':
              return (
                <div className="d-inline-block cardStatus__tags-item cardStatus__tags-item-buy" key={index}>
                  {item}
                </div>
              )
            default:
              return (
                <div className="d-inline-block cardStatus__tags-item" key={index}>
                  {item}
                </div>
              )
          }
        })
    }
    </div>
    <div>
      <Button.Solid text="Liên hệ" btnColor="solid_button-yellow" />
    </div>
  </div>
)

export default TagContact
