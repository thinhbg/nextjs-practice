import React from 'react'
import Rating from 'react-rating'
import './styles.css'

interface Props {
  classNameCard?: string;
  onClickSeller?: (event: any) => void;
  onClickFriend?: () => void;
  showTag?: Boolean;
  showAddFriend?: Boolean;
  activeFriend?: Boolean;
  readonlyStar?: Boolean;
  data?:{
    avatarSeller?: string;
    nameSeller?: string;
    company?: string;
    yearExp?: number;
    projectExp?: number;
    star?: number;
  }
}

const Seller = (props: Props) => {
  const {
    classNameCard = '',
    onClickSeller,
    onClickFriend,
    showTag = true,
    activeFriend = true,
    showAddFriend = true,
    readonlyStar = true,
    data = {
      avatarSeller: '/images/Avatar-seller.jpg',
      nameSeller: 'Esther Howard',
      company: 'Công ty CP Tân Hoàng Minh Plus',
      yearExp: 5,
      projectExp: 129,
      star: 3,
    },
  } = props

  return (
    <div className={`rec_card ${classNameCard}`}>
      <div className="rec_card_img">
        <div className="rec_header_card">
          <div className="header_seller">
            <div className="header_avatar">
              <img
                src={data?.avatarSeller}
                alt="images"
                className="h_avatar_seller"
                onClick={onClickSeller}
              />
              <div className="header_info ml-2">
                <div className="header_name">
                  <span className="text_name" onClick={onClickSeller}>{data?.nameSeller}</span>
                  {showTag ? (
                    <div className="header_tag ml-4">
                      <span className="ml-3 mr-3">PRO</span>
                    </div>
                  ) : null}
                </div>
                <div className="header_company">
                  <span>{data?.company}</span>
                </div>
              </div>
            </div>
          </div>
          {showAddFriend ? (
            <div className="header_menu" onClick={onClickFriend}>
              {activeFriend ? <img src="/icons/ico-add-friend.svg" alt="ico-add-friend" /> : <img src="/icons/ico-add-friend.svg" alt="ico-add-friend" />}
            </div>
          ) : null}
        </div>
      </div>
      <div className="rec_card_experience">
        <div className="chid_exp">
          <span>{data?.yearExp}</span>
          <span className="text_exp">Năm kinh nghiệm</span>
        </div>
        <div className="chid_exp">
          <span>{data?.projectExp}</span>
          <span className="text_exp">Dự án đã bán</span>
        </div>
        <div className="chid_exp">
          <span>{`${data?.star}/5`}</span>
          <div className="star_exp text_exp">
            <Rating
              emptySymbol={<img src="/icons/ico-rating-empty.svg" alt="not-rated" className="ico_star_exp" />}
              fullSymbol={<img src="/icons/ico-rating-rated.svg" alt="rated" className="ico_star_exp" />}
              initialRating={data?.star}
              readonly={readonlyStar ? true : false}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Seller
