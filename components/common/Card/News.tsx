import React from 'react'
import Button from '../Button'
import './styles.css'

interface Props {
  classNameCard?: string;
  onClickMore?: () => void;
  data?:{
    img?: string;
    title?: string;
    describe?: string;
    day?: number;
    linkMore?: number;
  }
}

const News = (props: Props) => {
  const {
    classNameCard = '',
    onClickMore,
    data = {
      img: '/images/card-news.png',
      title: 'PHÊ DUYỆT CHỦ CHƯƠNG XÂY DỰNG SIÊU ĐÔ THỊ VEN BIỂN THÀNH PHỐ HẠ LONG',
      describe: 'Turpis mi amet ornare nulla sapien magna velit. Tristique semper tellus consectetur enim eget sed. ',
      day: '12/12/2020',
      linkMore: '#',
    },
  } = props

  return (
    <div className={`rec_card_news ${classNameCard}`}>
      <div className="card_news_img">
        <img
          src={data?.img}
          alt="images"
          className="news_img"
        />
      </div>
      <div className="card_news_info">
        <div className="news_title" onClick={onClickMore}>
          <span>{data?.title}</span>
        </div>
        <div className="news_describe">
          <span>{data?.describe}</span>
        </div>
        <div className="news_more">
          <span>{data?.day}</span>
          <Button.Solid text="Đọc thêm" onClick={onClickMore} className="btn_read" />
        </div>
      </div>
    </div>
  )
}

export default News
