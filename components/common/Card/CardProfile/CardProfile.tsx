import Link from 'next/link'
import React from 'react'
import TextTruncate from 'react-text-truncate'
import { EAvatar, EGroupAvatar } from '../../EImage'
import './CardProfile.css'
import { numberWithCommas } from '../../../../lib/common'

interface Props {
  headerColor?: string
  type: 1 | 2 | 3 | 4 | 5 /* https://i.gyazo.com/5641c33e6e07e103b34741aff760ef4b.png */
  className?: string
  imageSrc?: string
  dataSource?: any
}

const CardProfile = ({
  headerColor = 'var(--color-matisse)',
  type,
  className = '',
  imageSrc = '',
  dataSource = null,
}:Props) => {
  switch (type) {
    case 1:
      return (
        <div className={`cardProfile ${className ? className : ''}`}>
          <div
            className="cardProfile-header type1"
            style={{ backgroundColor: headerColor }}
          >
            <div className="cardProfile__header-image">
              <EAvatar
                src={imageSrc}
                borderCustom="3px solid white"
              />
            </div>
          </div>
          <div className="cardProfile-body">
            <p className="cardProfile-name">
              {dataSource?.name}
            </p>
            <p className="cardProfile-title">
              {dataSource?.title}
            </p>
            <p className="cardProfile-company">
              {dataSource?.companyName}
            </p>
            <div className="d-flex align-items-center flex-column flex-xl-row  mb-4">
              <img src="/icons/ico-flag-blue.svg" alt="flag" className="mr-0 mr-xl-4" />
              <span style={{
                color: '#204A9D',
                fontSize: '0.875rem',
              }}
              >
                {dataSource?.yearExp}
                {' '}
                năm kinh nghiệm
              </span>
            </div>
            <div className="d-flex align-items-center flex-column flex-xl-row  mb-4">
              <img src="/icons/ico-house-blue.svg" alt="flag" className="mr-0 mr-xl-4" />
              <span style={{
                color: '#204A9D',
                fontSize: '0.875rem',
              }}
              >
                Đã bán
                {' '}
                <strong>
                  {dataSource?.projects}
                </strong>
                {' '}
                dự án
              </span>
            </div>
            <div className="d-flex align-items-center flex-column flex-xl-row  mb-4">
              <img src="/icons/ico-friends-blue.svg" alt="flag" className="mr-0 mr-xl-4" />
              <span style={{
                color: '#204A9D',
                fontSize: '0.875rem',
              }}
              >
                <strong>
                  {dataSource?.friendlist}
                </strong>
                {' '}
                bạn bè
              </span>
            </div>
            <div className="d-flex align-items-center flex-column flex-xl-row ">
              <img src="/icons/ico-follower-blue.svg" alt="flag" className="mr-0 mr-xl-4" />
              <span
                style={{
                  color: '#204A9D',
                  fontSize: '0.875rem',
                }}
              >
                <strong>
                  {dataSource?.followers}
                </strong>
                {' '}
                người đang theo dõi
              </span>
            </div>
          </div>
        </div>
      )

    case 2:
      return (
        <div className={`cardProfile ${className ? className : ''}`}>
          <div
            className="cardProfile-header"
            style={{ backgroundColor: headerColor }}
          >
            <span className="cardProfile__header-title">
              {dataSource.name}
            </span>
            <Link href="#">
              <a>
                <div className="cardProfile__header-share">
                  <img src="/icons/ico-share-yellow.svg" alt="share-yellow" className="object-fit-cover w-100 h-100" />
                </div>
              </a>
            </Link>
          </div>
          <div className="cardProfile-body-02">
            {dataSource.groupMaster && (
              <div className=" cardProfile__2-detail ">
                <img src="/icons/ico-flag-blue.svg" alt="web-blue" />
                <span className="cardProfile__text ml-1 ml-xl-4 font-weight-bold text-break" style={{ textDecoration: 'underline' }}>
                  {dataSource.groupMaster}
                </span>
              </div>
            )}
            { dataSource.email && (
              <div className=" cardProfile__2-detail">
                <img src="/icons/ico-person-blue.svg" alt="person-blue" />
                <span className="cardProfile__text ml-1 ml-xl-4">
                  {dataSource.email}
                </span>
              </div>
            )}
            {dataSource.hotline && (
            <div className=" cardProfile__2-detail">
              <img src="/icons/ico-hotline-blue.svg" alt="hotline-blue" />
              <span className="cardProfile__text ml-1 ml-xl-4">
                {dataSource.hotline}
              </span>
            </div>
            )}
            {dataSource.zalo && (
            <div className=" cardProfile__2-detail">
              <img src="/icons/ico-zalo-blue.svg" alt="zalo-blue" />
              <span className="cardProfile__text ml-1 ml-xl-4">
                {dataSource.zalo}
              </span>
            </div>
            )}
            {dataSource.web && (
            <div className=" cardProfile__2-detail ">
              <img src="/icons/ico-web-blue.svg" alt="web-blue" />
              <span className="cardProfile__text text-break ml-1 ml-xl-4">
                {dataSource.web}
              </span>
            </div>
            )}

            {dataSource.members && (
            <div className=" cardProfile__2-detail ">
              <img src="/icons/ico-friends-blue.svg" alt="web-blue" />
              <span className="cardProfile__text text-break mx-1 mx-xl-4">
                {numberWithCommas(dataSource.members)}
                {' '}
                thành viên
              </span>
              <EGroupAvatar imgArray={dataSource.membersImg} size="20px" classNameProp="flex-grow-1" />
            </div>
            )}
            {dataSource.projectsSelling && (
            <div className=" cardProfile__2-detail ">
              <img src="/icons/ico-house-blue.svg" alt="web-blue" />
              <span className="cardProfile__text ml-1 ml-xl-4">
                {dataSource.projectsSelling}
                {' '}
                bất động sản đang bán
              </span>
            </div>
            )}
          </div>
          {
            dataSource?.updateGroup && (
            <>
              <div className="eSeparator-horizontal" />
              <div
                className="d-flex align-items-center justify-content-center px-2 px-0"
                style={{
                  height: '3.375rem',
                  cursor: 'pointer',
                }}
                onClick={() => {
                  console.log('Click Cập nhật')
                }}
              >
                <span
                  className=""
                  style={{
                    color: '#0F4D98',
                    fontSize: '1.125rem',
                  }}
                >
                  Cập nhật thông tin nhóm
                </span>
              </div>
            </>
            )
        }
        </div>
      )

    case 3:
      return (
        <div className={`cardProfile ${className ? className : ''}`}>
          <div
            className="cardProfile-header"
            style={{ backgroundColor: headerColor }}
          >
            <span className="cardProfile__header-title">
              {dataSource?.title}
            </span>
          </div>
          <div className="cardProfile-body-02">
            {
              dataSource?.data?.map((item: any, index:number) => (
                <div className="cardProfile-body-group d-flex align-items-start flex-column flex-xl-row ">
                  <div
                    className="overflow-hidden"
                    style={{
                      borderRadius: '10px',
                      width: '3.5rem',
                      height: '3.5rem',
                      flexShrink: 0,
                    }}
                  >
                    <img src={item.imgSrc} alt={item.imgSrc} className="object-fit-cover w-100 h-100" />
                  </div>
                  <Link href="/groups/nhom-BDS-plus" passHref key={index}>
                    <a className="">
                      <div className="d-flex flex-grow-1 flex-column">
                        <TextTruncate
                          line={1}
                          element="div"
                          truncateText="…"
                          text={item.nameGroup}
                          containerClassName="cardProfile-3__group  ml-0 ml-xl-3 w-100"
                        />
                        <div className="ml-0 ml-xl-3 d-flex align-items-center flex-wrap">
                          <EGroupAvatar imgArray={item.membersImg} size="20px" />
                          <span className="cardProfile-3__members ml-1">
                            {item.members}
                            {' '}
                            thành viên
                          </span>
                        </div>
                      </div>
                    </a>
                  </Link>
                </div>

              ))
            }
          </div>
          <div
            className=""
            style={{
              width: '100%',
              height: '0.5px',
              borderTop: '0.75px solid #204A9D',
            }}
          />
          <Link href="#">
            <a>
              <div className="cardProfile3-footer">
                <span style={{ fontSize: '1.125rem' }}>
                  Xem tất cả
                </span>
              </div>
            </a>
          </Link>

        </div>
      )

    case 4:
      return (
        <div className={`cardProfile ${className ? className : ''}`}>
          <div
            className="cardProfile-header"
            style={{ backgroundColor: headerColor }}
          >
            <span className="cardProfile__header-title">
              Khách hàng tiềm năng
            </span>
          </div>
          <div className="cardProfile-body-02">
            {
              dataSource?.data?.map((item:any, index:number) => (
                <div className="d-flex flex-wrap align-items-center justify-content-around cardProfile4-body" key={index}>
                  <div>
                    <EAvatar
                      src={item.img}
                      width="2.5rem"
                      height="2.5rem"
                    />
                  </div>
                  <span className="cardProfile__text-4">
                    {item.name}
                  </span>
                  {
                    item.status === 'buy' && (
                    <span className="cardProfile4-buy">
                      Tìm mua
                    </span>
                    )
                  }
                  {
                    item.status === 'sell' && (
                    <span className="cardProfile4-sell">
                      Đang bán
                    </span>
                    )
                  }
                </div>
              ))
            }
          </div>
        </div>
      )

    case 5:
      return (
        <div className={`cardProfile ${className ? className : ''}`}>
          <div
            className="cardProfile-header"
            style={{ backgroundColor: headerColor }}
          >
            <span className="cardProfile__header-title">
              {dataSource?.title}
            </span>
          </div>
          <div className="cardProfile-body-02">
            {
                dataSource?.data?.map((item: any, index:number) => (
                  <div className="cardProfile-body-group" key={index}>
                    <div
                      className="overflow-hidden mr-4"
                      style={{
                        borderRadius: '10px',
                        width: '3.5rem',
                        height: '3.5rem',
                        flexShrink: 0,
                      }}
                    >
                      <img src={item.imgSrc} alt={item.imgSrc} className="object-fit-cover w-100 h-100" />
                    </div>
                    <Link href="/groups/nhom-BDS-plus" passHref key={index}>
                      <a className="cardProfile5__link">
                        <p className="mb-0">
                          {item?.projectName}
                        </p>
                      </a>
                    </Link>
                  </div>
                ))
            }
          </div>
          <div
            className=""
            style={{
              width: '100%',
              height: '0.5px',
              borderTop: '0.75px solid #204A9D',
            }}
          />
          <Link href="/groups/123" passHref>
            <a>
              <div className="cardProfile3-footer">
                <span style={{ fontSize: '1.125rem' }}>
                  Xem tất cả
                </span>
              </div>
            </a>
          </Link>

        </div>
      )

    default:
      return <div />
  }
}

export default CardProfile
