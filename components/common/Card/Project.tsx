import React from 'react'
import './styles.css'

interface Props {
  classNameProject?: string;
  classNameContent?: string;
  onClick?: () => void;
  type?: number;
  showImg?: Boolean;
  showCompass?: Boolean;
  showBorderTop?: Boolean;
  data?:{
    logo?: string;
    img?: string;
    name?: string;
    location?: string;
    direction?: string;
    investor?: string;
    acreage?: number;
    detail?: any;
  }
}

const Project = (props: Props) => {
  const {
    classNameProject,
    classNameContent,
    onClick,
    type = 1,
    showImg = true,
    showCompass = true,
    showBorderTop = false,
    data = {
      logo: '/images/logo-project.png',
      img: '/images/ImageBietThu.png',
      name: 'Vinhomes Smart City',
      location: 'Đại Mỗ, Tây Mỗ, Từ Liêm, Hà Nội',
      direction: 'Nằm trên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch rên Đại lộ Thăng Long – huyết mạch phía Tây thủ đô, Vinhomes Smart City sở hữu vị trí đắc địa, cách Trung tâm Hội nghị Quốc gia chỉ 7 phút di chuyển',
      investor: 'VinGroup',
      acreage: '280',
      detail: [
        { value: 'Khu chèo thuyền Kayak ' },
        { value: 'Bệnh viện Đa khoa Quốc tế Vinmec' },
        { value: 'Trường học Quốc tế Vinschool' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Tòa văn phòng...' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
        { value: 'Siêu TTTM Vincom Mega Mall' },
      ],
    },
  } = props
  return (
    <div className={`${type === 1 ? 'rec_card_project' : 'rec_card_project rec_card_project_2'} ${classNameProject}`} onClick={onClick}>
      <div className="card_project_logo">
        <img src={data?.logo} alt="#" className="img_project_logo" />
      </div>
      {showImg && (
      <div className="card_project_img">
        <img src={data?.img} alt="#" className="img_project_img" />
      </div>
      )}
      {type === 1 ? (
        <div className={`card_project_detail ${classNameContent}`}>
          <div className="detail_title">
            <span>{data?.name}</span>
          </div>
          <div className="detail_location">
            <span>{`(${data?.location})`}</span>
          </div>
          <div className="detail_investor">
            <span>{`Chủ đầu Tư: ${data?.investor}`}</span>
          </div>
        </div>
      ) : (
        <div className={`card_project_detail_2 ${classNameContent}`}>
          <div className={`detail_location pb-3 ${showBorderTop && 'border__top-solid'}`}>
            <img src="icons/ico-location.svg" alt="#" className="icon_detail_proj mr-3" />
            <span>{data?.location}</span>
          </div>
          <div className="detail_acreage py-3">
            <img src="/icons/ico-acreage-yellow.svg" alt="#" className="icon_detail_proj mr-3" />
            <span>{`Tổng diện tích: ${data?.acreage} ha`}</span>
          </div>
          {showCompass && (
          <div className="detail_acreage detail_description py-3">
            <img src="icons/ico-compass-yellow.svg" alt="#" className="icon_detail_proj mr-3" />
            <span className="detail_compass">{data?.direction}</span>
          </div>
          )}
          <div className="detail_description my-3">
            <img src="/icons/ico-city.svg" alt="#" className="icon_detail_proj mr-3" />
            <div className="descriptions">
              {data?.detail?.map((el: any) => (
                <>
                  <span>{`• ${el?.value}`}</span>
                  <br />
                </>
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default Project
