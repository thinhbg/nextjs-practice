import React from 'react'
import { Col, Row } from 'react-bootstrap'
import Button from '../Button'
import Input from '../Input'
import './styles.css'

interface Props {
  header?: string;
  optionsList?: {value: any, label: string}[];
  placeholder?: string;
}

const FormContact = (props: Props) => {
  const {
    header = 'Đăng ký nhận tin để không bỏ lỡ các tin tức Bất động sản hot nhất!',
    optionsList,
    placeholder = 'Nhu cầu tìm mua',
  } = props
  return (

    <div className="form_contacts">
      <p className="text-center color-matisse my-5">{header}</p>
      <div className="container my-5 register__for__info">
        <Row className="mb-4">
          <Col>
            <Input.InputText className="" placeholder="Họ tên" />
          </Col>
          <Col>
            <Input.InputText className="" placeholder="Email" />
          </Col>
          <Col>
            <Input.InputText className="" placeholder="Số điện thoại" />
          </Col>
          <Col>
            <Input.Select
              optionsList={optionsList}
              placeholder={placeholder}
              className=""
            />
          </Col>
          <Col sm={2}>
            <Button.Solid
              btnColor="solid_button-blue"
              text="ĐĂNG KÝ"
              className="px-0 w-100"
            />
          </Col>
        </Row>
      </div>

    </div>
  )
}

export default FormContact
