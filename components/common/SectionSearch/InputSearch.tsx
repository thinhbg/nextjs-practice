import React, { ReactNode, useEffect, useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap'
import './styles.css'

interface Props {
  disabled?: boolean
  id?: string
  prefix?: ReactNode
  suffix?: ReactNode
  type?: string
  value?: string
  onChangeFunc?: (data: any) => void
  onPressEnter?: (data: any) => void
  className?: string
  placeholder?: string
  readOnly?: boolean
  field?: any
}

const InputSearch = (
  {
    className = '',
    placeholder = '',
    prefix,
    suffix,
    value = '',
    type = 'text',
    disabled = false,
    readOnly = false,
    onChangeFunc, onPressEnter,
    field,
  }: Props,
) => {
  const [input, setInput] = useState('')

  useEffect(() => {
    setInput(value)
  }, [value])

  const handleKeyDown = (event: any) => {
    if (event.key === 'Enter') {
      console.log('do validate', input)
      onPressEnter && onPressEnter(input)
    }
  }
  return (
    <InputGroup
      className={`inputSearch-input ${className}`}
    >
      {
        prefix && (
          <div className="inputSearch-prefix">
            {prefix}
          </div>
        )
      }

      <Form.Control
        {...field}
        placeholder={placeholder}
        className={`inputSearch-text ${prefix ? 'rec_prefix' : ''}`}
        readOnly={readOnly}
        onChange={(e) => {
          setInput(e.target.value)
          onChangeFunc && onChangeFunc(e.target.value)
        }}
        value={input}
        type={type}
        disabled={disabled}
        onKeyPress={(event: any) => {
          handleKeyDown(event)
        }}
      />

    </InputGroup>
  )
}

export default InputSearch
