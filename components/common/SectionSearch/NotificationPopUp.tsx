import React from 'react'
import './styles.css'
import { EAvatar } from '../EImage'
import Button from '../Button'

const JsonMessage = [
  {
    name: 'HiSoft 1',
    message: ' Hello Hisoft',
    notSeen: true,
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 2',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 3',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 4',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 5',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 6',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 1',
    message: ' Hello Hisoft',
    notSeen: true,
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 2',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 3',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 4',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 5',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    name: 'HiSoft 6',
    message: ' Hello Hisoft',
    image: { src: '/images/RealEstate-1.png' },
  },
]

const JsonNotifications = [
  {
    type: 'notification',
    who: 'Nguyễn Thế Anh',
    message: 'đã thích bình luận của bạn trong nhóm Hội Nhà Đất Hải Phòng',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    type: 'add-friend',
    who: 'Lê Sơn',
    message: 'mời bạn tham gia nhóm Biệt đội săn đất',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    type: 'notification',
    who: 'Nguyễn Thế Anh',
    message: 'đã thích bình luận của bạn trong nhóm Hội Nhà Đất Hải Phòng',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    type: 'add-friend',
    who: 'Lê Sơn',
    message: 'mời bạn tham gia nhóm Biệt đội săn đất',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    type: 'notification',
    who: 'Nguyễn Thế Anh',
    message: 'đã thích bình luận của bạn trong nhóm Hội Nhà Đất Hải Phòng',
    image: { src: '/images/RealEstate-1.png' },
  }, {
    type: 'add-friend',
    who: 'Lê Sơn',
    message: 'mời bạn tham gia nhóm Biệt đội săn đất',
    image: { src: '/images/RealEstate-1.png' },
  },
]
interface Props{
    message?: boolean
}
const NotificationPopUp = ({ message }: Props) => (
  <div className="notification-popup">
    <div className="notification-block">
      <div className="notification__header">
        <p className="font-weight-bold color-toryBlue" style={{ fontSize: '1.375rem' }}>
          Thông báo
        </p>
        <div>
          {
            message && (
            <>
              <img className="ml-3 notification__icon" src="/icons/ico-search.svg" alt="search" />
              <img className="ml-3 notification__icon" src="/icons/ico-write.svg" alt="write" />
            </>
            )
          }
          <img className="ml-3 notification__icon" src="/icons/ico-three-dot.svg" alt="dot" />
        </div>
      </div>
      {
        message ? (
          <>
            {
                JsonMessage?.map((item:any, index:number) => (
                  <div className="notification__content" key={index}>
                    <div className="notification__content-image">
                      <EAvatar src="/images/RealEstate-1.png" width="48px" height="48px" verticalAlign="middle" />
                    </div>
                    <div className="d-flex flex-column">
                      <span className="notification__name">
                        {item.name}
                      </span>
                      <span className={`notification__message ${item?.notSeen ? 'notification__message-notSeen' : ''}`}>
                        {item.message}
                      </span>
                    </div>
                  </div>
                ))
              }
          </>
        ) : (
          <>
            {
              JsonNotifications?.map((item:any, index:number) => (
                <div className="notification__content" key={index}>
                  <div className="notification__content-image">
                    <EAvatar src="/images/RealEstate-1.png" width="48px" height="48px" verticalAlign="middle" />
                  </div>
                  <div className="d-flex flex-column">
                    <div className="mb-3">
                      <span className="notification__name">
                        {item.who}
                      </span>
                      {' '}
                      <span className="notification__message">
                        {item.message}
                      </span>
                    </div>
                    {
                      item?.type === 'add-friend' && (
                      <div className="d-flex">
                        <div className="pr-2" style={{ flex: '0 0 50%' }}>
                          <Button.Solid className="padding1rem w-100" text="Chấp nhận" btnColor="solid_button-blue" />
                        </div>
                        <div className="pl-2" style={{ flex: '0 0 50%' }}>
                          <Button.Solid className="padding1rem w-100" text="Từ chối" btnColor="solid_button-blue-100" />
                        </div>
                      </div>
                      )
                    }
                  </div>
                </div>
              ))
            }
          </>
        )
      }
    </div>
  </div>

)

export default NotificationPopUp
