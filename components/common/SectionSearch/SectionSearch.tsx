import React, { useState } from 'react'
import './styles.css'
import { Col, Row } from 'react-bootstrap'
import { useRouter } from 'next/router'
import InputSearch from './InputSearch'
import NotificationPopUp from './NotificationPopUp'

interface Props {
    className?: string
}
const SectionSearch = ({ className }:Props) => {
  const [active, setActive] = useState(1)
  const router = useRouter()
  return (
    <div className="w-100 px-3">
      <Row className={`sectionSearch ${className ? className : ''}`}>
        <Col xs={12} sm={7} lg={4}>
          <div className="sectionSearch-input">
            <InputSearch
              prefix={<img src="/icons/ico-search.svg" />}
              placeholder="Search"
            />
          </div>
        </Col>
        <Col xs={12} sm={5} lg style={{ display: 'grid', placeItems: 'center' }}>
          <div
            className="sectionSearch__postStatus-button"
            onClick={() => {
              console.log('Click Đăng tin')
            }}
          >
            <img src="/icons/ico-post-status-white.svg" alt="post-status-white" />
            <span>
              ĐĂNG TIN
            </span>
          </div>
        </Col>
        <Col xs={12} lg>
          <div className="sectionSearch__feature-buttons">
            {/*  .search__feature-icon-active */}
            <div
              className={`search__feature-icon ${active === 1 ? 'search__feature-icon-active' : ''}`}
              onClick={() => {
                console.log('Clicked')
                setActive(1)
              }}
            >
              <img src="/icons/ico-house-blue.svg" alt="home" />
            </div>
            <div
              className={`search__feature-icon ${active === 2 ? 'search__feature-icon-active' : ''}`}
              onClick={() => {
                console.log('Clicked')
                setActive(2)
              }}
            >
              <div className="search__icon-block">
                <img src="/icons/ico-bell-gray.svg" alt="home" />
                <span className="serce__badge">90</span>
              </div>
              {
                active === 2 && (
                  <NotificationPopUp />
                )
              }
            </div>

            <div
              className={`search__feature-icon ${active === 3 ? 'search__feature-icon-active' : ''}`}
              onClick={() => {
                console.log('Clicked')
                setActive(3)
              }}
            >
              <div className="search__icon-block">
                <img src="/icons/ico-inbox-gray.svg" alt="home" />
                <span className="serce__badge">90</span>
              </div>
              {
                    active === 3 && (
                    <NotificationPopUp message />
                    )
                }
            </div>
            <div
              className={`search__feature-icon ${active === 4 ? 'search__feature-icon-active' : ''}`}
              onClick={() => {
                // router.push('/groups')
                setActive(4)
              }}
            >
              <img src="/icons/ico-friends-gray.svg" alt="home" />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  )
}
export default SectionSearch
