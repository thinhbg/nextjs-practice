import React from 'react'
import './styles.css'

interface Props {
  classNameUser?: string;
  data?:{
    avatar?: string;
    name?: string;
    time?: string;
  }
}

const UserUpload = (props: Props) => {
  const {
    classNameUser,
    data = {
      avatar: '/images/Avatar-seller.jpg',
      name: 'Devon Lane',
      time: '16:09 - 29/10/2020',
    },
  } = props
  return (
    <div className={`rec__user-upload ${classNameUser}`}>
      <img src={data?.avatar} alt="#" className="avatar__user" />
      <div className="detail">
        <div className="name">
          <span>{data?.name}</span>
        </div>
        <div className="time">
          <span>{data?.time}</span>
        </div>
      </div>
    </div>
  )
}

export default UserUpload
