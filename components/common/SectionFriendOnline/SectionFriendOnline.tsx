import React from 'react'
import './SectionFriendOnline.css'
import { ESlider } from '../ESlider'
import { EAvatar } from '../EImage'

const listFriends = [
  {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  }, {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  }, {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  }, {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  }, {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  }, {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  }, {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  }, {
    name: 'Ahihi',
    imgSrc: '/images/ImageBietThu.png',
  },
]

interface Props{
    className?: string
    title: string
}
const SectionFriendOnline = ({ className, title }:Props) => (
  <div className={`d-flex align-items-center my-5 ${className ? className : ''}`}>
    <div className="d-flex align-items-center  flex-shrink-0">
      <div className="sectionFriend-dot" />
      <span className="sectionFriend-text ">
        {title}
      </span>
    </div>
    <div className="overflow-hidden flex-grow-1 ml-3">
      <ESlider
        settingCustom={{
          arrows: false,
          variableWidth: true,
          infinite: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: false,
          swipeToSlide: true,
          swipe: true,
          centerMode: false,
          responsive: [
            {
              breakpoint: 991.98,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 767.98,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 575.98,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              },
            },
          ],
        }}
      >
        {
          listFriends?.map((item, index) => (
            <div style={{ width: '80px', height: '64px', margin: '0 auto' }} key={index}>
              <EAvatar
                src={item.imgSrc}
                width="64px"
                height="64px"
                onlineStatus
                borderCustom="2px solid var(--color-green-300)"
              />
            </div>
          ))
        }
      </ESlider>
    </div>
  </div>
)

export default SectionFriendOnline
