import React from 'react'
import { Modal } from 'react-bootstrap'
import './EModal.css'

interface Props{
  title: string
  showModal: boolean
  onHide: ()=>void
  children: React.ReactNode
  widthHeightProp: string
}
const EModal = ({
  children, showModal, onHide, title, widthHeightProp = '',
}:Props) => (
  <Modal
    show={showModal}
    onHide={onHide}
    keyboard={false}
    dialogClassName={`${widthHeightProp}`}
  >
    <Modal.Header closeButton>
      <Modal.Title className="color-toryBlue">{title}</Modal.Title>
    </Modal.Header>
    <Modal.Body className="overflow-auto">
      {children}
    </Modal.Body>
  </Modal>
)

export default EModal
