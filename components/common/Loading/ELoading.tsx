import lottie from 'lottie-web';
import React, { useEffect } from 'react';

interface Props { }

const ELoading = () => {
  useEffect(() => {
    const loading = document.getElementById('eloading');
    lottie.loadAnimation({
      container: loading ? loading : document.createElement("div"),
      renderer: 'svg',
      loop: true,
      autoplay: true,
      path: 'static/loading.json'
    })
  }, [])
  return (
    <div style={{
      position: 'fixed',
      top: 0,
      left: 0,
      background: 'white',
      opacity: '95%',
      height: '100vh', width: '100vw', display: 'flex',
      justifyContent: 'center', alignItems: 'center'
    }} >
      <div id="eloading" />
    </div>
  )
}

export default ELoading
