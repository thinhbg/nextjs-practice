import React, { useEffect, useRef, useState } from 'react'
// @ts-ignore
import NoImage from './logo.svg'

const ErrorFallBack = NoImage

interface Props {
  src?: string
  width?: string | number
  height?: string | number
  onClick?: () => any
  objectFit?: 'fill' | 'contain' | 'cover'
  objectPosition?: string
  className?: string
  verticalAlignProp?: string
}

const CustomImage = ({
  src,
  width,
  height,
  onClick,
  objectFit,
  objectPosition,
  className,
  verticalAlignProp,
}: Props) => {
  const ref = useRef(null as any)
  const [initialized, setInitialized] = useState(false)
  const [error, setError] = useState(false)

  useEffect(() => {
    if (ref?.current && !initialized) {
      ref.current.addEventListener('error', () => {
        setError(true)
      })
      ref.current.src = src
      setInitialized(true)
    }
  }, [ref?.current, initialized])

  return (
    <img
      ref={ref}
      src={error ? ErrorFallBack : (src || NoImage)}
      alt={error ? 'error' : (src || NoImage)}
      className={`${className ? className : ''}`}
      style={{
        objectFit: error ? 'cover' : (objectFit || 'cover'),
        objectPosition: error ? 'center' : (objectPosition || 'center'),
        width: error ? '100%' : (width || '100%'),
        height: error ? '100%' : (height || '100%'),
        verticalAlign: verticalAlignProp ? verticalAlignProp : 'baseline',
      }}
    />
  )
}
export default CustomImage
