import React, { useEffect, useRef, useState } from 'react'
// @ts-ignore
import NoImage from './logo.svg'

const ErrorFallBack = NoImage

interface Props {
    src?: string
    width?: string | number
    height?: string | number
    onClick?: () => any
    objectFit?: 'fill' | 'contain' | 'cover'
    objectPosition?: string
    className?: string
    borderRadius?: string
    onlineStatus?: boolean
    borderCustom?: string
    verticalAlign?: string
}

const EAvatar = ({
  src,
  width,
  height,
  onClick,
  objectFit,
  objectPosition,
  className,
  borderRadius,
  onlineStatus, borderCustom, verticalAlign,
}: Props) => {
  const ref = useRef(null as any)
  const [initialized, setInitialized] = useState(false)
  const [error, setError] = useState(false)

  useEffect(() => {
    if (ref?.current && !initialized) {
      ref.current.addEventListener('error', () => {
        setError(true)
      })
      ref.current.src = src
      setInitialized(true)
    }
  }, [ref?.current, initialized])

  const style = () => {
    const customStyle = {
      boxShadow: 'none',
      objectFit: error ? 'cover' : (objectFit || 'cover'),
      objectPosition: error ? 'center' : (objectPosition || 'center'),
      width: error ? '100%' : (width || '100%'),
      height: error ? '100%' : (height || '100%'),
      verticalAlign: 'baseline',
      borderRadius: '999px',
      border: 'none',

    }
    if (borderRadius) customStyle.borderRadius = borderRadius
    if (borderCustom) customStyle.border = borderCustom
    if (height) customStyle.height = height
    if (width) customStyle.width = width
    if (verticalAlign) customStyle.verticalAlign = verticalAlign
    console.log(customStyle)

    return customStyle
  }

  return (
    <div className="position-relative d-inline-block h-100 w-100">
      <img
        ref={ref}
        src={error ? ErrorFallBack : (src || NoImage)}
        alt={error ? 'error' : (src || NoImage)}
        className={`${className} eAvatar `}
        onClick={() => {
        }}
        style={style()}
      />
      {
                onlineStatus && borderCustom && (
                <div
                  style={{
                    width: '10px',
                    height: '10px',
                    borderRadius: '99px',
                    backgroundColor: '#36EA7E',
                    position: 'absolute',
                    bottom: '5%',
                    right: '25%',
                  }}
                />
                )
            }
    </div>
  )
}
export default EAvatar
