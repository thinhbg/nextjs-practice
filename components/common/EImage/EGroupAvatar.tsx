import React, { useMemo } from 'react'

interface Props {
  imgArray?: {src:string}[]
  size?: string
  classNameProp?: string
}
const EGroupAvatar = ({ imgArray = [], size = '50px', classNameProp = '' }:Props) => {
  const imgArrayAfter = useMemo(() => imgArray?.slice(0, 5),
    [imgArray])
  return (
    <div style={{ marginLeft: `calc(${size}*2/3)` }}>
      {
        imgArrayAfter?.map((item, index) => (
          <img
            key={index}
            src={item.src}
            alt="item?.src"
            className={`object-fit-cover ${classNameProp}`}
            style={{
              borderRadius: '50%',
              marginLeft: `calc(-${size}*2/3)`,
              border: '1px solid var(--color-green-300)',
              width: size,
              height: size,

            }}
          />
        ))
      }
    </div>
  )
}

export default EGroupAvatar
