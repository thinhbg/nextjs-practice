import React from 'react'
import './styles.css'

interface Props {
  text: string
  btnColor?: 'outline-wIcon-clear' | 'outline-wIcon-clear-yellow' | 'outline-wIcon-yellow' | 'outline-wIcon-blue'
  onClick?: () => void
  disabled?: boolean,
  type?: 'submit' | 'button' | 'reset',
  className?: string,
  shadow?: boolean,
  prefix?: React.ReactNode,
  suffix?: React.ReactNode,
}

const OutlineButton = (props: Props) => {
  const {
    text = '',
    className = '',
    onClick,
    disabled = false,
    type = 'button',
    btnColor,
    shadow = false,
    prefix,
    suffix,
  } = props

  const style = () => {
    const customStyle = {}
    // if (shadow) customStyle.boxShadow = `0px 0px 10px 5px var(--color-${'black' || 'creamCan'}-shadow)`
    return customStyle
  }

  return (

    <button
      className={`rec_button_wIcon outline-wIcon-block ${btnColor} ${className}`}
      /* eslint-disable-next-line react/button-has-type */
      type={type}
      onClick={onClick}
      disabled={disabled}
      style={style()}
    >
      {
        prefix && (prefix)
      }
      <span className="btnWIcon--text">
        {text}
      </span>
      {
        suffix && (suffix)
      }
    </button>

  )
}

export default OutlineButton
