import React from 'react'
import { Button } from 'react-bootstrap'
import './styles.css'

interface Props {
  text?: string
  btnColor?: 'solid_button-yellow' | 'solid_button-clear' | 'solid_button-blue' |'solid_button-blue-100'
  onClick?: () => void
  disabled?: boolean,
  type?: 'submit' | 'button' | 'reset',
  className?: string,
  boxShadow?: string,
  borderRadius?: string
}

interface Styles {
  boxShadow?: string
  borderRadius?: string
}

const SolidButton = (props: Props) => {
  const {
    text = '',
    className = '',
    onClick,
    disabled = false,
    type = 'button',
    btnColor,
    boxShadow = false,
    borderRadius = null,
  } = props

  const style = () => {
    const customStyle: Styles = { }
    if (boxShadow) customStyle.boxShadow = boxShadow
    if (borderRadius) customStyle.borderRadius = borderRadius
    return customStyle
  }

  return (
    <Button
      className={`rec_button_custom solid_button ${btnColor} ${className}`}
      type={type}
      onClick={onClick}
      disabled={disabled}
      style={style()}
    >
      {text}
    </Button>
  )
}

export default SolidButton
