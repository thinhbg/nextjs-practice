import React from 'react'
import { Button } from 'react-bootstrap'
import './styles.css'

interface Props {
  text: string
  btnColor?: 'outline_button-yellow' | 'outline_button-clear' | 'outline_button-blue' | 'outline_button-clear-yellow'
  onClick?: () => void
  disabled?: boolean,
  type?: 'submit' | 'button' | 'reset',
  className?: string,
  shadow?: boolean,
  prefix?: React.ReactNode,
  suffix?: React.ReactNode,

}

const OutlineButton = (props: Props) => {
  const {
    text = '',
    className = '',
    onClick,
    disabled = false,
    type = 'button',
    btnColor = 'outline_button-yellow',
    shadow = false,
    prefix, suffix,
  } = props

  const style = () => {
    const customStyle = {
      boxShadow: 'none',
    }
    if (shadow) customStyle.boxShadow = `0px 0px 10px 5px var(--color-${'black' || 'creamCan'}-shadow)`
    return customStyle
  }

  return (
    <Button
      className={`rec_button_custom outline_button ${btnColor} ${className}`}
      type={type}
      onClick={onClick}
      disabled={disabled}
      style={style()}
    >
      {
        prefix && (prefix)
      }
      {text}
      {
        suffix && (suffix)
      }
    </Button>
  )
}

export default OutlineButton
