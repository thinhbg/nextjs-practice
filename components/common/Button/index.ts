import OutlineButton from './Outline'
import SolidButton from './Solid'
import ButtonWIcon from './ButtonWIcon'

const Button = {
  Solid: SolidButton,
  Outline: OutlineButton,
  ButtonWIcon,
}
export default Button
