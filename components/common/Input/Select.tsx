import React from 'react'
import Select, { components } from 'react-select'

import './styles.css'

interface Props {
  onSelect?: (event: any) => void;
  optionsList?: {value: any, label: string}[];
  placeholder?: string;
  className?: string;
  icon?: string;
  classNamePrefix?: string /* no-border , font-'number' */
}
const DropdownIndicator = (props: any) => (
  components.DropdownIndicator && (
  <components.DropdownIndicator {...props}>
    <img src="/icons/ico-dropdown.svg" alt="rec-location" />
  </components.DropdownIndicator>
  )
)
const InputSelect = (props: Props) => {
  const {
    placeholder = 'Tuỳ chọn',
    optionsList = [],
    onSelect,
    icon,
    className = '',
    classNamePrefix = '',
  } = props

  return (
    <div className="wrap_select">
      <Select
        components={{
          DropdownIndicator,
          IndicatorSeparator: () => null,
          SingleValue: ({ children, ...props }) => (
            <components.SingleValue {...props}>
              <div className="d-flex align-items-center">
                {icon ? (
                  <span>
                    <img src={icon} alt="icon" className="mr-1" />
                  </span>
                ) : ''}
                <span>{children}</span>
              </div>
            </components.SingleValue>
          ),
          Placeholder: ({ children, ...props }) => (
            <components.Placeholder {...props}>
              <div className="d-flex align-items-center">
                {icon ? (
                  <img src={icon} alt="icon" className="mr-1" />
                ) : ''}
                <span>{children}</span>

              </div>
            </components.Placeholder>
          ),
        }}
        options={optionsList}
        className={`rec_select_input ${className}`}
        classNamePrefix={`${classNamePrefix} rec_select_input_pre`}
        placeholder={placeholder}
        onChange={onSelect}
      />
    </div>
  )
}

export default InputSelect
