import React, { useEffect, useState } from 'react'

import './styles.css'

interface Props {
  onChange?: (event: any) => void;
  value?: string;
  classNamesInput?: string;
  classNamesIcon?: string;
}

const InputNumber = (props: Props) => {
  const {
    onChange = (e) => setNumber(e),
    value,
    classNamesInput,
    classNamesIcon,
  } = props
  const [number, setNumber] = useState('0')
  useEffect(() => {
    setNumber(value || '')
  }, [value])
  return (
    <div className="wrap_input_number">
      <img
        src="/icons/ico-DecreaseNumber.svg"
        alt="ico-DecreaseNumber"
        className={`${classNamesIcon} ico-number`}
        onClick={() => onChange((Number(number) - 1 < 0 ? 0 : Number(number) - 1).toString())}
      />
      <input
        value={number}
        className={`${classNamesInput} input_number`}
        onChange={(e) => onChange(e.target.value.replace(/\D/g, ''))}
      />
      <img
        src="/icons/ico-IncreaseNumber.svg"
        alt="ico-IncreaseNumber"
        className={`${classNamesIcon} ico-number`}
        onClick={() => onChange((Number(number) + 1).toString())}
      />
    </div>
  )
}

export default InputNumber
