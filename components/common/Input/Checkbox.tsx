import * as React from 'react'
import { useEffect, useState } from 'react'
import { Form } from 'react-bootstrap'

interface Props {
  className?: string
  label?: string
  checked?: boolean
  disabled?: boolean
  onChangeFunc: (checked: boolean) => void
  name: string
}

const InputCheckBox = (props: Props) => {
  const {
    className = '',
    label = '',
    checked = false,
    disabled = false,
    onChangeFunc,
    name,
  } = props

  const [checkbox, setCheckbox] = useState(false)
  useEffect(() => {
    if (checked) {
      setCheckbox(true)
    }
  }, [checked])

  return (
    <div key="custom-eCheckbox" className={`${className}`}>
      <Form.Check
        custom
        type="checkbox"
        name={name}
        id="custom-eCheckbox"
        label={label}
        checked={checkbox}
        disabled={disabled}
        onChange={(e) => {
          e.persist()
          setCheckbox((e.target as HTMLInputElement).checked)
          onChangeFunc((e.target as HTMLInputElement).checked)
        }}
      />
    </div>
  )
}

export default InputCheckBox
