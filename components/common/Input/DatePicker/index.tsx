import * as React from 'react'
import { useState } from 'react'
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker'
import './styles.css'

import vi from 'date-fns/locale/vi'

registerLocale('vi', vi)

interface Props {
  selected?: Date,
  className?: string
  placeholder?: string
  disabled?: boolean
  onChange?: (selected: Date) => void
}

const InputDate = (props: Props) => {
  const {
    placeholder = 'dd/mm/yyyy',
    className = '',
    selected,
    disabled = false,
    onChange,
  } = props
  return (
    <div className="rec__date position-relative">
      <img className="rec__date__input--icon" src="/icons/ico-calendar.svg" alt="..." />
      <DatePicker
        className={`rec__date__input ${className} `}
        selected={selected}
        onChange={(date: any) => {
          console.log('data onchange', date)
          onChange && onChange(date)
        }}
        locale="vi"
        disabled={disabled}
        placeholderText={placeholder}
      />
    </div>
  )
}

export default InputDate
