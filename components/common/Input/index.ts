import Checkbox from './Checkbox'
import InputText from './Text'
import Select from './Select'
import Date from './DatePicker'
import InputTextarea from './Textarea'
import Number from './Number'

const Input = {
  Checkbox,
  Select,
  Date,
  InputText,
  InputTextarea,
  Number,
}
export default Input
