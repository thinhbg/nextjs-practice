import React, { ReactNode, useEffect, useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap'
import './styles.css'

interface Props {
  disabled?: boolean
  id?: string
  prefix?: ReactNode
  suffix?: ReactNode
  type?: string
  value?: string
  onChangeFunc: (data: any) => void
  onPressEnter?: (data: any) => void
  className?: string
  placeholder?: string
  maxHeight?: string
  name: string,
}

const InputText = (
  {
    className = '',
    placeholder = '',
    prefix,
    suffix,
    value = '',
    type = 'text',
    disabled = false,
    maxHeight = '10rem', // 1 row = 3.375rem
    onChangeFunc,
    onPressEnter,
    name,
  }: Props,
) => {
  const [input, setInput] = useState('')

  useEffect(() => {
    setInput(value)
  }, [value])

  const handleKeyDown = (event: any) => {
    if (event.key === 'Enter') {
      console.log('do validate', input)
      onPressEnter && onPressEnter(input)
    }
  }
  return (
    <InputGroup
      className={`recInput-text ${className} mb-0`}
    >
      {
        prefix && (
          <div className="recInput-text-prefix">
            {prefix}
          </div>
        )
      }

      <Form.Control
        name={name}
        placeholder={placeholder}
        className="recInput__input-area"
        onChange={(e) => {
          setInput(e.target.value)
          onChangeFunc(e.target.value)
        }}
        value={input}
        type={type}
        disabled={disabled}
        onKeyPress={(event: any) => {
          handleKeyDown(event)
        }}
        as="textarea"
        style={{ maxHeight: `${maxHeight}` }}
      />

      {
        suffix && (
          <div className="recInput-text-suffix">
            {suffix}
          </div>
        )
      }

    </InputGroup>
  )
}

export default InputText
