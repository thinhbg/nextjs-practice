import React, { ReactNode, useEffect, useState } from 'react'
import { Form, InputGroup } from 'react-bootstrap'
import './styles.css'

interface Props {
  disabled?: boolean
  id?: string
  prefix?: ReactNode
  suffix?: ReactNode
  type?: string
  value?: string
  onChangeFunc?: (data: any) => void
  onPressEnter?: (data: any) => void
  className?: string
  placeholder?: string
  readOnly?: boolean
  field?: any
}

const InputText = (
  {
    className = '',
    placeholder = '',
    prefix,
    suffix,
    value = '',
    type = 'text',
    disabled = false,
    readOnly = false,
    onChangeFunc, onPressEnter,
    field,
  }: Props,
) => {
  const [input, setInput] = useState('')

  useEffect(() => {
    setInput(value)
  }, [value])

  const handleKeyDown = (event: any) => {
    if (event.key === 'Enter') {
      console.log('do validate', input)
      onPressEnter && onPressEnter(input)
    }
  }
  return (
    <InputGroup
      className={`recInput-text ${className} mb-0`}
    >
      {
        prefix && (
          <div className="recInput-text-prefix  d-none d-sm-block">
            {prefix}
          </div>
        )
      }

      <Form.Control
        {...field}
        placeholder={placeholder}
        className={`recInput__input-text ${prefix ? 'rec_prefix' : ''} ${suffix ? 'rec_suffix' : ''}`}
        readOnly={readOnly}
        onChange={(e) => {
          setInput(e.target.value)
          onChangeFunc && onChangeFunc(e.target.value)
        }}
        value={input}
        type={type}
        disabled={disabled}
        onKeyPress={(event: any) => {
          handleKeyDown(event)
        }}
      />

      {
        suffix && (
          <div className="recInput-text-suffix">
            {suffix}
          </div>
        )
      }

    </InputGroup>
  )
}

export default InputText
