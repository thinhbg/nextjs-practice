import React from 'react'
import './styles.css'

interface Props {
  onClick?: () => void;
  data?:{
    avatar?: string;
    name?: string;
    content?: string;
    isRead?: Boolean;
  }
}

const ItemMessage = (props: Props) => {
  const {
    onClick,
    data = {
      avatar: '/images/Avatar-seller.jpg',
      name: 'Lương Mạnh Hải',
      content: 'Khi nào được thì báo lại anh nhé, rồi',
      isRead: false,
    },
  } = props

  return (
    <div className="wrap_content_messages hover_point" onClick={onClick}>
      <div className="content_a_message">
        <img src={data?.avatar} alt="avatar" className="avatar_messages" />
        <div className="content_message">
          <div className="name_message">
            <span>{data?.name}</span>
          </div>
          <div className={`content_mess ${data?.isRead && 'color_yellow'}`}>
            <span>{data?.content}</span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ItemMessage
