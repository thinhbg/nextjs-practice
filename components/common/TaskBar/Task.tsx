import React, { ReactNode, useState } from 'react'
import './styles.css'

interface Props {
  classNameTask?: string;
  dataNotifyHouse?: ReactNode;
  dataMessages?: ReactNode;
  isNotify?: number;
  isMessages?: number;
  onClickSearch?:() => void;
  onClickNewMess?:() => void;
  onClickOptionMess?:() => void;
}

const Task = (props: Props) => {
  const {
    classNameTask,
    dataNotifyHouse,
    dataMessages,
    isNotify,
    isMessages,
    onClickSearch,
    onClickNewMess,
    onClickOptionMess,
  } = props
  const [isActive, setIsActive] = useState(0)
  const [isIcoActive, setIsIcoActive] = useState(0)
  const [isShowTab, setIsShowTab] = useState(0)
  return (
    <div className={`rec_task ${classNameTask}`}>
      <div
        className={`tab_ico tab_home ${isActive === 1 && 'bg_active'}`}
        onClick={() => {
          setIsActive(1)
          setIsShowTab(1)
        }}
        onMouseEnter={() => setIsIcoActive(1)}
        onMouseLeave={() => setIsIcoActive(0)}
      >
        {isIcoActive === 1 || isActive === 1 ? <img src="/icons/ico-home-active.svg" alt="home" className="ico-toolbar" /> : <img src="/icons/ico-home.svg" alt="home" className="ico-toolbar" />}
      </div>
      <div className="wrap_notify">
        {isShowTab === 2 && (
        <div className="tab_content_notify">
          <div className="wrap_label_notify">
            <span>Bài đăng mới</span>
            <img src="/icons/ico-close.svg" alt="close" className="notify_ico_close" onClick={() => setIsShowTab(0)} />
          </div>
          <div className="wrap_content_notify">
            {dataNotifyHouse}
          </div>
        </div>
        )}
        <div
          className={`tab_ico tab_notify ${isActive === 2 && 'bg_active'}`}
          onClick={() => {
            setIsActive(2)
            setIsShowTab(isShowTab === 2 ? 0 : 2)
          }}
          onMouseEnter={() => setIsIcoActive(2)}
          onMouseLeave={() => setIsIcoActive(0)}
        >
          {isNotify ? (
            <div className="badge_notify">
              <span>{`${isNotify > 9 ? '9+' : isNotify}`}</span>
            </div>
          ) : null}
          {isIcoActive === 2 || isActive === 2 ? <img src="/icons/ico-bell-active.svg" alt="bell" className="ico-toolbar" /> : <img src="/icons/ico-bell.svg" alt="bell" className="ico-toolbar" /> }
        </div>
      </div>
      <div className="wrap_messages_tab">
        {isShowTab === 3 && (
        <div className="tab_content_messages">
          <div className="wrap_label_messages">
            <span>Tin nhắn</span>
            <div className="tool_messages">
              <img src="/icons/ico-search.svg" alt="search" className="ico_tool_messages" onClick={onClickSearch} />
              <img src="/icons/ico-new-message.svg" alt="new-messages" className="ico_tool_messages" onClick={onClickNewMess} />
              <img src="/icons/ico-3-dot-mes.svg" alt="ico" className="ico_tool_messages" onClick={onClickOptionMess} />
            </div>
          </div>
          <div className="wrap_content_message">
            {dataMessages}
          </div>
        </div>
        )}
        <div
          className={`tab_ico tab_message ${isActive === 3 && 'bg_active'}`}
          onClick={() => {
            setIsActive(3)
            setIsShowTab(isShowTab === 3 ? 0 : 3)
          }}
          onMouseEnter={() => setIsIcoActive(3)}
          onMouseLeave={() => setIsIcoActive(0)}
        >
          {isMessages ? (
            <div className="badge_notify">
              <span>{`${isMessages > 9 ? '9+' : isMessages}`}</span>
            </div>
          ) : null}
          {isIcoActive === 3 || isActive === 3 ? <img src="/icons/ico-message-active.svg" alt="message" className="ico-toolbar" /> : <img src="/icons/ico-message.svg" alt="message" className="ico-toolbar" /> }
        </div>
      </div>
      <div
        className={`tab_ico tab_friend ${isActive === 4 && 'bg_active'}`}
        onClick={() => {
          setIsActive(4)
          setIsShowTab(4)
        }}
        onMouseEnter={() => setIsIcoActive(4)}
        onMouseLeave={() => setIsIcoActive(0)}
      >
        {isIcoActive === 4 || isActive === 4 ? <img src="/icons/ico-two-man-active.svg" alt="friend" className="ico-toolbar" /> : <img src="/icons/ico-two-man.svg" alt="friend" className="ico-toolbar" /> }
      </div>
    </div>
  )
}

export default Task
