import Task from './Task'
import ItemMessage from './ItemMessage'

const TaskBar = {
  Task,
  ItemMessage,
}
export default TaskBar
