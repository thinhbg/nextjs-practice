import React, { useRef } from 'react'
import Slider from 'react-slick'
import './styles.css'

export interface SliderProps {
  children: React.ReactNode
  haveDot?: boolean
  className?: string
  settingCustom?: any
  slickLeftClass?: string
  slickRightClass?: string
  arrowSize?: any
  type?: number
}
const ESlider = (
  {
    children, haveDot = false, className, settingCustom,
    slickLeftClass = '',
    slickRightClass = '',
    arrowSize = { width: '4rem', height: '4rem' },
    type = 1,
  }: SliderProps,
) => {
  const slick = useRef(null)
  const SlickArrowLeft = ({ currentSlide, slideCount, ...props }: any) => (
    <div
      {...props}
      className={
        `${slickLeftClass} slick-prev slick-arrow${
          currentSlide === 0 ? ' slick-disabled' : ''} ${type === 1 ? '' : 'slick-arrow-2'}`
      }
      aria-hidden="true"
      aria-disabled={currentSlide === 0 ? true : false}
      style={arrowSize}
    >
      {type === 1
        ? <img src="/icons/ico-cavet-left-no-shadow.svg" aria-hidden alt="slide-arrow" className="" />
        : <img src="/icons/ico-left-arrow-white.svg" aria-hidden alt="slide-arrow" className="" /> }
    </div>
  )
  const SlickArrowRight = ({ currentSlide, slideCount, ...props }: any) => (
    <div
      {...props}
      className={
        `${slickRightClass} slick-next slick-arrow${
          currentSlide === slideCount - 1 ? ' slick-disabled' : ''} ${type === 1 ? '' : 'slick-arrow-2'}`
      }
      aria-hidden="true"
      aria-disabled={currentSlide === slideCount - 1 ? true : false}
      style={arrowSize}
    >
      {type === 1
        ? <img src="/icons/ico-cavet-right-no-shadow.svg" aria-hidden alt="slide-arrow" className="" />
        : <img src="/icons/ico-right-arrow-white.svg" aria-hidden alt="slide-arrow" className="" /> }
    </div>
  )
  const settings = {
    centerMode: true,
    dots: haveDot ? true : false,
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 10000,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: <SlickArrowRight />,
    prevArrow: <SlickArrowLeft />,
    responsive: [
      {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767.98,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 575.98,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
    ...settingCustom,
  }

  return (
    <Slider ref={slick} {...settings} className={`eSlider ${className ? className : ''}`}>
      {children}
    </Slider>
  )
}

export default ESlider
