import React from 'react'
import { TFunction } from 'next-i18next'
import RecLayout from '../../components/Layout'
import { withTranslation } from '../../i18n'
import {
  SectionSlide,
  SectionSearch,
  SectionTrend,
  SectionLocalTrend,
  SectionIncentives,
  SectionHot,
  SectionPartners,
  SectionProjectNews,
} from '../../components/pages-components/projects'
import SectionContacts from '../../components/common/SectionContacts'
import './styles.css'

const ProjectPage = ({ t }: { readonly t: TFunction }) => (
  <>
    <RecLayout active="project">
      <SectionSlide />
      <SectionSearch />
      <SectionTrend />
      <SectionLocalTrend />
      <SectionIncentives />
      <SectionHot />
      <SectionPartners />
      <SectionProjectNews />
      <SectionContacts />
    </RecLayout>

  </>
)

export const getServerSideProps = async (context: any) => ({
  props: {
    namespacesRequired: ['common'],
  },
})

export default withTranslation('common')(ProjectPage)
