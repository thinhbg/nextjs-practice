import React from 'react'

function Error({ statusCode }: any) {
  return (
    <p>
      {statusCode
        ? `An error ${statusCode} occurred on server`
        : 'An error occurred on client'}
    </p>
  )
}

export async function getStaticProps({ res, err }: any) {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return {
    props: {
      statusCode,
      namespacesRequired: ['common'],
    },
  }
}

export default Error
