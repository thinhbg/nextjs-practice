import React from 'react'
import HomePage from './home'

const Index = () => (
  <HomePage />
)

Index.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default Index
