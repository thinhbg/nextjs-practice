import { I18nContext } from 'next-i18next'
import React, { useContext, useEffect, useState } from 'react'
import Button from '../../components/common/Button'
import Card from '../../components/common/Card'
import StoryCard from '../../components/common/Card/StoryCard'
import { CustomImage, EAvatar } from '../../components/common/EImage'
import { ESlider } from '../../components/common/ESlider'
import TaskBar from '../../components/common/TaskBar'
import RecLayout from '../../components/Layout'
import { withTranslation } from '../../i18n'
import './styles.css'

const JsonData = [
  {
    image: '/images/RealEstate-1.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    location: 'Liễu giai, Ba Đình, Hà Nội',
    size: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
  },
  {
    image: '/images/RealEstate-1.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    location: 'Liễu giai, Ba Đình, Hà Nội',
    size: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
  },
  {
    image: '/images/RealEstate-1.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    location: 'Liễu giai, Ba Đình, Hà Nội',
    size: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
  },
  {
    image: '/images/RealEstate-1.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    location: 'Liễu giai, Ba Đình, Hà Nội',
    size: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
  },
  {
    image: '/images/RealEstate-1.png',
    title: 'Căn m3 06 vin homes metropolis Liêu giai',
    location: 'Liễu giai, Ba Đình, Hà Nội',
    size: '120m2',
    bedrooms: '3pn',
    bathrooms: '2vs',
    direction: 'Đông nam',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, cumque rem vel dolore consectetur repellat dolor, neque hic ut id maiores, unde corporis. Sint unde minus ab? Placeat, hic voluptate!Impedit ratione amet quae voluptatibus doloribus voluptate minus enim accusamus ipsa. Consequatur aliquid, suscipit ducimus officia dolores eaque illum asperiores doloremque. Officiis in repellendus distinctio repudiandae, unde beatae quis possimus?',
  },
]
const JsonDataHouse = [
  {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'Esther Howard',
    img: '/images/ImageBietThu.png',
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông Nam',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'Esther Howard',
    img: '/images/ImageBietThu.png',
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông Nam',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'Esther Howard',
    img: '/images/ImageBietThu.png',
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông Nam',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'Esther Howard',
    img: '/images/ImageBietThu.png',
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông Nam',
    price: '4,300,000,000',
  },
  {
    avatarSeller: '/images/Avatar-seller.jpg',
    nameSeller: 'Esther Howard',
    img: '/images/ImageBietThu.png',
    company: 'Công ty CP Tân Hoàng Minh Plus',
    title: 'Biệt thự có hồ bơi riêng, P.Yên Hòa, Cầu Giầy',
    address: 'Liễu Giai, Ba Đình, Hà Nội',
    acreage: '120m2',
    bedrooms: '3PN',
    bathrooms: '2VS',
    direction: 'Đông Nam',
    price: '4,300,000,000',
  },
]
const JsonDataMess = [
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: false,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: false,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: true,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: false,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: true,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: true,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: true,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: true,
  },
  {
    avatar: '/images/Avatar-seller.jpg',
    name: 'Lương Mạnh Hải',
    content: 'Khi nào được thì báo lại anh nhé, rồi',
    isRead: true,
  },
]

const DemoPage = () => {
  const { i18n: { language } } = useContext(I18nContext)
  const [state, setState] = useState('')

  const JsonData = [
    {
      value: 'name 1',
      label: 'Nhu cầu: Tìm mua',
    },
    {
      value: 'name 2',
      label: 'Nhu cầu: Tìm ban',
    },
    {
      value: 'name 3',
      label: 'Nhu cầu: Tìm cua',
    },
    {
      value: 'name 4',
      label: 'Nhu cầu: Tìm tim',
    },
  ]
  return (
    <RecLayout>
      <ESlider>
        {
          JsonData.map(item => (
            <div className="px-2">
              <Card.House />
            </div>
          ))
        }
      </ESlider>
      <div className="w-100 p-5 flex-row flex">
        <TaskBar.Task
          dataNotifyHouse={JsonDataHouse.map((el:any) => <Card.HouseNotification data={el} />)}
          dataMessages={JsonDataMess.map((el:any) => <TaskBar.ItemMessage data={el} />)}
          isMessages={3}
          isNotify={0}
        />
      </div>
      <div className="w-25 mt-5">
        <Card.Project type={2} />
      </div>
      <div className="w-25">
        <CustomImage src="/images/1Group248.png" />

      </div>

      {/* <Button.ButtonWIcon text="hihi" btnColor="outline_button-blue" prefix={<img src="/icons/ico-eye-blue.svg" />} /> */}
      <Button.Outline
        text="hihi"
        btnColor="outline_button-blue"
        prefix={<img src="/icons/ico-eye-blue.svg" alt="icon eye" className="mr-4" />}
        className="px-4"
      />

      <div style={{
        width: '25%', marginLeft: '5rem', marginBottom: '5rem', marginTop: '5rem',
      }}
      >
        <StoryCard
          cardData={{
            name: 'helo helo helo',
            title: 'Đăng bán',
            img: '/images/1Group248.png',
            titleBackground: 'var(--color-green-300)',
            titleColor: 'var(--color-white)',
          }}
        />
      </div>
      <div style={{
        width: '25%', marginLeft: '5rem', marginBottom: '5rem', marginTop: '5rem',
      }}
      >
        <EAvatar
          src="/images/Group248.png"
          width="64px"
          height="64px"
          onlineStatus
          borderCustom="1px solid var(--color-toryBlue)"
        />
      </div>

    </RecLayout>
  )
}

DemoPage.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(DemoPage)
