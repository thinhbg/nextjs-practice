import React from 'react'
import RecLayout from '../../components/Layout'
import { withTranslation } from '../../i18n'

import './styles.css'

interface Props {
  t:any
}
const GroupsList = ({ t }:Props) => (
  <>
    <RecLayout>
      <p>
        This is All Group Page
      </p>
    </RecLayout>

  </>
)

export const getServerSideProps = async (context: any) => ({
  props: {
    namespacesRequired: ['common'],
  },
})

export default withTranslation('common')(GroupsList)
