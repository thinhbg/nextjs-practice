import React from 'react'
import axios from 'axios'
import { wrapper } from '../../redux/store'
import { actionSaveDemoData } from '../../redux/home/actions'
import RecLayout from '../../components/Layout'

import './styles.css'
import SectionSearch from '../../components/common/SectionSearch'
import { GroupDetailComp } from '../../components/pages-components/group'

export const getServerSideProps = wrapper.getServerSideProps(
  async ({
    store, req, res, ...etc
  }) => {
    // console.log('store,', store)
    try {
      const response = await axios.get('https://5fae518863e40a0016d89901.mockapi.io/Fake01')
      store.dispatch(actionSaveDemoData(response.data))
      // store.dispatch(addHouseAction('hihi'))
      console.log('response', response.data)
    } catch (err) {
      console.log('err', err)
    }
  },
)

interface Props {

}

const GroupDetail = () => (
  <RecLayout>
    <div className="detailGroup-wallpaper">
      <img src="/images/recbook-wallpaper.jpg" alt="rec wallpaper" className="object-fit-cover" />
      <div className="detailGroup-search my-4">
        <SectionSearch />
      </div>
    </div>
    <GroupDetailComp />
  </RecLayout>
)

export default GroupDetail
