import App, { AppProps, AppContext } from 'next/app'
import * as React from 'react'
import { appWithTranslation } from '../i18n'
import '../styles/main.css'
import { wrapper } from '../redux/store'

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <Component {...pageProps} />
  )
}

// MyApp.getInitialProps = async (appContext: AppContext) => ({ ...await App.getInitialProps(appContext) })
export default wrapper.withRedux(appWithTranslation(MyApp))
