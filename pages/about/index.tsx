import { I18nContext } from 'next-i18next'
import { useContext } from 'react'
import { withTranslation, i18n } from '../../i18n'
import './styles.css'

interface Props{
    t:any
}
const AboutPage = ({ t }: Props) => {
  const { i18n: { language } } = useContext(I18nContext)

  return (
    <div
      className="AboutPage"
    >
      {t('homePage')}
    </div>
  )
}

AboutPage.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(AboutPage)
