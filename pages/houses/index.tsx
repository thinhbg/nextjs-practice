import React from 'react'
import { TFunction } from 'next-i18next'
import RecLayout from '../../components/Layout'
import { withTranslation } from '../../i18n'
import {
  SectionSearch, SectionSlide, SectionsLocalTrend, SectionBankNews,
} from '../../components/pages-components/houses'
import './styles.css'
import { wrapper } from '../../redux/store'
import axios from 'axios'
import { actionSaveDemoData } from '../../redux/home/actions'
import { addHouseAction } from '../../redux/houses/actions'

export const getServerSideProps = wrapper.getServerSideProps(
  async ({
    store, req, res, ...etc
  }) => {
    console.log('store,', store)
    try {
      store.dispatch(addHouseAction('haha'))
    } catch (err) {
      console.log('err', err)
    }
  },
)

const HousesPage = ({ t }: { readonly t: TFunction }) => (
  <>
    <RecLayout active="houses">
      <SectionSlide />
      <SectionSearch />
      <SectionsLocalTrend />
      <SectionBankNews />
    </RecLayout>

  </>
)

export default withTranslation('common')(HousesPage)
