import React from 'react'
import { useSelector } from 'react-redux'
import axios from 'axios'
import Link from 'next/link'
import { wrapper } from '../../redux/store'
import { ApplicationState } from '../../redux'
import { actionSaveDemoData } from '../../redux/home/actions'
import { addHouseAction } from '../../redux/houses/actions'

export const getServerSideProps = wrapper.getServerSideProps(
  async ({
    store, req, res, ...etc
  }) => {
    // console.log('store,', store)
    try {
      const response = await axios.get('https://5fae518863e40a0016d89901.mockapi.io/Fake01')
      store.dispatch(actionSaveDemoData(response.data))
      // store.dispatch(addHouseAction('hihi'))
      console.log('response', response.data)
    } catch (err) {
      console.log('err', err)
    }
  },
)

const DetailDemo = () => {
  const state = useSelector((state:ApplicationState) => state.home.logged)
  return (
    <div>
      <Link href="/houses">House</Link>
    </div>
  )
}

export default DetailDemo
