import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import RecLayout from '../../components/Layout'
import { LoggedIn, Login } from '../../components/pages-components/home'
import { withTranslation } from '../../i18n'

import './styles.css'
import { ApplicationState } from '../../redux'

const countryList = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
]
interface Props {
  t:any
}
const HomePage = ({ t }:Props) => {
  const [logged, setLogged] = useState(true)
  const isLogged = useSelector((state: ApplicationState) => state?.home?.logged)
  useEffect(() => {
    if (isLogged) {
      setLogged(true)
    }
  }, [isLogged])
  return (
    <>
      <RecLayout>
        {
          logged
            ? (
              <LoggedIn />
            )
            : (
              <Login />
            )
        }
      </RecLayout>

    </>
  )
}

HomePage.getServerSideProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(HomePage)
