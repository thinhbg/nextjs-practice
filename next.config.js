const withFonts = require('next-fonts')
const withCSS = require('@zeit/next-css')
const withOffline = require('next-offline')
const withImages = require('next-images')
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin')

const nextConfig = {
  webpack: (config) => {
    config.plugins.push(
      new FilterWarningsPlugin({
        exclude: /mini-css-extract-plugin[^]*Conflicting order between:/,
      }),
    )
    return config
  },
}

module.exports = withOffline(withFonts(
  withCSS(withImages(
    {
      env: {},
      async rewrites() {
        return [
          { source: '/', destination: '/' },
        ]
      },
      webpack: (config) => {
        config.plugins.push(
          new FilterWarningsPlugin({
            exclude: /mini-css-extract-plugin[^]*Conflicting order between:/,
          }),
        )
        return config
      },
    },
  )),
))
