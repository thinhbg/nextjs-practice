module.exports = function (plop) {
  // create your generators here
  plop.setGenerator('basics', {
    description: 'this is a skeleton plopfile',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Page Name: ',
      },
      // {
      //   type: 'input',
      //   name: 'name',
      //   message: 'Page Name: '
      // },
    ], // array of inquirer prompts
    actions: [

      // add component in pages
      {
        type: 'add',
        path: 'pages/{{name}}/index.tsx',
        templateFile: 'plop-templates/index.tsx.hbs',
      },
      {
        type: 'add',
        path: 'pages/{{name}}/styles.css',
        templateFile: 'plop-templates/styles.css.hbs',
      },

      // add redux file in redux
      {
        type: 'addMany',
        destination: 'redux/{{name}}',
        templateFiles: ['plop-templates/**.ts.hbs'],
      },
      {
        type: 'append',
        path: 'redux/index.ts',
        pattern: '/* AUTO IMPORT PLOP */',
        template: 'import {{name}}Reducer from \'./{{name}}/reducers\'',
      },
      {
        type: 'append',
        path: 'redux/index.ts',
        pattern: '/* AUTO IMPORT PLOP */',
        template: 'import {{name}}State from \'./{{name}}/states\'',
      },
      {
        type: 'append',
        path: 'redux/index.ts',
        pattern: '/* AUTO REDUCER PLOP */',
        template: '  {{name}}: {{name}}Reducer,',
      },
      {
        type: 'append',
        path: 'redux/index.ts',
        pattern: '/* AUTO STATES PLOP */',
        template: '  {{name}}: {{name}}State,',
      },
    ],
  })
}
