export const numberWithCommas = (x?: number | string) => (
  (x !== undefined && x !== null)
    ? x
      .toString()
      .replace(/[a-zA-Z]/gi, '')
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    : '')
