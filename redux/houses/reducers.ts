import { Reducer } from 'redux'
import States from './states'
import { Types } from './types'
// import Types from './types'

const initialState: States = {
  example: '',
  houses: [],
}

const ReducerPage: Reducer<States> = (state = initialState, payload) => {
  switch (payload.type) {
    case Types.ADD_HOUSE:
      return {
        ...state,
        houses: state.houses.concat(payload.payload),
      }
    default:
      return state
  }
}

export default ReducerPage
