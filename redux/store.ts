// import { loadState, saveState } from '../lib/storage'
import { createWrapper } from 'next-redux-wrapper'
import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '.'

const bindMiddleware = (middleware: any[]) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension')
    return composeWithDevTools(applyMiddleware(...middleware))
  }
  return applyMiddleware(...middleware)
}

function initStore() {
  // @ts-ignore
  return createStore(rootReducer, bindMiddleware([thunkMiddleware]))
}

export const wrapper = createWrapper(initStore)
