import { Types } from './types'

export const actionSaveLogIn = (payload: any) => ({
  type: Types.LOGGED_IN,
  payload,
})
export const actionSaveDemoData = (payload: any) => ({
  type: Types.DEMO_DATA,
  payload,
})

export const actionGetLogIn = (payload:any) => async (dispatch: any) => {
  try {
    await dispatch(actionSaveLogIn(payload))
  } catch (error) {
    console.log('getLogIn -> error', error)
  }
}
