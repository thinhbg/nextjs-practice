import { AnyAction } from 'redux'
import States from './states'
import { Types } from './types'

const initialState: States = {
  logged: false,
  demoData: [],
}

const ReducerPage:any = (state = initialState, payload: AnyAction) => {
  switch (payload.type) {
    case Types.LOGGED_IN:
      return {
        ...state,
        logged: payload,
      }
    case Types.DEMO_DATA:
      console.log('payload', payload)
      return {
        ...state,
        demoData: payload.payload,
      }
    default:
      return state
  }
}

export default ReducerPage
