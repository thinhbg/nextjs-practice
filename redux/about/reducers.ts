import { Reducer } from 'redux'
import States from './states'
// import Types from './types'

const initialState: States = {
  example: '',
}

const ReducerPage: Reducer<States> = (state = initialState, payload) => {
  switch (payload.type) {
    default:
      return state
  }
}

export default ReducerPage
