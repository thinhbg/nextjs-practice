import { combineReducers } from 'redux'
import { HYDRATE } from 'next-redux-wrapper'
import HomeReducer from './home/reducers'
import HomeStates from './home/states'
/* AUTO IMPORT PLOP */
import housesState from './houses/states'
import housesReducer from './houses/reducers'
import aboutState from './about/states'
import aboutReducer from './about/reducers'

export interface ApplicationState {
  home: HomeStates,
  houses: housesState,
  about: aboutState,
}

// export default combineReducers(RootReducer)

const combinedReducer = combineReducers({
  home: HomeReducer,
  /* AUTO REDUCER PLOP */
  houses: housesReducer,
  about: aboutReducer,
})

const rootReducer = (state: ApplicationState, action: any) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    }

    console.log('prvState', state)
    console.log('nextState', nextState)

    return nextState
  }
  return combinedReducer(state, action)
}

export default rootReducer
